﻿using System;
using System.IO;

namespace Freycook0.Crypto {
	// 32-bit
	public static class Blake2s {
		// Constants
		// Initial Vector, which is no longer different for different length outputs (Blake used different IVs for, e.g., Blake-256 and Blake-224).
		private static readonly uint[] InitialVector = new uint[8] {
			0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
		};
		private const int BlockSizeBytes = 64;
		private const int HalfBlockSizeBytes = 32;

		// At the moment we're only going to accept byte arrays.  There is a drawback with this approach: the algorithm is limited to ~2GB of data that can be hashed.  The specification states that it should be able to handle data up to ~2 exabytes (2 ^ 64 bytes).
		public static byte[] Hash(byte[] message, int hashLength = HalfBlockSizeBytes, byte[] key = null) {
			// Parameter Checking
			// message: non-null
			if(message == null)
				throw new ArgumentNullException("message", "Cannot hash a null message.  Provide a non-null message to hash.");
			// hashLength: between 1 and 32
			if(hashLength < 1 || hashLength > HalfBlockSizeBytes)
				throw new ArgumentOutOfRangeException("hashLength", hashLength, "The length of the output hash for Blake2s must be between 1 and 32 bytes long");
			// key: between 0 and 32 bytes long
			if(key != null && key.Length > HalfBlockSizeBytes)
				throw new ArgumentOutOfRangeException("key", key.Length, "Key must not exceed 32 bytes for Blake2s.  Provide key no longer than 32 bytes.");

			// Data Preparation
			// Determine what the length of the message is before padding.  This is utilized towards the end in the creation of the matrix for the last block of data.
			ulong length = (ulong) message.Length;
			// Determine the key length.  It is used in generating the initial chain value and it affects the counter used when creating Blake2s_Matrix objects.
			uint keyLength, keyBlockLength;
			if(key == null) {
				keyLength = 0;
				keyBlockLength = 0;
			}
			else {
				keyLength = (uint)key.Length;
				keyBlockLength = BlockSizeBytes;
				// Pad the key with 0s to the size of a full data block (64 bytes in Blake2s).
				byte[] tmp = new byte[BlockSizeBytes];
				Array.Copy(key, 0, tmp, 0, key.Length);
				key = tmp;
			}
			// Pad the message.
			int messageLengthWithPad = message.Length + ((BlockSizeBytes - (message.Length % BlockSizeBytes)) % BlockSizeBytes);
			Stream messageStream = new MemoryStream(message, 0, message.Length, false);
			// Generate the first Chain Value.
			uint[] chainValue = new uint[8];
			Array.Copy(InitialVector, 0, chainValue, 0, 8);
			chainValue[0] = chainValue[0] ^ 0x01010000 ^ (keyLength << 8) ^ ((uint) hashLength);
			// Placeholders
			byte[] messageFractionBytes = new byte[BlockSizeBytes];
			uint[] messageFraction = new uint[16];

			// Data Processing
			// Break the (padded) message into 64-byte blocks, and process each block sequentially.
			for(int x = 0; x < messageLengthWithPad + keyBlockLength; x += BlockSizeBytes) {
				// Obtain the message block.  If a key was provided for the hash, then it is used as the first data block.  The message will follow.
				if(x == 0 && keyBlockLength != 0) {
					messageFractionBytes = key;
				}
				else {
					// Read the next 64 bytes from the MemoryStream.  If there are less than 64 bytes read from the MemoryStream, then fill the rest with 0s.
					int tmp = messageStream.Read(messageFractionBytes, 0, BlockSizeBytes);
					if(tmp < BlockSizeBytes) {
						for(int y = tmp; y < BlockSizeBytes; y++)
							messageFractionBytes[y] = 0;
					}
				}

				// Use the message block to build the array of unsigned integers.
				for(int y = 0; y < 16; y++)
					messageFraction[y] = BitConverter.ToUInt32(messageFractionBytes, y * 4);

				// Calculate the length in bytes of the message so far through this block.  If a key was provided for hashing, exclude the first block of data processed from the number of bytes.
				ulong lengthX = System.Math.Min(((ulong) x) - ((ulong) keyBlockLength) + BlockSizeBytes, (ulong) length);

				// Build our matrix, then run all the rounds necessary.
				Blake2s_Matrix matrix = new Blake2s_Matrix(chainValue, messageFraction, GetLower32Bits(lengthX), GetUpper32Bits(lengthX), (x == messageLengthWithPad + keyBlockLength - BlockSizeBytes));
				matrix.Rounds();

				// Get the next chain value; we'll use it in the next loop.
				chainValue = matrix.GetNextChain();
			}
			messageStream.Close();

			// Finalize
			// Convert the final chain value to bytes.  The final result is truncated to the number of bytes requested in the input parameters.
			byte[] tmpHash = new byte[HalfBlockSizeBytes];
			for(int x = 0; x < 8; x++) {
				byte[] tmp = BitConverter.GetBytes(chainValue[x]);
				Array.Copy(tmp, 0, tmpHash, x * 4, 4);
			}
			byte[] hash = new byte[hashLength];
			Array.Copy(tmpHash, 0, hash, 0, hashLength);
			return hash;
		}

		private static uint GetLower32Bits(ulong t) {
			return (uint)t;
		}

		private static uint GetUpper32Bits(ulong t) {
			return (uint)(t >> 32);
		}

		// Private class for modeling the 4x4 matrix of unsigned integers and the round of operations that are performed on them.
		private class Blake2s_Matrix {
			// Blake2s uses 10 rounds, so 10 permutations are needed.
			private static readonly int[,] Permutations = new int[10, 16] {
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 },
				{ 11, 08, 12, 00, 05, 02, 15, 13, 10, 14, 03, 06, 07, 01, 09, 04 },
				{ 07, 09, 03, 01, 13, 12, 11, 14, 02, 06, 05, 10, 04, 00, 15, 08 },
				{ 09, 00, 05, 07, 02, 04, 10, 15, 14, 01, 11, 12, 06, 08, 03, 13 },
				{ 02, 12, 06, 10, 00, 11, 08, 03, 04, 13, 07, 05, 15, 14, 01, 09 },
				{ 12, 05, 01, 15, 14, 13, 04, 10, 00, 07, 06, 03, 09, 02, 08, 11 },
				{ 13, 11, 07, 14, 12, 01, 03, 09, 05, 00, 15, 04, 08, 06, 02, 10 },
				{ 06, 15, 14, 09, 11, 03, 00, 08, 12, 02, 13, 07, 01, 04, 10, 05 },
				{ 10, 02, 08, 04, 07, 06, 01, 05, 15, 11, 09, 14, 03, 12, 13, 00 }
				// No need to repeat the pattern.
			};

			// Array representing the 4x4 matrix.
			private uint[] matrix;
			// Hold onto the previous chain in order to generate the next chain after all the rounds are complete.
			private readonly uint[] oldChain;
			// The message block.
			private readonly uint[] message;

			public Blake2s_Matrix(uint[] chain, uint[]m, uint t0, uint t1, bool final) {
				// Assuming chain is 8 UInts long.
				// Assuming m i 16 UInts long.
				// Assuming m is 16 UInts long.
				oldChain = chain;
				matrix = new uint[16];
				Array.Copy(oldChain, 0, matrix, 0, 8);
				Array.Copy(InitialVector, 0, matrix, 8, 8);
				matrix[12] ^= t0;
				matrix[13] ^= t1;
				// If this is the final data block, then flip all bits in element 14.
				if(final)
					matrix[14] ^= 0xFFFFFFFF;
				message = m;
			}

			public override string ToString() {
				string tmp = "";
				foreach(uint m in matrix)
					tmp += m.ToString("X8") + " ";
				return tmp;
			}

			public void Rounds() {
				// Blake2s performs 10 full rounds.
				for(int z = 0; z < 10; z++)
					Round(z);
			}

			public void Round(int permutation) {
				// Column 1
				QuarterRound(00, 04, 08, 12, message[Permutations[permutation, 00]], message[Permutations[permutation, 01]]);
				// Column 2
				QuarterRound(01, 05, 09, 13, message[Permutations[permutation, 02]], message[Permutations[permutation, 03]]);
				// Column 3
				QuarterRound(02, 06, 10, 14, message[Permutations[permutation, 04]], message[Permutations[permutation, 05]]);
				// Column 4
				QuarterRound(03, 07, 11, 15, message[Permutations[permutation, 06]], message[Permutations[permutation, 07]]);
				// Diagonal 1
				QuarterRound(00, 05, 10, 15, message[Permutations[permutation, 08]], message[Permutations[permutation, 09]]);
				// Diagonal 2
				QuarterRound(01, 06, 11, 12, message[Permutations[permutation, 10]], message[Permutations[permutation, 11]]);
				// Diagonal 3
				QuarterRound(02, 07, 08, 13, message[Permutations[permutation, 12]], message[Permutations[permutation, 13]]);
				// Diagonal 4
				QuarterRound(03, 04, 09, 14, message[Permutations[permutation, 14]], message[Permutations[permutation, 15]]);
			}

			// Perform a quarter round of operations on the matrix (or, more practically, a column or diagonal in the matrix).  The caller is responsible for passing in the correct values from the message and constant.
			private void QuarterRound(int a, int b, int c, int d, uint m2I, uint m2I1) {
				// a = a + b + mor(2i)
				matrix[a] = matrix[a] + matrix[b] + m2I;
				// d = (d ^ a) >>> 16
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 16) | ((matrix[d] ^ matrix[a]) << (32 - 16));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 12
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 12) | ((matrix[b] ^ matrix[c]) << (32 - 12));
				// a = a + b + mor(2i+1)
				matrix[a] = matrix[a] + matrix[b] + m2I1;
				// d = (d ^ a) >>> 8
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 08) | ((matrix[d] ^ matrix[a]) << (32 - 08));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 7
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 07) | ((matrix[b] ^ matrix[c]) << (32 - 07));
			}

			// Generate the next chain value.  To be accessed after 10 full rounds.
			public uint[] GetNextChain() {
				uint[] newChain = new uint[8];
				for(int x = 0; x < 8; x++)
					newChain[x] = oldChain[x] ^ matrix[x] ^ matrix[x + 8];
				return newChain;
			}
		}
	}

	// 64-bit
	public static class Blake2b {
		// Constants
		// Initial Vector, which is no longer different for different length outputs (Blake used different IVs for, e.g., Blake-512 and Blake-384).
		private static readonly ulong[] InitialVector = new ulong[8] {
			0x6A09E667F3BCC908, 0xBB67AE8584CAA73B, 0x3C6EF372FE94F82B, 0xA54FF53A5F1D36F1, 0x510E527FADE682D1, 0x9B05688C2B3E6C1F, 0x1F83D9ABFB41BD6B, 0x5BE0CD19137E2179
		};
		private const int BlockSizeBytes = 128;
		private const int HalfBlockSizeBytes = 64;

		// At the moment we're only going to accept byte arrays.  There is a drawback with this approach: the algorithm is limited to ~2GB of data that can be hashed.  The specification states that it should be able to handle data up to 2 ^ 128 bytes.
		public static byte[] Hash(byte[] message, int hashLength = HalfBlockSizeBytes, byte[] key = null) {
			// Parameter Checking
			// message: non-null
			if(message == null)
				throw new ArgumentNullException("message", "Cannot hash a null message.  Provide a non-null message to hash.");
			// hashLength: between 1 and 64
			if(hashLength < 1 || hashLength > HalfBlockSizeBytes)
				throw new ArgumentOutOfRangeException("hashLength", hashLength, "The length of the output hash for Blake2b must be between 1 and 64 bytes long");
			// key: between 0 and 64 bytes long
			if(key != null && key.Length > HalfBlockSizeBytes)
				throw new ArgumentOutOfRangeException("key", key.Length, "Key must not exceed 64 bytes for Blake2b.  Provide key no longer than 64 bytes.");

			// Data Preparation
			// Determine what the length of the message is before padding.  This is utilized towards the end in the creation of the matrix for the last block of data.
			ulong length = (ulong)message.Length;
			// Determine the key length.  It is used in generating the initial chain value and it affects the counter used when creating Blake2s_Matrix objects.
			uint keyLength, keyBlockLength;
			if(key == null) {
				keyLength = 0;
				keyBlockLength = 0;
			}
			else {
				keyLength = (uint) key.Length;
				keyBlockLength = BlockSizeBytes;
				// Pad the key with 0s to the size of a full data block (64 bytes in Blake2s).
				byte[] tmp = new byte[BlockSizeBytes];
				Array.Copy(key, 0, tmp, 0, key.Length);
				key = tmp;
			}
			// Pad the message.
			int messageLengthWithPad = message.Length + ((BlockSizeBytes - (message.Length % BlockSizeBytes)) % BlockSizeBytes);
			Stream messageStream = new MemoryStream(message, 0, message.Length, false);
			// Generate the first Chain Value.
			ulong[] chainValue = new ulong[8];
			Array.Copy(InitialVector, 0, chainValue, 0, 8);
			chainValue[0] = chainValue[0] ^ 0x01010000 ^ (keyLength << 8) ^ ((ulong) hashLength);
			// Placeholders
			byte[] messageFractionBytes = new byte[BlockSizeBytes];
			ulong[] messageFraction = new ulong[16];

			// Data Processing
			// Break the (padded) message into 128-byte blocks, and process each block sequentially.
			for(int x = 0; x < messageLengthWithPad + keyBlockLength; x += BlockSizeBytes) {
				// Obtain the message block.  If a key was provided for the hash, then it is used as the first data block.  The message will follow.
				if(x == 0 && keyBlockLength != 0) {
					messageFractionBytes = key;
				}
				else {
					// Read the next 128 bytes from the MemoryStream.  If there are less than 128 bytes read from the MemoryStream, then fill the rest with 0s.
					int tmp = messageStream.Read(messageFractionBytes, 0, BlockSizeBytes);
					if(tmp < BlockSizeBytes) {
						for(int y = tmp; y < BlockSizeBytes; y++)
							messageFractionBytes[y] = 0;
					}
				}

				// Use the message block to build the array of unsigned integers.
				for(int y = 0; y < 16; y++)
					messageFraction[y] = BitConverter.ToUInt64(messageFractionBytes, y * 8);

				// Calculate the length in bytes of the message so far through this block.  If a key was provided for hashing, exclude the first block of data processed from the number of bytes.
				ulong lengthX = System.Math.Min(((ulong) x) - ((ulong) keyBlockLength) + BlockSizeBytes, (ulong) length);

				// Build our matrix, then run all the rounds necessary.
				Blake2s_Matrix matrix = new Blake2s_Matrix(chainValue, messageFraction, GetLower64Bits(lengthX), GetUpper64Bits(lengthX), (x == messageLengthWithPad + keyBlockLength - BlockSizeBytes));
				matrix.Rounds();

				// Get the next chain value; we'll use it in the next loop.
				chainValue = matrix.GetNextChain();
			}
			messageStream.Close();

			// Finalize
			// Convert the final chain value to bytes.  The final result is truncated to the number of bytes requested in the input parameters.
			byte[] tmpHash = new byte[HalfBlockSizeBytes];
			for(int x = 0; x < 8; x++) {
				byte[] tmp = BitConverter.GetBytes(chainValue[x]);
				Array.Copy(tmp, 0, tmpHash, x * 8, 8);
			}
			byte[] hash = new byte[hashLength];
			Array.Copy(tmpHash, 0, hash, 0, hashLength);
			return hash;
		}

		private static ulong GetLower64Bits(ulong t) {
			return t;
		}

		private static ulong GetUpper64Bits(ulong t) {
			return 0;
		}

		// Private class for modeling the 4x4 matrix of unsigned integers and the round of operations that are performed on them.
		private class Blake2s_Matrix {
			// Blake2b uses 12 rounds, so 12 permutations are needed.
			private static readonly int[,] Permutations = new int[12, 16] {
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 },
				{ 11, 08, 12, 00, 05, 02, 15, 13, 10, 14, 03, 06, 07, 01, 09, 04 },
				{ 07, 09, 03, 01, 13, 12, 11, 14, 02, 06, 05, 10, 04, 00, 15, 08 },
				{ 09, 00, 05, 07, 02, 04, 10, 15, 14, 01, 11, 12, 06, 08, 03, 13 },
				{ 02, 12, 06, 10, 00, 11, 08, 03, 04, 13, 07, 05, 15, 14, 01, 09 },
				{ 12, 05, 01, 15, 14, 13, 04, 10, 00, 07, 06, 03, 09, 02, 08, 11 },
				{ 13, 11, 07, 14, 12, 01, 03, 09, 05, 00, 15, 04, 08, 06, 02, 10 },
				{ 06, 15, 14, 09, 11, 03, 00, 08, 12, 02, 13, 07, 01, 04, 10, 05 },
				{ 10, 02, 08, 04, 07, 06, 01, 05, 15, 11, 09, 14, 03, 12, 13, 00 },
				// Start repeating the pattern...
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 }
			};

			// Array representing the 4x4 matrix.
			private ulong[] matrix;
			// Hold onto the previous chain in order to generate the next chain after all the rounds are complete.
			private readonly ulong[] oldChain;
			// The message block.
			private readonly ulong[] message;

			public Blake2s_Matrix(ulong[] chain, ulong[] m, ulong t0, ulong t1, bool final) {
				// Assuming chain is 8 ULongs long.
				// Assuming m i 16 ULongs long.
				// Assuming m is 16 ULongs long.
				oldChain = chain;
				matrix = new ulong[16];
				Array.Copy(oldChain, 0, matrix, 0, 8);
				Array.Copy(InitialVector, 0, matrix, 8, 8);
				matrix[12] ^= t0;
				matrix[13] ^= t1;
				// If this is the final data block, then flip all bits in element 14.
				if(final)
					matrix[14] ^= 0xFFFFFFFFFFFFFFFF;
				message = m;
			}

			public override string ToString() {
				string tmp = "";
				foreach(ulong m in matrix)
					tmp += m.ToString("X16") + " ";
				return tmp;
			}

			public void Rounds() {
				// Blake2b performs 12 full rounds.
				for(int z = 0; z < 12; z++)
					Round(z);
			}

			public void Round(int permutation) {
				// Column 1
				QuarterRound(00, 04, 08, 12, message[Permutations[permutation, 00]], message[Permutations[permutation, 01]]);
				// Column 2
				QuarterRound(01, 05, 09, 13, message[Permutations[permutation, 02]], message[Permutations[permutation, 03]]);
				// Column 3
				QuarterRound(02, 06, 10, 14, message[Permutations[permutation, 04]], message[Permutations[permutation, 05]]);
				// Column 4
				QuarterRound(03, 07, 11, 15, message[Permutations[permutation, 06]], message[Permutations[permutation, 07]]);
				// Diagonal 1
				QuarterRound(00, 05, 10, 15, message[Permutations[permutation, 08]], message[Permutations[permutation, 09]]);
				// Diagonal 2
				QuarterRound(01, 06, 11, 12, message[Permutations[permutation, 10]], message[Permutations[permutation, 11]]);
				// Diagonal 3
				QuarterRound(02, 07, 08, 13, message[Permutations[permutation, 12]], message[Permutations[permutation, 13]]);
				// Diagonal 4
				QuarterRound(03, 04, 09, 14, message[Permutations[permutation, 14]], message[Permutations[permutation, 15]]);
			}

			// Perform a quarter round of operations on the matrix (or, more practically, a column or diagonal in the matrix).  The caller is responsible for passing in the correct values from the message and constant.
			private void QuarterRound(int a, int b, int c, int d, ulong m2I, ulong m2I1) {
				// a = a + b + mor(2i)
				matrix[a] = matrix[a] + matrix[b] + m2I;
				// d = (d ^ a) >>> 32
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 32) | ((matrix[d] ^ matrix[a]) << (64 - 32));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 24
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 24) | ((matrix[b] ^ matrix[c]) << (64 - 24));
				// a = a + b + mor(2i+1)
				matrix[a] = matrix[a] + matrix[b] + m2I1;
				// d = (d ^ a) >>> 16
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 16) | ((matrix[d] ^ matrix[a]) << (64 - 16));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 63
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 63) | ((matrix[b] ^ matrix[c]) << (64 - 63));
			}

			// Generate the next chain value.  To be accessed after 10 full rounds.
			public ulong[] GetNextChain() {
				ulong[] newChain = new ulong[8];
				for(int x = 0; x < 8; x++)
					newChain[x] = oldChain[x] ^ matrix[x] ^ matrix[x + 8];
				return newChain;
			}
		}
	}
}
