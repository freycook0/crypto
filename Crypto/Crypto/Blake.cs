using System;
using System.IO;

namespace Freycook0.Crypto {

	// Blake_32 is a "super-class" to be used by Blake_256 and Blake_224.  It implements most of the code, and the "sub-classes" provide the IV and the input parameters (message, salt).  There's no real inheritance utilized; its methods are called by the "sub-classes", but nothing else.
	public static class Blake_32 {
		// At the moment we're only going to accept byte arrays.  There are 2 drawbacks with this approach: 1) Data loads that are not divisible by eight cannot be correctly padded if they are fit into a byte array.  2) The algorithm is limited to ~4GB of data that can be hashed.  The specification states that it should be able to handle data up to ~2 exabytes.
		public static byte[] Hash(byte[] message, byte[] salt, uint[] initialVector, byte firstPadByte = 0b00000000, byte lastPadByte = 0b00000000) {
			// Parameter Checking
			// message: non-null
			if(message == null)
				throw new ArgumentNullException("message", "Cannot hash a null message.  Provide a non-null message to hash.");
			// salt: non-null, exactly 16 bytes (4 uints)
			if(salt == null)
				throw new ArgumentNullException("salt", "Cannot hash with a null salt.  Provide a non-null salt.");
			if(salt.Length != 16)
				throw new ArgumentOutOfRangeException("salt", salt.Length, "Salt must have a length of 16 bytes (4 unsigned integers).");
			// initialVector: non-null, exactly 8 uints
			if(initialVector == null)
				throw new ArgumentNullException("initialVector", "Cannot hash with a null initial vector.  Provide a non-null initial vector.");
			if(initialVector.Length != 8)
				throw new ArgumentOutOfRangeException("initialVector", initialVector.Length, "Initial vector must have a length of 8 unsigned integers.");

			// Data Preparation
			// Determine what the length of the message is before padding.  This is utilized towards the end in the creation of the matrix (or matrices) for the last block or two of data.
			ulong length = ((ulong) message.Length) * 8;
			// Pad our message.
			byte[] messagePad = GetMessagePad(message, firstPadByte, lastPadByte);
			int messageLengthWithPad = message.Length + messagePad.Length;
			Stream messageStream = new MemoryStream(message, 0, message.Length, false);
			Stream padStream = new MemoryStream(messagePad, 0, messagePad.Length, false);
			// Convert the salt to an array of uints.
			uint[] saltUInt = new uint[4];
			saltUInt[0] = BitConverter.ToUInt32(salt, 00);
			saltUInt[1] = BitConverter.ToUInt32(salt, 04);
			saltUInt[2] = BitConverter.ToUInt32(salt, 08);
			saltUInt[3] = BitConverter.ToUInt32(salt, 12);
			// Populate Chain Value with Initial Vector
			uint[] chainValue = new uint[8];
			Array.Copy(initialVector, 0, chainValue, 0, 8);
			// Placeholders
			byte[] messageFractionBytes = new byte[64];
			uint[] messageFraction = new uint[16];
			bool endOfStream = false;
			bool past = false;

			// Data Processing
			// Break the (padded) message into 512-bit (64-byte) blocks, and process each block sequntially.
			for(int x = 0; x < messageLengthWithPad; x += 64) {
				// Copy each block into a temporary array, as each block needs to be reversed before it is converted from bytes to unsigned integers.
				if(!endOfStream) {
					int tmp = messageStream.Read(messageFractionBytes, 0, 64);
					if(tmp < 64) {
						endOfStream = true;
						padStream.Read(messageFractionBytes, tmp, 64 - tmp);
					}
				}
				else
					padStream.Read(messageFractionBytes, 0, 64);
				// If the system is little Endian, then reverse the array, as each 4 bytes are to be converted to an unsigned integer using little-endian order.
				if(BitConverter.IsLittleEndian)
					Array.Reverse(messageFractionBytes);
				// Build the array of unsigned integers.
				for(int y = 0; y < 16; y++)
					messageFraction[y] = BitConverter.ToUInt32(messageFractionBytes, y * 4);
				// If the system is little Endian, then reverse the array of integers, in order to preserve the original order.
				if(BitConverter.IsLittleEndian)
					Array.Reverse(messageFraction);
				
				// Calculate the length (in bits) of the message so far through this block.  If the last block contains no bits from the original message, then the length for that block is zero.
				ulong lengthX = ((ulong) x) * 8 + 512;
				if(past)
					lengthX = 0;
				else if(lengthX >= length) {
					lengthX = length;
					past = true;
				}

				// Build our matrix, then run all the rounds necessary.
				Blake_32_Matrix matrix = new Blake_32_Matrix(chainValue, saltUInt, GetLower32Bits(lengthX), GetUpper32Bits(lengthX));
				matrix.Rounds(messageFraction);

				// Get the next chain value; we'll use it in the next loop.
				chainValue = matrix.GetNextChain();
			}
			messageStream.Close();
			padStream.Close();

			// Finalize
			// Convert the final chain value to bytes.  Each integer from the final chain value need to be reversed before they are converted to bytes, as they are little-endian.
			byte[] hash = new byte[32];
			for(int x = 0; x < 8; x++) {
				byte[] tmp = BitConverter.GetBytes(chainValue[x]);
				// If the system is little Endian, then reverse the bytes after each conversion from uint.
				if(BitConverter.IsLittleEndian)
					Array.Reverse(tmp);
				Array.Copy(tmp, 0, hash, x * 4, 4);
			}
			return hash;
		}

		// TODO: Decide if this overload is necessary.  It's not referenced by either Blake_256 or Blake_128.
		// Overload for the Hash function, which adds a salt of all zeroes when none is provided.
		public static byte[] Hash(byte[] message, uint[] initialVector, byte firstPadByte = 0b00000000, byte lastPadByte = 0b00000000) {
			byte[] salt = new byte[16];
			return Hash(message, salt, initialVector, firstPadByte, lastPadByte);
		}

		// Pad the message.  Because this method only accepts data in byte arrays, the pad will always have a multiple of 8 for a pad length.
		private static byte[] PadMessage(byte[] message, byte first = 0b00000000, byte last = 0b00000000) {
			if(message == null)
				throw new ArgumentNullException("message", "Cannot pad a null message.  Provide a non-null message to pad.");
			// Generate the padding for the message.
			byte[] pad = GetMessagePad(message, first, last);
			// Allocate new array with space for the padding.
			byte[] paddedMessage = new byte[message.Length + pad.Length];
			// Copy contents of message to the new array.
			Array.Copy(message, 0, paddedMessage, 0, message.Length);
			// Copy contents of the padding to the remaining space in the new array.
			Array.Copy(pad, 0, paddedMessage, message.Length, pad.Length);
			return paddedMessage;
		}

		// Get the pad generated for the given message.  Because this method only accepts data in byte arrays, the pad will always have a multiple of 8 for a pad length.  This method utilizes much of the same code as the original version of PadMessage().
		private static byte[] GetMessagePad(byte[] message, byte first = 0b00000000, byte last = 0b00000000) {
			if(message == null)
				throw new ArgumentNullException("message", "Cannot pad a null message.  Provide a non-null message to pad.");
			// Get the length of the message.
			int lengthBytes = message.Length;
			ulong lengthBits = (ulong)(message.Length * 8);
			// Get the # of bytes we need to pad the message by (not counting the 8 bytes used to pad the end with the length of the message).  The number calculated is a minimum of 1 and a maximum of 64.
			int padLengthBytes = 64 - ((lengthBytes + 8) % 64);
			// Allocate array to contain the padding.
			byte[] messagePad = new byte[padLengthBytes + 8];
			// Populate the padding array.  The padding consists of a 1 bit, multiple 0 bits, another 1 bit, then 64 bits (Big-Endian) representing the length (in bits) of the message.
			for(int x = 0; x < padLengthBytes; x++) {
				byte tmp = 0b00000000;
				if(x == 0)
					tmp |= first;
				if(x == (padLengthBytes - 1))
					tmp |= last;
				messagePad[x] = tmp;
			}
			byte[] bytesLengthBits = BitConverter.GetBytes(lengthBits);
			if(BitConverter.IsLittleEndian)
				Array.Reverse(bytesLengthBits);
			Array.Copy(bytesLengthBits, 0, messagePad, padLengthBytes, 8);
			return messagePad;
		}

		private static uint GetLower32Bits(ulong t) {
			return (uint) t;
		}

		private static uint GetUpper32Bits(ulong t) {
			return (uint) (t >> 32);
		}

		// Private class for modeling the 4x4 matrix of unsigned integers and the round of operations that are performed on them.
		private class Blake_32_Matrix {
			// Constants utilized in both populating matrices and during rounds of operations.
			private static readonly uint[] Constants = new uint[16] {
				0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344, 0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89,
				0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C, 0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917
			};
			// Blake-256 and Blake-224 use 14 rounds, so 14 permutations are needed.
			private static readonly int[,] Permutations = new int[14, 16] {
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 },
				{ 11, 08, 12, 00, 05, 02, 15, 13, 10, 14, 03, 06, 07, 01, 09, 04 },
				{ 07, 09, 03, 01, 13, 12, 11, 14, 02, 06, 05, 10, 04, 00, 15, 08 },
				{ 09, 00, 05, 07, 02, 04, 10, 15, 14, 01, 11, 12, 06, 08, 03, 13 },
				{ 02, 12, 06, 10, 00, 11, 08, 03, 04, 13, 07, 05, 15, 14, 01, 09 },
				{ 12, 05, 01, 15, 14, 13, 04, 10, 00, 07, 06, 03, 09, 02, 08, 11 },
				{ 13, 11, 07, 14, 12, 01, 03, 09, 05, 00, 15, 04, 08, 06, 02, 10 },
				{ 06, 15, 14, 09, 11, 03, 00, 08, 12, 02, 13, 07, 01, 04, 10, 05 },
				{ 10, 02, 08, 04, 07, 06, 01, 05, 15, 11, 09, 14, 03, 12, 13, 00 },
				// Start repeating the pattern...
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 },
				{ 11, 08, 12, 00, 05, 02, 15, 13, 10, 14, 03, 06, 07, 01, 09, 04 },
				{ 07, 09, 03, 01, 13, 12, 11, 14, 02, 06, 05, 10, 04, 00, 15, 08 }
			};
			// Complete 2D array with Constants, accessed through sigma permutations.
			/*private static readonly uint[,] ConstantsSigma = new uint[14, 16] {
				{ Constants[Permutations[00, 00]], Constants[Permutations[00, 01]], Constants[Permutations[00, 02]], Constants[Permutations[00, 03]], Constants[Permutations[00, 04]], Constants[Permutations[00, 05]], Constants[Permutations[00, 06]], Constants[Permutations[00, 07]], Constants[Permutations[00, 08]], Constants[Permutations[00, 09]], Constants[Permutations[00, 10]], Constants[Permutations[00, 11]], Constants[Permutations[00, 12]], Constants[Permutations[00, 13]], Constants[Permutations[00, 14]], Constants[Permutations[00, 15]] },
				{ Constants[Permutations[01, 00]], Constants[Permutations[01, 01]], Constants[Permutations[01, 02]], Constants[Permutations[01, 03]], Constants[Permutations[01, 04]], Constants[Permutations[01, 05]], Constants[Permutations[01, 06]], Constants[Permutations[01, 07]], Constants[Permutations[01, 08]], Constants[Permutations[01, 09]], Constants[Permutations[01, 10]], Constants[Permutations[01, 11]], Constants[Permutations[01, 12]], Constants[Permutations[01, 13]], Constants[Permutations[01, 14]], Constants[Permutations[01, 15]] },
				{ Constants[Permutations[02, 00]], Constants[Permutations[02, 01]], Constants[Permutations[02, 02]], Constants[Permutations[02, 03]], Constants[Permutations[02, 04]], Constants[Permutations[02, 05]], Constants[Permutations[02, 06]], Constants[Permutations[02, 07]], Constants[Permutations[02, 08]], Constants[Permutations[02, 09]], Constants[Permutations[02, 10]], Constants[Permutations[02, 11]], Constants[Permutations[02, 12]], Constants[Permutations[02, 13]], Constants[Permutations[02, 14]], Constants[Permutations[02, 15]] },
				{ Constants[Permutations[03, 00]], Constants[Permutations[03, 01]], Constants[Permutations[03, 02]], Constants[Permutations[03, 03]], Constants[Permutations[03, 04]], Constants[Permutations[03, 05]], Constants[Permutations[03, 06]], Constants[Permutations[03, 07]], Constants[Permutations[03, 08]], Constants[Permutations[03, 09]], Constants[Permutations[03, 10]], Constants[Permutations[03, 11]], Constants[Permutations[03, 12]], Constants[Permutations[03, 13]], Constants[Permutations[03, 14]], Constants[Permutations[03, 15]] },
				{ Constants[Permutations[04, 00]], Constants[Permutations[04, 01]], Constants[Permutations[04, 02]], Constants[Permutations[04, 03]], Constants[Permutations[04, 04]], Constants[Permutations[04, 05]], Constants[Permutations[04, 06]], Constants[Permutations[04, 07]], Constants[Permutations[04, 08]], Constants[Permutations[04, 09]], Constants[Permutations[04, 10]], Constants[Permutations[04, 11]], Constants[Permutations[04, 12]], Constants[Permutations[04, 13]], Constants[Permutations[04, 14]], Constants[Permutations[04, 15]] },
				{ Constants[Permutations[05, 00]], Constants[Permutations[05, 01]], Constants[Permutations[05, 02]], Constants[Permutations[05, 03]], Constants[Permutations[05, 04]], Constants[Permutations[05, 05]], Constants[Permutations[05, 06]], Constants[Permutations[05, 07]], Constants[Permutations[05, 08]], Constants[Permutations[05, 09]], Constants[Permutations[05, 10]], Constants[Permutations[05, 11]], Constants[Permutations[05, 12]], Constants[Permutations[05, 13]], Constants[Permutations[05, 14]], Constants[Permutations[05, 15]] },
				{ Constants[Permutations[06, 00]], Constants[Permutations[06, 01]], Constants[Permutations[06, 02]], Constants[Permutations[06, 03]], Constants[Permutations[06, 04]], Constants[Permutations[06, 05]], Constants[Permutations[06, 06]], Constants[Permutations[06, 07]], Constants[Permutations[06, 08]], Constants[Permutations[06, 09]], Constants[Permutations[06, 10]], Constants[Permutations[06, 11]], Constants[Permutations[06, 12]], Constants[Permutations[06, 13]], Constants[Permutations[06, 14]], Constants[Permutations[06, 15]] },
				{ Constants[Permutations[07, 00]], Constants[Permutations[07, 01]], Constants[Permutations[07, 02]], Constants[Permutations[07, 03]], Constants[Permutations[07, 04]], Constants[Permutations[07, 05]], Constants[Permutations[07, 06]], Constants[Permutations[07, 07]], Constants[Permutations[07, 08]], Constants[Permutations[07, 09]], Constants[Permutations[07, 10]], Constants[Permutations[07, 11]], Constants[Permutations[07, 12]], Constants[Permutations[07, 13]], Constants[Permutations[07, 14]], Constants[Permutations[07, 15]] },
				{ Constants[Permutations[08, 00]], Constants[Permutations[08, 01]], Constants[Permutations[08, 02]], Constants[Permutations[08, 03]], Constants[Permutations[08, 04]], Constants[Permutations[08, 05]], Constants[Permutations[08, 06]], Constants[Permutations[08, 07]], Constants[Permutations[08, 08]], Constants[Permutations[08, 09]], Constants[Permutations[08, 10]], Constants[Permutations[08, 11]], Constants[Permutations[08, 12]], Constants[Permutations[08, 13]], Constants[Permutations[08, 14]], Constants[Permutations[08, 15]] },
				{ Constants[Permutations[09, 00]], Constants[Permutations[09, 01]], Constants[Permutations[09, 02]], Constants[Permutations[09, 03]], Constants[Permutations[09, 04]], Constants[Permutations[09, 05]], Constants[Permutations[09, 06]], Constants[Permutations[09, 07]], Constants[Permutations[09, 08]], Constants[Permutations[09, 09]], Constants[Permutations[09, 10]], Constants[Permutations[09, 11]], Constants[Permutations[09, 12]], Constants[Permutations[09, 13]], Constants[Permutations[09, 14]], Constants[Permutations[09, 15]] },
				{ Constants[Permutations[10, 00]], Constants[Permutations[10, 01]], Constants[Permutations[10, 02]], Constants[Permutations[10, 03]], Constants[Permutations[10, 04]], Constants[Permutations[10, 05]], Constants[Permutations[10, 06]], Constants[Permutations[10, 07]], Constants[Permutations[10, 08]], Constants[Permutations[10, 09]], Constants[Permutations[10, 10]], Constants[Permutations[10, 11]], Constants[Permutations[10, 12]], Constants[Permutations[10, 13]], Constants[Permutations[10, 14]], Constants[Permutations[10, 15]] },
				{ Constants[Permutations[11, 00]], Constants[Permutations[11, 01]], Constants[Permutations[11, 02]], Constants[Permutations[11, 03]], Constants[Permutations[11, 04]], Constants[Permutations[11, 05]], Constants[Permutations[11, 06]], Constants[Permutations[11, 07]], Constants[Permutations[11, 08]], Constants[Permutations[11, 09]], Constants[Permutations[11, 10]], Constants[Permutations[11, 11]], Constants[Permutations[11, 12]], Constants[Permutations[11, 13]], Constants[Permutations[11, 14]], Constants[Permutations[11, 15]] },
				{ Constants[Permutations[12, 00]], Constants[Permutations[12, 01]], Constants[Permutations[12, 02]], Constants[Permutations[12, 03]], Constants[Permutations[12, 04]], Constants[Permutations[12, 05]], Constants[Permutations[12, 06]], Constants[Permutations[12, 07]], Constants[Permutations[12, 08]], Constants[Permutations[12, 09]], Constants[Permutations[12, 10]], Constants[Permutations[12, 11]], Constants[Permutations[12, 12]], Constants[Permutations[12, 13]], Constants[Permutations[12, 14]], Constants[Permutations[12, 15]] },
				{ Constants[Permutations[13, 00]], Constants[Permutations[13, 01]], Constants[Permutations[13, 02]], Constants[Permutations[13, 03]], Constants[Permutations[13, 04]], Constants[Permutations[13, 05]], Constants[Permutations[13, 06]], Constants[Permutations[13, 07]], Constants[Permutations[13, 08]], Constants[Permutations[13, 09]], Constants[Permutations[13, 10]], Constants[Permutations[13, 11]], Constants[Permutations[13, 12]], Constants[Permutations[13, 13]], Constants[Permutations[13, 14]], Constants[Permutations[13, 15]] }
			};*/
			private static readonly uint[,] ConstantsSigma = new uint[14, 16] {
				{ 0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344, 0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89, 0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C, 0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917 },
				{ 0x3F84D5B5, 0xBE5466CF, 0xA4093822, 0x452821E6, 0x38D01377, 0xB5470917, 0xC97C50DD, 0x082EFA98, 0x85A308D3, 0xC0AC29B7, 0x243F6A88, 0x13198A2E, 0x34E90C6C, 0xEC4E6C89, 0x299F31D0, 0x03707344 },
				{ 0x34E90C6C, 0x452821E6, 0xC0AC29B7, 0x243F6A88, 0x299F31D0, 0x13198A2E, 0xB5470917, 0xC97C50DD, 0xBE5466CF, 0x3F84D5B5, 0x03707344, 0x082EFA98, 0xEC4E6C89, 0x85A308D3, 0x38D01377, 0xA4093822 },
				{ 0xEC4E6C89, 0x38D01377, 0x03707344, 0x85A308D3, 0xC97C50DD, 0xC0AC29B7, 0x34E90C6C, 0x3F84D5B5, 0x13198A2E, 0x082EFA98, 0x299F31D0, 0xBE5466CF, 0xA4093822, 0x243F6A88, 0xB5470917, 0x452821E6 },
				{ 0x38D01377, 0x243F6A88, 0x299F31D0, 0xEC4E6C89, 0x13198A2E, 0xA4093822, 0xBE5466CF, 0xB5470917, 0x3F84D5B5, 0x85A308D3, 0x34E90C6C, 0xC0AC29B7, 0x082EFA98, 0x452821E6, 0x03707344, 0xC97C50DD },
				{ 0x13198A2E, 0xC0AC29B7, 0x082EFA98, 0xBE5466CF, 0x243F6A88, 0x34E90C6C, 0x452821E6, 0x03707344, 0xA4093822, 0xC97C50DD, 0xEC4E6C89, 0x299F31D0, 0xB5470917, 0x3F84D5B5, 0x85A308D3, 0x38D01377 },
				{ 0xC0AC29B7, 0x299F31D0, 0x85A308D3, 0xB5470917, 0x3F84D5B5, 0xC97C50DD, 0xA4093822, 0xBE5466CF, 0x243F6A88, 0xEC4E6C89, 0x082EFA98, 0x03707344, 0x38D01377, 0x13198A2E, 0x452821E6, 0x34E90C6C },
				{ 0xC97C50DD, 0x34E90C6C, 0xEC4E6C89, 0x3F84D5B5, 0xC0AC29B7, 0x85A308D3, 0x03707344, 0x38D01377, 0x299F31D0, 0x243F6A88, 0xB5470917, 0xA4093822, 0x452821E6, 0x082EFA98, 0x13198A2E, 0xBE5466CF },
				{ 0x082EFA98, 0xB5470917, 0x3F84D5B5, 0x38D01377, 0x34E90C6C, 0x03707344, 0x243F6A88, 0x452821E6, 0xC0AC29B7, 0x13198A2E, 0xC97C50DD, 0xEC4E6C89, 0x85A308D3, 0xA4093822, 0xBE5466CF, 0x299F31D0 },
				{ 0xBE5466CF, 0x13198A2E, 0x452821E6, 0xA4093822, 0xEC4E6C89, 0x082EFA98, 0x85A308D3, 0x299F31D0, 0xB5470917, 0x34E90C6C, 0x38D01377, 0x3F84D5B5, 0x03707344, 0xC0AC29B7, 0xC97C50DD, 0x243F6A88 },
				{ 0x243F6A88, 0x85A308D3, 0x13198A2E, 0x03707344, 0xA4093822, 0x299F31D0, 0x082EFA98, 0xEC4E6C89, 0x452821E6, 0x38D01377, 0xBE5466CF, 0x34E90C6C, 0xC0AC29B7, 0xC97C50DD, 0x3F84D5B5, 0xB5470917 },
				{ 0x3F84D5B5, 0xBE5466CF, 0xA4093822, 0x452821E6, 0x38D01377, 0xB5470917, 0xC97C50DD, 0x082EFA98, 0x85A308D3, 0xC0AC29B7, 0x243F6A88, 0x13198A2E, 0x34E90C6C, 0xEC4E6C89, 0x299F31D0, 0x03707344 },
				{ 0x34E90C6C, 0x452821E6, 0xC0AC29B7, 0x243F6A88, 0x299F31D0, 0x13198A2E, 0xB5470917, 0xC97C50DD, 0xBE5466CF, 0x3F84D5B5, 0x03707344, 0x082EFA98, 0xEC4E6C89, 0x85A308D3, 0x38D01377, 0xA4093822 },
				{ 0xEC4E6C89, 0x38D01377, 0x03707344, 0x85A308D3, 0xC97C50DD, 0xC0AC29B7, 0x34E90C6C, 0x3F84D5B5, 0x13198A2E, 0x082EFA98, 0x299F31D0, 0xBE5466CF, 0xA4093822, 0x243F6A88, 0xB5470917, 0x452821E6 }
			};

			// Array representing the 4x4 matrix.
			private uint[] matrix;
			// Hold onto the previous chain in order to generate the next chain after all the rounds are complete.
			private uint[] oldChain;
			// Hold onto the salt in order to generate the next chain after all the rounds are complete.
			private uint[] salt;

			public Blake_32_Matrix(uint[] chain, uint[] s, uint t0, uint t1) {
				// TODO: Decide if parameter arguments need to be checked.
				// Assuming chain is 8 UInts long.
				// Assuming s is 4 UInts long.
				// Assuming constant is 8+ UInts long.
				oldChain = chain;
				salt = new uint[4];
				Array.Copy(s, 0, salt, 0, 4);
				matrix = new uint[16];
				Array.Copy(oldChain, 0, matrix, 0, 8);
				matrix[08] = salt[00] ^ Constants[00];
				matrix[09] = salt[01] ^ Constants[01];
				matrix[10] = salt[02] ^ Constants[02];
				matrix[11] = salt[03] ^ Constants[03];
				matrix[12] = t0 ^ Constants[04];
				matrix[13] = t0 ^ Constants[05];
				matrix[14] = t1 ^ Constants[06];
				matrix[15] = t1 ^ Constants[07];
			}

			public override string ToString() {
				string tmp = "";
				foreach(uint m in matrix)
					tmp += m.ToString("X8") + " ";
				return tmp;
			}

			public void Rounds(uint[] messageFraction) {
				// Blake-256 and Blake-224 perform 14 full rounds.
				for(int z = 0; z < 14; z++)
					Round(messageFraction, z);
			}

			public void Round(uint[] message, int permutation) {
				// Assuming message is 16 UInts long.
				// Assuming constants is 16 UInts long.
				// Column 1
				QuarterRound(00, 04, 08, 12, message[Permutations[permutation, 00]], message[Permutations[permutation, 01]], ConstantsSigma[permutation, 00], ConstantsSigma[permutation, 01]);
				// Column 2
				QuarterRound(01, 05, 09, 13, message[Permutations[permutation, 02]], message[Permutations[permutation, 03]], ConstantsSigma[permutation, 02], ConstantsSigma[permutation, 03]);
				// Column 3
				QuarterRound(02, 06, 10, 14, message[Permutations[permutation, 04]], message[Permutations[permutation, 05]], ConstantsSigma[permutation, 04], ConstantsSigma[permutation, 05]);
				// Column 4
				QuarterRound(03, 07, 11, 15, message[Permutations[permutation, 06]], message[Permutations[permutation, 07]], ConstantsSigma[permutation, 06], ConstantsSigma[permutation, 07]);
				// Diagonal 1
				QuarterRound(00, 05, 10, 15, message[Permutations[permutation, 08]], message[Permutations[permutation, 09]], ConstantsSigma[permutation, 08], ConstantsSigma[permutation, 09]);
				// Diagonal 2
				QuarterRound(01, 06, 11, 12, message[Permutations[permutation, 10]], message[Permutations[permutation, 11]], ConstantsSigma[permutation, 10], ConstantsSigma[permutation, 11]);
				// Diagonal 3
				QuarterRound(02, 07, 08, 13, message[Permutations[permutation, 12]], message[Permutations[permutation, 13]], ConstantsSigma[permutation, 12], ConstantsSigma[permutation, 13]);
				// Diagonal 4
				QuarterRound(03, 04, 09, 14, message[Permutations[permutation, 14]], message[Permutations[permutation, 15]], ConstantsSigma[permutation, 14], ConstantsSigma[permutation, 15]);
			}

			// Perform a quarter round of operations on the matrix (or, more practically, a column or diagonal in the matrix).  The caller is responsible for passing in the correct values from the message and constant.
			private void QuarterRound(int a, int b, int c, int d, uint m2I, uint m2I1, uint c2I, uint c2I1) {
				// a = a + b + (mor(2i) ^ cor(2i+1))
				matrix[a] = matrix[a] + matrix[b] + (m2I ^ c2I1);
				// d = (d ^ a) >>> 16
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 16) | ((matrix[d] ^ matrix[a]) << (32 - 16));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 12
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 12) | ((matrix[b] ^ matrix[c]) << (32 - 12));
				// a = a + b + (mor(2i+1) ^ cor(2i)
				matrix[a] = matrix[a] + matrix[b] + (m2I1 ^ c2I);
				// d = (d ^ a) >>> 8
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 08) | ((matrix[d] ^ matrix[a]) << (32 - 08));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 7
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 07) | ((matrix[b] ^ matrix[c]) << (32 - 07));
			}

			// Generate the next chain value.  To be accessed after 14 full rounds.
			public uint[] GetNextChain() {
				uint[] newChain = new uint[8];
				for(int x = 0; x < 8; x++)
					newChain[x] = oldChain[x] ^ salt[x % 4] ^ matrix[x] ^ matrix[x + 8];
				return newChain;
			}
		}
	}

	// Compute 256-bit (32-byte) hash using the Blake-256 algorithm.
	public static class Blake_256 {
		public static readonly uint[] InitialVector = new uint[8] {
			0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
		};

		public static byte[] Hash(byte[] message, byte[] salt) {
			return Blake_32.Hash(message, salt, InitialVector, 0b10000000, 0b00000001);
		}

		// Overload for the Hash function, which adds a salt of all zeroes when none is provided.
		public static byte[] Hash(byte[] message) {
			byte[] salt = new byte[16];
			return Hash(message, salt);
		}
	}

	// Compute 224-bit (28-byte) hash using the Blake-224 algorithm.
	public static class Blake_224 {
		public static readonly uint[] InitialVector = new uint[8] {
			0xC1059ED8, 0x367CD507, 0x3070DD17, 0xF70E5939, 0xFFC00B31, 0x68581511, 0x64F98FA7, 0xBEFA4FA4
		};

		public static byte[] Hash(byte[] message, byte[] salt) {
			byte[] hash = new byte[28];
			Array.Copy(Blake_32.Hash(message, salt, InitialVector, 0b10000000, 0b00000000), 0, hash, 0, 28);
			return hash;
		}

		// Overload for the Hash function, which adds a salt of all zeroes when none is provided.
		public static byte[] Hash(byte[] message) {
			byte[] salt = new byte[16];
			return Hash(message, salt);
		}
	}

	// https://stackoverflow.com/questions/47830510/2d-array-with-more-than-655352-elements-array-dimensions-exceeded-supported

	// Blake_64 is a "super-class" to be used by Blake_512 and Blake_384.  It implements most of the code, and the "sub-classes" provide the IV and the input parameters (message, salt).  There's no real inheritance utilized; its methods are called by the "sub-classes", but nothing else.
	public static class Blake_64 {
		// At the moment we're only going to accept byte arrays.  Not sure what is the best method of handling a bit string that doesn't conform to bytes perfectly.
		public static byte[] Hash(byte[] message, byte[] salt, ulong[] initialVector, byte firstPadByte = 0b00000000, byte lastPadByte = 0b00000000) {
			// Parameter Checking
			// message: non-null
			if(message == null) {
				throw new ArgumentNullException("message", "Cannot hash a null message.  Provide a non-null message to hash.");
			}
			// salt: non-null, exactly 32 bytes (4 ulongs)
			if(salt == null) {
				throw new ArgumentNullException("salt", "Cannot hash with a null salt.  Provide a non-null salt.");
			}
			else if(salt.Length != 32) {
				throw new ArgumentOutOfRangeException("salt", salt.Length, "Salt must have a length of 16 bytes (4 unsigned integers).");
			}
			// initialVector: non-null, exactly 8 uints
			if(initialVector == null) {
				throw new ArgumentNullException("initialVector", "Cannot hash with a null initial vector.  Provide a non-null initial vector.");
			}
			else if(initialVector.Length != 8) {
				throw new ArgumentOutOfRangeException("initialVector", initialVector.Length, "Initial vector must have a length of 8 unsigned integers.");
			}

			// Data Preparation
			// Determine what the length of the message is before padding.  This is utilized towards the end in the creation of the matrix (or matrices) for the last block or two of data.
			ulong length = ((ulong) message.Length) * 8;
			// Pad our message.
			byte[] messagePad = GetMessagePad(message, firstPadByte, lastPadByte);
			int messageLengthWithPad = message.Length + messagePad.Length;
			Stream messageStream = new MemoryStream(message, 0, message.Length, false);
			Stream padStream = new MemoryStream(messagePad, 0, messagePad.Length, false);
			// Convert the salt to an array of uints.
			ulong[] saltUInt = new ulong[4];
			saltUInt[0] = BitConverter.ToUInt64(salt, 00);
			saltUInt[1] = BitConverter.ToUInt64(salt, 08);
			saltUInt[2] = BitConverter.ToUInt64(salt, 16);
			saltUInt[3] = BitConverter.ToUInt64(salt, 24);
			// Populate Chain Value with Initial Vector
			ulong[] chainValue = new ulong[8];
			Array.Copy(initialVector, 0, chainValue, 0, 8);
			// Placeholders
			byte[] messageFractionBytes = new byte[128];
			ulong[] messageFraction = new ulong[16];
			bool endOfStream = false;
			bool past = false;

			// Data Processing
			// Break the (padded) message into 512-bit (64-byte) blocks, and process each block sequntially.
			for(int x = 0; x < messageLengthWithPad; x += 128) {
				// Copy the relevant block into the temporary array.  On little-endian systems, each block needs to be reversed before it is converted from bytes to unsigned integers.  If the 
				if(!endOfStream) {
					int tmp = messageStream.Read(messageFractionBytes, 0, 128);
					if(tmp < 128) {
						endOfStream = true;
						padStream.Read(messageFractionBytes, tmp, 128 - tmp);
					}
				}
				else
					padStream.Read(messageFractionBytes, 0, 128);
				// If the system is little Endian, then reverse the array, as each 4 bytes are to be converted to an unsigned integer using little-endian order.
				if(BitConverter.IsLittleEndian)
					Array.Reverse(messageFractionBytes);
				// Build the array of unsigned integers.
				for(int y = 0; y < 16; y++)
					messageFraction[y] = BitConverter.ToUInt64(messageFractionBytes, y * 8);
				// If the system is little Endian, then reverse the array of integers, in order to preserve the original order.
				if(BitConverter.IsLittleEndian)
					Array.Reverse(messageFraction);

				// Calculate the length (in bits) of the message so far through this block.  If the last block contains no bits from the original message, then the length for that block is zero.
				ulong lengthX = ((ulong) x) * 8 + 1024;
				if(past)
					lengthX = 0;
				else if(lengthX >= length) {
					lengthX = length;
					past = true;
				}

				// Build our matrix, then run all the rounds necessary.
				Blake_64_Matrix matrix = new Blake_64_Matrix(chainValue, saltUInt, GetLower64Bits(lengthX), GetUpper64Bits(lengthX));
				matrix.Rounds(messageFraction);

				// Get the next chain value; we'll use it in the next loop.
				chainValue = matrix.GetNextChain();
			}
			messageStream.Close();
			padStream.Close();

			// Finalize
			// Convert the final chain value to bytes.  Each integer from the final chain value need to be reversed before they are converted to bytes, as they are little-endian.
			byte[] hash = new byte[64];
			for(int x = 0; x < 8; x++) {
				byte[] tmp = BitConverter.GetBytes(chainValue[x]);
				// If the system is little Endian, then reverse the bytes after each conversion from uint.
				if(BitConverter.IsLittleEndian)
					Array.Reverse(tmp);
				Array.Copy(tmp, 0, hash, x * 8, 8);
			}
			return hash;
		}

		// Overload for the Hash function, which adds a salt of all zeroes when none is provided.
		public static byte[] Hash(byte[] message, ulong[] initialVector, byte firstPadByte = 0b00000000, byte lastPadByte = 0b00000000) {
			byte[] salt = new byte[32];
			return Hash(message, salt, initialVector, firstPadByte, lastPadByte);
		}

		// Pad the message.  Because this method only accepts data in byte arrays, the pad will always have a multiple of 8 for a pad length.
		private static byte[] PadMessage(byte[] message, byte first = 0b00000000, byte last = 0b00000000) {
			if(message == null)
				throw new ArgumentNullException("message", "Cannot pad a null message.  Provide a non-null message to pad.");
			// Generate the padding for the message.
			byte[] pad = GetMessagePad(message, first, last);
			// Original code: Allocate brand new array and put the message + padding in it.
			// Allocate new array with space for the padding.
			byte[] paddedMessage = new byte[message.Length + pad.Length];
			// Temporary code: Test resizing the original message to add the pad.  It was hoped that this approach would solve the issue where the RAM used by the Blake algorithms is 2 times the message.
			// The code works in that all tests still pass, but it does not fix the memory issue mentioned.
			/*byte[] paddedMessage = message;
			Array.Resize(ref paddedMessage, message.Length + pad.Length);*/
			// Copy contents of message to the new array.
			Array.Copy(message, 0, paddedMessage, 0, message.Length);
			// Copy contents of the padding to the remaining space in the new array.
			Array.Copy(pad, 0, paddedMessage, message.Length, pad.Length);
			return paddedMessage;
		}

		// Get the pad generated for the given message.  Because this method only accepts data in byte arrays, the pad will always have a multiple of 8 for a pad length.  This method utilizes much of the same code as the original version of PadMessage().
		private static byte[] GetMessagePad(byte[] message, byte first = 0b00000000, byte last = 0b00000000) {
			if(message == null)
				throw new ArgumentNullException("message", "Cannot pad a null message.  Provide a non-null message to pad.");
			// Get the length of the message.
			int lengthBytes = message.Length;
			ulong lengthBits = ((ulong)message.Length) * 8;
			// Get the # of bytes we need to pad the message by (not counting the 16 bytes used to pad the end with the length of the message).  The number calculated is a minimum of 1 and a maximum of 64.
			int padLengthBytes = 128 - ((lengthBytes + 16) % 128);
			// Allocate array to contain the padding.
			byte[] messagePad = new byte[padLengthBytes + 16];
			// Populate the padding array.  The padding consists of a 1 bit, multiple 0 bits, another 1 bit, then 128 bits (Big-Endian) representing the length (in bits) of the message.
			for(int x = 0; x < padLengthBytes; x++) {
				byte tmp = 0b00000000;
				if(x == 0)
					tmp |= first;
				if(x == (padLengthBytes - 1))
					tmp |= last;
				messagePad[x] = tmp;
			}
			byte[] bytesLengthBits = BitConverter.GetBytes(lengthBits);
			if(BitConverter.IsLittleEndian)
				Array.Reverse(bytesLengthBits);
			// This is a break-fix for an issue with the fact that we're not using a 128-bit unsigned integer to track how much data we're hashing.
			// TODO: Change this back later after 128-bit unsigned integers are implemented/in use.
			//Array.Copy(bytesLengthBits, 0, messagePad, padLengthBytes, 8);
			Array.Copy(bytesLengthBits, 0, messagePad, padLengthBytes + 8, 8);
			return messagePad;
		}

		private static ulong GetLower64Bits(ulong t) {
			return t;
		}

		private static ulong GetUpper64Bits(ulong t) {
			return 0;
		}

		// Private class for modeling the 4x4 matrix of (64-bit) unsigned integers and the round of operations that are performed on them.
		private class Blake_64_Matrix {
			private static readonly ulong[] Constants = new ulong[16] {
				0x243F6A8885A308D3, 0x13198A2E03707344, 0xA4093822299F31D0, 0x082EFA98EC4E6C89, 0x452821E638D01377, 0xBE5466CF34E90C6C, 0xC0AC29B7C97C50DD, 0x3F84D5B5B5470917,
				0x9216D5D98979FB1B, 0xD1310BA698DFB5AC, 0x2FFD72DBD01ADFB7, 0xB8E1AFED6A267E96, 0xBA7C9045F12C7F99, 0x24A19947B3916CF7, 0x0801F2E2858EFC16, 0x636920D871574E69
			};
			//	Blake-512 and Blake-384 use 16 rounds, so 16 permutations are needed.
			private static readonly int[,] Permutations = new int[16, 16] {
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 },
				{ 11, 08, 12, 00, 05, 02, 15, 13, 10, 14, 03, 06, 07, 01, 09, 04 },
				{ 07, 09, 03, 01, 13, 12, 11, 14, 02, 06, 05, 10, 04, 00, 15, 08 },
				{ 09, 00, 05, 07, 02, 04, 10, 15, 14, 01, 11, 12, 06, 08, 03, 13 },
				{ 02, 12, 06, 10, 00, 11, 08, 03, 04, 13, 07, 05, 15, 14, 01, 09 },
				{ 12, 05, 01, 15, 14, 13, 04, 10, 00, 07, 06, 03, 09, 02, 08, 11 },
				{ 13, 11, 07, 14, 12, 01, 03, 09, 05, 00, 15, 04, 08, 06, 02, 10 },
				{ 06, 15, 14, 09, 11, 03, 00, 08, 12, 02, 13, 07, 01, 04, 10, 05 },
				{ 10, 02, 08, 04, 07, 06, 01, 05, 15, 11, 09, 14, 03, 12, 13, 00 },
				// Start repeating the pattern...
				{ 00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15 },
				{ 14, 10, 04, 08, 09, 15, 13, 06, 01, 12, 00, 02, 11, 07, 05, 03 },
				{ 11, 08, 12, 00, 05, 02, 15, 13, 10, 14, 03, 06, 07, 01, 09, 04 },
				{ 07, 09, 03, 01, 13, 12, 11, 14, 02, 06, 05, 10, 04, 00, 15, 08 },
				{ 09, 00, 05, 07, 02, 04, 10, 15, 14, 01, 11, 12, 06, 08, 03, 13 },
				{ 02, 12, 06, 10, 00, 11, 08, 03, 04, 13, 07, 05, 15, 14, 01, 09 }
			};
			// Complete 2D array with Constants, accessed through sigma permutations.
			/*private static readonly ulong[,] ConstantsSigma = new ulong[16, 16] {
				{ Constants[Permutations[00, 00]], Constants[Permutations[00, 01]], Constants[Permutations[00, 02]], Constants[Permutations[00, 03]], Constants[Permutations[00, 04]], Constants[Permutations[00, 05]], Constants[Permutations[00, 06]], Constants[Permutations[00, 07]], Constants[Permutations[00, 08]], Constants[Permutations[00, 09]], Constants[Permutations[00, 10]], Constants[Permutations[00, 11]], Constants[Permutations[00, 12]], Constants[Permutations[00, 13]], Constants[Permutations[00, 14]], Constants[Permutations[00, 15]] },
				{ Constants[Permutations[01, 00]], Constants[Permutations[01, 01]], Constants[Permutations[01, 02]], Constants[Permutations[01, 03]], Constants[Permutations[01, 04]], Constants[Permutations[01, 05]], Constants[Permutations[01, 06]], Constants[Permutations[01, 07]], Constants[Permutations[01, 08]], Constants[Permutations[01, 09]], Constants[Permutations[01, 10]], Constants[Permutations[01, 11]], Constants[Permutations[01, 12]], Constants[Permutations[01, 13]], Constants[Permutations[01, 14]], Constants[Permutations[01, 15]] },
				{ Constants[Permutations[02, 00]], Constants[Permutations[02, 01]], Constants[Permutations[02, 02]], Constants[Permutations[02, 03]], Constants[Permutations[02, 04]], Constants[Permutations[02, 05]], Constants[Permutations[02, 06]], Constants[Permutations[02, 07]], Constants[Permutations[02, 08]], Constants[Permutations[02, 09]], Constants[Permutations[02, 10]], Constants[Permutations[02, 11]], Constants[Permutations[02, 12]], Constants[Permutations[02, 13]], Constants[Permutations[02, 14]], Constants[Permutations[02, 15]] },
				{ Constants[Permutations[03, 00]], Constants[Permutations[03, 01]], Constants[Permutations[03, 02]], Constants[Permutations[03, 03]], Constants[Permutations[03, 04]], Constants[Permutations[03, 05]], Constants[Permutations[03, 06]], Constants[Permutations[03, 07]], Constants[Permutations[03, 08]], Constants[Permutations[03, 09]], Constants[Permutations[03, 10]], Constants[Permutations[03, 11]], Constants[Permutations[03, 12]], Constants[Permutations[03, 13]], Constants[Permutations[03, 14]], Constants[Permutations[03, 15]] },
				{ Constants[Permutations[04, 00]], Constants[Permutations[04, 01]], Constants[Permutations[04, 02]], Constants[Permutations[04, 03]], Constants[Permutations[04, 04]], Constants[Permutations[04, 05]], Constants[Permutations[04, 06]], Constants[Permutations[04, 07]], Constants[Permutations[04, 08]], Constants[Permutations[04, 09]], Constants[Permutations[04, 10]], Constants[Permutations[04, 11]], Constants[Permutations[04, 12]], Constants[Permutations[04, 13]], Constants[Permutations[04, 14]], Constants[Permutations[04, 15]] },
				{ Constants[Permutations[05, 00]], Constants[Permutations[05, 01]], Constants[Permutations[05, 02]], Constants[Permutations[05, 03]], Constants[Permutations[05, 04]], Constants[Permutations[05, 05]], Constants[Permutations[05, 06]], Constants[Permutations[05, 07]], Constants[Permutations[05, 08]], Constants[Permutations[05, 09]], Constants[Permutations[05, 10]], Constants[Permutations[05, 11]], Constants[Permutations[05, 12]], Constants[Permutations[05, 13]], Constants[Permutations[05, 14]], Constants[Permutations[05, 15]] },
				{ Constants[Permutations[06, 00]], Constants[Permutations[06, 01]], Constants[Permutations[06, 02]], Constants[Permutations[06, 03]], Constants[Permutations[06, 04]], Constants[Permutations[06, 05]], Constants[Permutations[06, 06]], Constants[Permutations[06, 07]], Constants[Permutations[06, 08]], Constants[Permutations[06, 09]], Constants[Permutations[06, 10]], Constants[Permutations[06, 11]], Constants[Permutations[06, 12]], Constants[Permutations[06, 13]], Constants[Permutations[06, 14]], Constants[Permutations[06, 15]] },
				{ Constants[Permutations[07, 00]], Constants[Permutations[07, 01]], Constants[Permutations[07, 02]], Constants[Permutations[07, 03]], Constants[Permutations[07, 04]], Constants[Permutations[07, 05]], Constants[Permutations[07, 06]], Constants[Permutations[07, 07]], Constants[Permutations[07, 08]], Constants[Permutations[07, 09]], Constants[Permutations[07, 10]], Constants[Permutations[07, 11]], Constants[Permutations[07, 12]], Constants[Permutations[07, 13]], Constants[Permutations[07, 14]], Constants[Permutations[07, 15]] },
				{ Constants[Permutations[08, 00]], Constants[Permutations[08, 01]], Constants[Permutations[08, 02]], Constants[Permutations[08, 03]], Constants[Permutations[08, 04]], Constants[Permutations[08, 05]], Constants[Permutations[08, 06]], Constants[Permutations[08, 07]], Constants[Permutations[08, 08]], Constants[Permutations[08, 09]], Constants[Permutations[08, 10]], Constants[Permutations[08, 11]], Constants[Permutations[08, 12]], Constants[Permutations[08, 13]], Constants[Permutations[08, 14]], Constants[Permutations[08, 15]] },
				{ Constants[Permutations[09, 00]], Constants[Permutations[09, 01]], Constants[Permutations[09, 02]], Constants[Permutations[09, 03]], Constants[Permutations[09, 04]], Constants[Permutations[09, 05]], Constants[Permutations[09, 06]], Constants[Permutations[09, 07]], Constants[Permutations[09, 08]], Constants[Permutations[09, 09]], Constants[Permutations[09, 10]], Constants[Permutations[09, 11]], Constants[Permutations[09, 12]], Constants[Permutations[09, 13]], Constants[Permutations[09, 14]], Constants[Permutations[09, 15]] },
				{ Constants[Permutations[10, 00]], Constants[Permutations[10, 01]], Constants[Permutations[10, 02]], Constants[Permutations[10, 03]], Constants[Permutations[10, 04]], Constants[Permutations[10, 05]], Constants[Permutations[10, 06]], Constants[Permutations[10, 07]], Constants[Permutations[10, 08]], Constants[Permutations[10, 09]], Constants[Permutations[10, 10]], Constants[Permutations[10, 11]], Constants[Permutations[10, 12]], Constants[Permutations[10, 13]], Constants[Permutations[10, 14]], Constants[Permutations[10, 15]] },
				{ Constants[Permutations[11, 00]], Constants[Permutations[11, 01]], Constants[Permutations[11, 02]], Constants[Permutations[11, 03]], Constants[Permutations[11, 04]], Constants[Permutations[11, 05]], Constants[Permutations[11, 06]], Constants[Permutations[11, 07]], Constants[Permutations[11, 08]], Constants[Permutations[11, 09]], Constants[Permutations[11, 10]], Constants[Permutations[11, 11]], Constants[Permutations[11, 12]], Constants[Permutations[11, 13]], Constants[Permutations[11, 14]], Constants[Permutations[11, 15]] },
				{ Constants[Permutations[12, 00]], Constants[Permutations[12, 01]], Constants[Permutations[12, 02]], Constants[Permutations[12, 03]], Constants[Permutations[12, 04]], Constants[Permutations[12, 05]], Constants[Permutations[12, 06]], Constants[Permutations[12, 07]], Constants[Permutations[12, 08]], Constants[Permutations[12, 09]], Constants[Permutations[12, 10]], Constants[Permutations[12, 11]], Constants[Permutations[12, 12]], Constants[Permutations[12, 13]], Constants[Permutations[12, 14]], Constants[Permutations[12, 15]] },
				{ Constants[Permutations[13, 00]], Constants[Permutations[13, 01]], Constants[Permutations[13, 02]], Constants[Permutations[13, 03]], Constants[Permutations[13, 04]], Constants[Permutations[13, 05]], Constants[Permutations[13, 06]], Constants[Permutations[13, 07]], Constants[Permutations[13, 08]], Constants[Permutations[13, 09]], Constants[Permutations[13, 10]], Constants[Permutations[13, 11]], Constants[Permutations[13, 12]], Constants[Permutations[13, 13]], Constants[Permutations[13, 14]], Constants[Permutations[13, 15]] },
				{ Constants[Permutations[14, 00]], Constants[Permutations[14, 01]], Constants[Permutations[14, 02]], Constants[Permutations[14, 03]], Constants[Permutations[14, 04]], Constants[Permutations[14, 05]], Constants[Permutations[14, 06]], Constants[Permutations[14, 07]], Constants[Permutations[14, 08]], Constants[Permutations[14, 09]], Constants[Permutations[14, 10]], Constants[Permutations[14, 11]], Constants[Permutations[14, 12]], Constants[Permutations[14, 13]], Constants[Permutations[14, 14]], Constants[Permutations[14, 15]] },
				{ Constants[Permutations[15, 00]], Constants[Permutations[15, 01]], Constants[Permutations[15, 02]], Constants[Permutations[15, 03]], Constants[Permutations[15, 04]], Constants[Permutations[15, 05]], Constants[Permutations[15, 06]], Constants[Permutations[15, 07]], Constants[Permutations[15, 08]], Constants[Permutations[15, 09]], Constants[Permutations[15, 10]], Constants[Permutations[15, 11]], Constants[Permutations[15, 12]], Constants[Permutations[15, 13]], Constants[Permutations[15, 14]], Constants[Permutations[15, 15]] }
			};*/
			private static readonly ulong[,] ConstantsSigma = new ulong[16, 16] {
				{ 0x243F6A8885A308D3, 0x13198A2E03707344, 0xA4093822299F31D0, 0x082EFA98EC4E6C89, 0x452821E638D01377, 0xBE5466CF34E90C6C, 0xC0AC29B7C97C50DD, 0x3F84D5B5B5470917, 0x9216D5D98979FB1B, 0xD1310BA698DFB5AC, 0x2FFD72DBD01ADFB7, 0xB8E1AFED6A267E96, 0xBA7C9045F12C7F99, 0x24A19947B3916CF7, 0x0801F2E2858EFC16, 0x636920D871574E69 },
				{ 0x0801F2E2858EFC16, 0x2FFD72DBD01ADFB7, 0x452821E638D01377, 0x9216D5D98979FB1B, 0xD1310BA698DFB5AC, 0x636920D871574E69, 0x24A19947B3916CF7, 0xC0AC29B7C97C50DD, 0x13198A2E03707344, 0xBA7C9045F12C7F99, 0x243F6A8885A308D3, 0xA4093822299F31D0, 0xB8E1AFED6A267E96, 0x3F84D5B5B5470917, 0xBE5466CF34E90C6C, 0x082EFA98EC4E6C89 },
				{ 0xB8E1AFED6A267E96, 0x9216D5D98979FB1B, 0xBA7C9045F12C7F99, 0x243F6A8885A308D3, 0xBE5466CF34E90C6C, 0xA4093822299F31D0, 0x636920D871574E69, 0x24A19947B3916CF7, 0x2FFD72DBD01ADFB7, 0x0801F2E2858EFC16, 0x082EFA98EC4E6C89, 0xC0AC29B7C97C50DD, 0x3F84D5B5B5470917, 0x13198A2E03707344, 0xD1310BA698DFB5AC, 0x452821E638D01377 },
				{ 0x3F84D5B5B5470917, 0xD1310BA698DFB5AC, 0x082EFA98EC4E6C89, 0x13198A2E03707344, 0x24A19947B3916CF7, 0xBA7C9045F12C7F99, 0xB8E1AFED6A267E96, 0x0801F2E2858EFC16, 0xA4093822299F31D0, 0xC0AC29B7C97C50DD, 0xBE5466CF34E90C6C, 0x2FFD72DBD01ADFB7, 0x452821E638D01377, 0x243F6A8885A308D3, 0x636920D871574E69, 0x9216D5D98979FB1B },
				{ 0xD1310BA698DFB5AC, 0x243F6A8885A308D3, 0xBE5466CF34E90C6C, 0x3F84D5B5B5470917, 0xA4093822299F31D0, 0x452821E638D01377, 0x2FFD72DBD01ADFB7, 0x636920D871574E69, 0x0801F2E2858EFC16, 0x13198A2E03707344, 0xB8E1AFED6A267E96, 0xBA7C9045F12C7F99, 0xC0AC29B7C97C50DD, 0x9216D5D98979FB1B, 0x082EFA98EC4E6C89, 0x24A19947B3916CF7 },
				{ 0xA4093822299F31D0, 0xBA7C9045F12C7F99, 0xC0AC29B7C97C50DD, 0x2FFD72DBD01ADFB7, 0x243F6A8885A308D3, 0xB8E1AFED6A267E96, 0x9216D5D98979FB1B, 0x082EFA98EC4E6C89, 0x452821E638D01377, 0x24A19947B3916CF7, 0x3F84D5B5B5470917, 0xBE5466CF34E90C6C, 0x636920D871574E69, 0x0801F2E2858EFC16, 0x13198A2E03707344, 0xD1310BA698DFB5AC },
				{ 0xBA7C9045F12C7F99, 0xBE5466CF34E90C6C, 0x13198A2E03707344, 0x636920D871574E69, 0x0801F2E2858EFC16, 0x24A19947B3916CF7, 0x452821E638D01377, 0x2FFD72DBD01ADFB7, 0x243F6A8885A308D3, 0x3F84D5B5B5470917, 0xC0AC29B7C97C50DD, 0x082EFA98EC4E6C89, 0xD1310BA698DFB5AC, 0xA4093822299F31D0, 0x9216D5D98979FB1B, 0xB8E1AFED6A267E96 },
				{ 0x24A19947B3916CF7, 0xB8E1AFED6A267E96, 0x3F84D5B5B5470917, 0x0801F2E2858EFC16, 0xBA7C9045F12C7F99, 0x13198A2E03707344, 0x082EFA98EC4E6C89, 0xD1310BA698DFB5AC, 0xBE5466CF34E90C6C, 0x243F6A8885A308D3, 0x636920D871574E69, 0x452821E638D01377, 0x9216D5D98979FB1B, 0xC0AC29B7C97C50DD, 0xA4093822299F31D0, 0x2FFD72DBD01ADFB7 },
				{ 0xC0AC29B7C97C50DD, 0x636920D871574E69, 0x0801F2E2858EFC16, 0xD1310BA698DFB5AC, 0xB8E1AFED6A267E96, 0x082EFA98EC4E6C89, 0x243F6A8885A308D3, 0x9216D5D98979FB1B, 0xBA7C9045F12C7F99, 0xA4093822299F31D0, 0x24A19947B3916CF7, 0x3F84D5B5B5470917, 0x13198A2E03707344, 0x452821E638D01377, 0x2FFD72DBD01ADFB7, 0xBE5466CF34E90C6C },
				{ 0x2FFD72DBD01ADFB7, 0xA4093822299F31D0, 0x9216D5D98979FB1B, 0x452821E638D01377, 0x3F84D5B5B5470917, 0xC0AC29B7C97C50DD, 0x13198A2E03707344, 0xBE5466CF34E90C6C, 0x636920D871574E69, 0xB8E1AFED6A267E96, 0xD1310BA698DFB5AC, 0x0801F2E2858EFC16, 0x082EFA98EC4E6C89, 0xBA7C9045F12C7F99, 0x24A19947B3916CF7, 0x243F6A8885A308D3 },
				{ 0x243F6A8885A308D3, 0x13198A2E03707344, 0xA4093822299F31D0, 0x082EFA98EC4E6C89, 0x452821E638D01377, 0xBE5466CF34E90C6C, 0xC0AC29B7C97C50DD, 0x3F84D5B5B5470917, 0x9216D5D98979FB1B, 0xD1310BA698DFB5AC, 0x2FFD72DBD01ADFB7, 0xB8E1AFED6A267E96, 0xBA7C9045F12C7F99, 0x24A19947B3916CF7, 0x0801F2E2858EFC16, 0x636920D871574E69 },
				{ 0x0801F2E2858EFC16, 0x2FFD72DBD01ADFB7, 0x452821E638D01377, 0x9216D5D98979FB1B, 0xD1310BA698DFB5AC, 0x636920D871574E69, 0x24A19947B3916CF7, 0xC0AC29B7C97C50DD, 0x13198A2E03707344, 0xBA7C9045F12C7F99, 0x243F6A8885A308D3, 0xA4093822299F31D0, 0xB8E1AFED6A267E96, 0x3F84D5B5B5470917, 0xBE5466CF34E90C6C, 0x082EFA98EC4E6C89 },
				{ 0xB8E1AFED6A267E96, 0x9216D5D98979FB1B, 0xBA7C9045F12C7F99, 0x243F6A8885A308D3, 0xBE5466CF34E90C6C, 0xA4093822299F31D0, 0x636920D871574E69, 0x24A19947B3916CF7, 0x2FFD72DBD01ADFB7, 0x0801F2E2858EFC16, 0x082EFA98EC4E6C89, 0xC0AC29B7C97C50DD, 0x3F84D5B5B5470917, 0x13198A2E03707344, 0xD1310BA698DFB5AC, 0x452821E638D01377 },
				{ 0x3F84D5B5B5470917, 0xD1310BA698DFB5AC, 0x082EFA98EC4E6C89, 0x13198A2E03707344, 0x24A19947B3916CF7, 0xBA7C9045F12C7F99, 0xB8E1AFED6A267E96, 0x0801F2E2858EFC16, 0xA4093822299F31D0, 0xC0AC29B7C97C50DD, 0xBE5466CF34E90C6C, 0x2FFD72DBD01ADFB7, 0x452821E638D01377, 0x243F6A8885A308D3, 0x636920D871574E69, 0x9216D5D98979FB1B },
				{ 0xD1310BA698DFB5AC, 0x243F6A8885A308D3, 0xBE5466CF34E90C6C, 0x3F84D5B5B5470917, 0xA4093822299F31D0, 0x452821E638D01377, 0x2FFD72DBD01ADFB7, 0x636920D871574E69, 0x0801F2E2858EFC16, 0x13198A2E03707344, 0xB8E1AFED6A267E96, 0xBA7C9045F12C7F99, 0xC0AC29B7C97C50DD, 0x9216D5D98979FB1B, 0x082EFA98EC4E6C89, 0x24A19947B3916CF7 },
				{ 0xA4093822299F31D0, 0xBA7C9045F12C7F99, 0xC0AC29B7C97C50DD, 0x2FFD72DBD01ADFB7, 0x243F6A8885A308D3, 0xB8E1AFED6A267E96, 0x9216D5D98979FB1B, 0x082EFA98EC4E6C89, 0x452821E638D01377, 0x24A19947B3916CF7, 0x3F84D5B5B5470917, 0xBE5466CF34E90C6C, 0x636920D871574E69, 0x0801F2E2858EFC16, 0x13198A2E03707344, 0xD1310BA698DFB5AC }
			};

			// Array representing the 4x4 matrix.
			private ulong[] matrix;
			// Hold onto the previous chain in order to generate the next chain after all the rounds are complete.
			private ulong[] oldChain;
			// Hold onto the salt in order to generate the next chain after all the rounds are complete.
			private ulong[] salt;

			public Blake_64_Matrix(ulong[] chain, ulong[] s, ulong t0, ulong t1) {
				// TODO: Decide if parameter arguments need to be checked.
				// Assuming chain is 8 UInts long.
				// Assuming s is 4 UInts long.
				// Assuming constant is 8+ UInts long.
				oldChain = chain;
				salt = new ulong[4];
				Array.Copy(s, 0, salt, 0, 4);
				matrix = new ulong[16];
				Array.Copy(oldChain, 0, matrix, 0, 8);
				matrix[08] = salt[00] ^ Constants[00];
				matrix[09] = salt[01] ^ Constants[01];
				matrix[10] = salt[02] ^ Constants[02];
				matrix[11] = salt[03] ^ Constants[03];
				matrix[12] = t0 ^ Constants[04];
				matrix[13] = t0 ^ Constants[05];
				matrix[14] = t1 ^ Constants[06];
				matrix[15] = t1 ^ Constants[07];
			}

			public override string ToString() {
				string tmp = "";
				foreach(uint m in matrix) {
					tmp += m.ToString("X16") + " ";
				}
				return tmp;
			}

			public void Rounds(ulong[] messageFraction) {
				// Blake-512 and Blake-384 perform 16 full rounds.
				for(int z = 0; z < 16; z++)
					Round(messageFraction, z);
			}

			public void Round(ulong[] message, int permutation) {
				// Assuming message is 16 UInts long.
				// Assuming constants is 16 UInts long.
				// Column 1
				QuarterRound(00, 04, 08, 12, message[Permutations[permutation, 00]], message[Permutations[permutation, 01]], ConstantsSigma[permutation, 00], ConstantsSigma[permutation, 01]);
				// Column 2
				QuarterRound(01, 05, 09, 13, message[Permutations[permutation, 02]], message[Permutations[permutation, 03]], ConstantsSigma[permutation, 02], ConstantsSigma[permutation, 03]);
				// Column 3
				QuarterRound(02, 06, 10, 14, message[Permutations[permutation, 04]], message[Permutations[permutation, 05]], ConstantsSigma[permutation, 04], ConstantsSigma[permutation, 05]);
				// Column 4
				QuarterRound(03, 07, 11, 15, message[Permutations[permutation, 06]], message[Permutations[permutation, 07]], ConstantsSigma[permutation, 06], ConstantsSigma[permutation, 07]);
				// Diagonal 1
				QuarterRound(00, 05, 10, 15, message[Permutations[permutation, 08]], message[Permutations[permutation, 09]], ConstantsSigma[permutation, 08], ConstantsSigma[permutation, 09]);
				// Diagonal 2
				QuarterRound(01, 06, 11, 12, message[Permutations[permutation, 10]], message[Permutations[permutation, 11]], ConstantsSigma[permutation, 10], ConstantsSigma[permutation, 11]);
				// Diagonal 3
				QuarterRound(02, 07, 08, 13, message[Permutations[permutation, 12]], message[Permutations[permutation, 13]], ConstantsSigma[permutation, 12], ConstantsSigma[permutation, 13]);
				// Diagonal 4
				QuarterRound(03, 04, 09, 14, message[Permutations[permutation, 14]], message[Permutations[permutation, 15]], ConstantsSigma[permutation, 14], ConstantsSigma[permutation, 15]);
			}

			// Perform a quarter round of operations on the matrix (or, more practically, a column or diagonal in the matrix).  The caller is responsible for passing in the correct values from the message and constant.
			private void QuarterRound(int a, int b, int c, int d, ulong m2I, ulong m2I1, ulong c2I, ulong c2I1) {
				// a = a + b + (mor(2i) ^ cor(2i+1))
				matrix[a] = matrix[a] + matrix[b] + (m2I ^ c2I1);
				// d = (d ^ a) >>> 32
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 32) | ((matrix[d] ^ matrix[a]) << (64 - 32));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 25
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 25) | ((matrix[b] ^ matrix[c]) << (64 - 25));
				// a = a + b + (mor(2i+1) ^ cor(2i)
				matrix[a] = matrix[a] + matrix[b] + (m2I1 ^ c2I);
				// d = (d ^ a) >>> 16
				matrix[d] = ((matrix[d] ^ matrix[a]) >> 16) | ((matrix[d] ^ matrix[a]) << (64 - 16));
				// c = c + d
				matrix[c] = matrix[c] + matrix[d];
				// b = (b ^ c) >>> 11
				matrix[b] = ((matrix[b] ^ matrix[c]) >> 11) | ((matrix[b] ^ matrix[c]) << (64 - 11));
			}

			// Generate the next chain value.  To be accessed after 16 full rounds.
			public ulong[] GetNextChain() {
				ulong[] newChain = new ulong[8];
				for(int x = 0; x < 8; x++)
					newChain[x] = oldChain[x] ^ salt[x % 4] ^ matrix[x] ^ matrix[x + 8];
				return newChain;
			}
		}
	}

	// Compute 512-bit (64-byte) hash using the Blake-512 algorithm.
	public static class Blake_512 {
		public static readonly ulong[] InitialVector = new ulong[8] {
			0x6A09E667F3BCC908, 0xBB67AE8584CAA73B, 0x3C6EF372FE94F82B, 0xA54FF53A5F1D36F1, 0x510E527FADE682D1, 0x9B05688C2B3E6C1F, 0x1F83D9ABFB41BD6B, 0x5BE0CD19137E2179
		};

		// All error checking is done in Blake_64.Hash().
		public static byte[] Hash(byte[] message, byte[] salt) {
			byte[] hash = Blake_64.Hash(message, salt, InitialVector, 0b10000000, 0b00000001);
			return hash;
		}

		// Overload for the Hash function, which adds a salt of all zeroes when none is provided.
		public static byte[] Hash(byte[] message) {
			byte[] salt = new byte[32];
			return Hash(message, salt);
		}
	}

	// Compute 384-bit (48-byte) hash using the Blake-384 algorithm.
	public static class Blake_384 {
		public static readonly ulong[] InitialVector = new ulong[8] {
			0xCBBB9D5DC1059ED8, 0x629A292A367CD507, 0x9159015A3070DD17, 0x152FECD8F70E5939, 0x67332667FFC00B31, 0x8EB44A8768581511, 0xDB0C2E0D64F98FA7, 0x47B5481DBEFA4FA4
		};

		// All error checking is done in Blake_64.Hash().
		public static byte[] Hash(byte[] message, byte[] salt) {
			byte[] hash = new byte[48];
			byte[] baseHash = Blake_64.Hash(message, salt, InitialVector, 0b10000000, 0b00000000);
			Array.Copy(baseHash, 0, hash, 0, 48);
			return hash;
		}

		// Overload for the Hash function, which adds a salt of all zeroes when none is provided.
		public static byte[] Hash(byte[] message) {
			byte[] salt = new byte[32];
			return Hash(message, salt);
		}
	}
}
