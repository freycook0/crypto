using System;

namespace Freycook0.Crypto {
	public static class AEAD_ChaCha20_Poly1305 {
		public static byte[] EncryptAndSign(byte[] k, byte[] n, byte[] m, byte[] aad) {
			// k 256-bit key (32 bytes)
			if(k == null) {
				throw new ArgumentNullException("An empty key was provided; a 256-bit (32-byte) key must be provided to encrypt and sign the message.", "k");
			}
			if(k.Length != 32) {
				int bits = k.Length * 8;
				string errorMessage = "A key must be 256 bits (32 bytes) for the ChaCha20-Poly1305 AEAD algorithm.  The key provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, k.Length), "k");
			}
			byte[] Key = new byte[32];
			Array.Copy(k, Key, k.Length);

			// n 96-bit nonce (12 bytes)
			if(n == null) {
				throw new ArgumentNullException("An empty nonce was provided; a 96-bit (12-byte) nonce must be provided to encrypt and sign the message.", "n");
			}
			if(n.Length != 12) {
				int bits = n.Length * 8;
				string errorMessage = "A nonce must be 96 bits (12 bytes) for the ChaCha20-Poly1305 AEAD algorithm.  The nonce provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, n.Length), "n");
			}
			byte[] Nonce = new byte[12];
			Array.Copy(n, Nonce, n.Length);

			// m any* length message
			if(m == null) {
				throw new ArgumentNullException("A null message was provided; please provide a non-null message.", "m");
			}
			// Is it valid for a message to be length 0?
			if(m.Length == 0) {
				string errorMessage = "A message must not be 0 bits (0 bytes) for the ChaCha20-Poly1305 AEAD algorithm.  The nonce provided is {0} bits {{0} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, m.Length), "m");
			}
			byte[] Message = new byte[m.Length];
			Array.Copy(m, Message, m.Length);

			// aad any length additional authenticated data
			if(aad == null) {
				throw new ArgumentNullException("A null additional authenticated data was provided; please provide a non-null additional authenticated data.", "aad");
			}
			// Is it valid for an additional authenticated data to be length 0?
			if(aad.Length == 0) {
				string errorMessage = "An additional authenticated data must not be 0 bits (0 bytes) for the ChaCha20-Poly1305 AEAD algorithm.  The additional authenticated data provided is {0} bits {{0} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, aad.Length), "aad");
			}
			byte[] AdditionalAuthenticatedData = new byte[aad.Length];
			Array.Copy(aad, AdditionalAuthenticatedData, aad.Length);

			// Concatenate some blank data (all zeroes) with the message to provide to the ChaCha20 algorithm.
			byte[] BlankAndPlainText = new byte[Message.Length + 64];
			Array.Copy(Message, 0, BlankAndPlainText, 64, Message.Length);

			// Call the ChaCha20 algorithm.  The output will contain the encrypted form of the message, as well as the key that will be used by the Poly1305 algorithm.
			byte[] chacha20CipherText = ChaCha20.Encrypt(BlankAndPlainText, Key, Nonce);

			// Extract the key used in the Poly1305 algorithm from the cipher text generated with ChaCha20.
			// NOTE: There are 256 bits (32 bytes) of wasted cipher text, as the block size from the ChaCha20 algorithm is twice the size needed for the Poly1305 key.
			byte[] Poly1305Key = new byte[32];
			Array.Copy(chacha20CipherText, Poly1305Key, 32);
			// Extract the "true" cipher text from the cipher text generated with ChaCha20.
			byte[] CipherText = new byte[Message.Length];
			Array.Copy(chacha20CipherText, 64, CipherText, 0, CipherText.Length);

			// Construct the message that is passed to the Poly1305 algorithm.  The message structure:
			// *	The additional authenticated data, padded (with zeroes) to a multiple of 16 bytes.
			// *	The message, padded (with zeroes) to a multiple of 16 bytes.
			// *	A 64-bit unsigned integer, representing the length of the additional authenticated data, in bytes.
			// *	A 64-bit unsigned integer, representing the length of the message, in bytes.
			int aadBlocks = AdditionalAuthenticatedData.Length / 16;
			if(AdditionalAuthenticatedData.Length % 16 != 0)
				aadBlocks++;
			int cipherTextBlocks = CipherText.Length / 16;
			if(CipherText.Length % 16 != 0)
				cipherTextBlocks++;
			long aadBytes = AdditionalAuthenticatedData.Length;
			byte[] aadBlocksBytes = BitConverter.GetBytes(aadBytes);
			long cipherTextBytes = CipherText.Length;
			byte[] cipherTextBlocksBytes = BitConverter.GetBytes(cipherTextBytes);
			byte[] Poly1305Message = new byte[(aadBlocks + cipherTextBlocks + 1) * 16];
			Array.Copy(AdditionalAuthenticatedData, Poly1305Message, AdditionalAuthenticatedData.Length);
			Array.Copy(CipherText, 0, Poly1305Message, aadBlocks * 16, CipherText.Length);
			Array.Copy(aadBlocksBytes, 0, Poly1305Message, (aadBlocks + cipherTextBlocks) * 16, 8);
			Array.Copy(cipherTextBlocksBytes, 0, Poly1305Message, ((aadBlocks + cipherTextBlocks) * 16) + 8, 8);

			// Call the Poly1305 algorithm.  This is a simple output; it's just the MAC generated by Poly1305.
			byte[] poly1305Mac = Poly1305.GenerateMAC(Poly1305Key, Poly1305Message);

			byte[] cipherTextAndSignature = new byte[CipherText.Length + 16];
			Array.Copy(CipherText, cipherTextAndSignature, CipherText.Length);
			Array.Copy(poly1305Mac, 0, cipherTextAndSignature, CipherText.Length, 16);

			return cipherTextAndSignature;
		}
	}
}
