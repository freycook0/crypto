using Freycook0.Math;
using System;

namespace Freycook0.Crypto {
	public static class Poly1305 {
		// These constants do not change across calls to Poly1305.
		private static readonly UInt256 RClamp = new UInt256((uint)0x0FFFFFFF, (uint)0x0FFFFFFC, (uint)0x0FFFFFFC, (uint)0x0FFFFFFC);
		private static readonly UInt256 P = new UInt256((uint)0xFFFFFFFB, (uint)0xFFFFFFFF, (uint)0xFFFFFFFF, (uint)0xFFFFFFFF, (uint)0x00000003);

		public static byte[] GenerateMAC(byte[] k, byte[] m) {
			// Checking input paramters for issues.
			if(k == null) {
				throw new ArgumentNullException("An empty key was provided; a 256-bit (32-byte) key must be provided to sign the message.", "k");
			}
			if(k.Length != 32) {
				int bits = k.Length * 8;
				string errorMessage = "A key must be 256 bits (32 bytes) for the Poly1305 signing algorithm.  The key provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, k.Length), "k");
			}
			if(m == null) {
				throw new ArgumentNullException("A null message was provided; please provide a non-null message.", "m");
			}
			// Is it valid for a message to be length 0?
			if(m.Length == 0) {
				string errorMessage = "A message must not be 0 bits (0 bytes) for the ChaCha20-Poly1305 AEAD algorithm.  The nonce provided is {0} bits {{0} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, m.Length), "m");
			}

			byte[] Key = new byte[32];
			Array.Copy(k, Key, 32);
			byte[] Message = new byte[m.Length];
			Array.Copy(m, Message, m.Length);
			UInt256 Accumulator = new UInt256();
			UInt256 R = new UInt256(BitConverter.ToUInt32(Key, 00), BitConverter.ToUInt32(Key, 04), BitConverter.ToUInt32(Key, 08), BitConverter.ToUInt32(Key, 12));
			UInt256 RClamped = R & RClamp;
			UInt256 S = new UInt256(BitConverter.ToUInt32(Key, 16), BitConverter.ToUInt32(Key, 20), BitConverter.ToUInt32(Key, 24), BitConverter.ToUInt32(Key, 28));
			UInt256 onePrefix = new UInt256(0, 0, 0, 0, 1);

			// Variables for parceling the message into 256-bit integers, 16 bytes at a time.
			uint[] temp = new uint[4];
			int rounds = Message.Length / 16;
			int extraBytes = Message.Length % 16;
			int totalRounds;
			UInt256[] blocks;

			// Determine if we need an extra round of encapsulation, which is necessary if the message doesn't perfectly fit into 16-byte blocks.
			if(extraBytes == 0)
				totalRounds = rounds;
			else
				totalRounds = rounds + 1;
			blocks = new UInt256[totalRounds];
			for(int a = 0; a < rounds; a++) {
				for(int b = 0; b < 16; b++) {
					temp[b / 4] = temp[b / 4] | (((uint)Message[a * 16 + b]) << (8 * (b % 4)));
				}
				blocks[a] = new UInt256(temp[0], temp[1], temp[2], temp[3]);
				temp = new uint[4];
			}
			// If there is an extra block to process that is less than 16 bytes long, then extract just those bytes (to avoid an IndexOutOfBoundsException).
			if(extraBytes != 0) {
				for(int c = 0; c < extraBytes; c++) {
					temp[c / 4] = temp[c / 4] | (((uint)Message[rounds * 16 + c]) << (8 * (c % 4)));
				}
				blocks[rounds] = new UInt256(temp[0], temp[1], temp[2], temp[3]);
			}

			// Operations peformed for each block:
			// *	Append Block with a 1 (or prepend, depending on your point of view)
			// *	Accumulator += Block
			// *	Accumulator *= R (after it's been "clamped")
			// *	Accumulator %= P
			for(int d = 0; d < totalRounds; d++) {
				// On the last round, adjust the 01 prefix to fit the last block.  Otherwise, leave it at 01 << (16 * 8)
				if(d == rounds && d != totalRounds)
					onePrefix = (new UInt256(1) << (extraBytes * 8));
				Accumulator += blocks[d] | onePrefix;
				Accumulator *= RClamped;
				Accumulator %= P;
			}

			// After all the blocks have been processed, the last step in Poly1305 is to add S to Accumulator.
			Accumulator += S;

			// Convert the accumulator into a byte string, as it is the MAC code.  Any digits over the first 16 bytes of data in the accumulator gets truncated.
			byte[] MAC = Extract16BytesFromUInt256(Accumulator);

			return MAC;
		}

		private static byte[] Extract16BytesFromUInt256(UInt256 number) {
			return ExtractBytesFromUInt256(number, 16);
		}

		private static byte[] ExtractBytesFromUInt256(UInt256 number, int numBytes) {
			if(numBytes <= 0)
				throw new ArgumentException("The number of bytes to be extracted from \"number\" must be greater than 0.", "numBytes");
			// There are only 32 bytes (320 bits) in a UInt256, so we'll cap the output to 40 bytes.
			if(numBytes > 32)
				numBytes = 32;

			byte[] bytes = new byte[numBytes];
			for(int x = 0; x < numBytes; x++) {
				// Determine which integer to pull from, shift it to the right to get to the correct bit, then cast it to a byte (truncating any extraneous bits).
				uint tempUInt;
				int whichInt = x / 4;
				if(whichInt == 0)
					tempUInt = number.UInt00;
				else if(whichInt == 1)
					tempUInt = number.UInt01;
				else if(whichInt == 2)
					tempUInt = number.UInt02;
				else if(whichInt == 3)
					tempUInt = number.UInt03;
				else if(whichInt == 4)
					tempUInt = number.UInt04;
				else if(whichInt == 5)
					tempUInt = number.UInt05;
				else if(whichInt == 6)
					tempUInt = number.UInt06;
				else //if(whichInt == 7)
					tempUInt = number.UInt07;
				/*else if(whichInt == 8)
					tempUInt = number.UInt08;
				else
					tempUInt = number.UInt09;*/
				bytes[x] = (byte)(tempUInt >> (8 * (x % 4)));
			}

			return bytes;
		}
	}
}
