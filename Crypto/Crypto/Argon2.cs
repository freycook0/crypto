﻿using System;
using System.Threading.Tasks;
//using System.Collections.Generic;
//using System.Text;

namespace Freycook0.Crypto {
	public class Argon2 {
		public const uint Version = 0x13;
		public const uint Parallelism_Max = (2u << 23) - 1u;   // (2 << 23) - 1 == (2 ** 24) - 1

		public static byte[] Hash(byte[] message, byte[] nonce, uint parallelism, uint tagLength, uint memorySize, uint iterations, byte[] key, byte[] associatedData, uint type, uint version = Version) {
			// Parameter Checking
			// message: non-null, between 0 and 2 ** 32 - 1 bytes long
			if(message == null)
				throw new ArgumentNullException("message", "Cannot hash a null message.  Provide a non-null message to hash.");
			/*if(message.Length < 0)
				throw new ArgumentOutOfRangeException("message", message, "");*/
			// nonce: non-null, between 8 and 2 ** 32 - 1 bytes long
			if(nonce == null)
				throw new ArgumentNullException("nonce", "Cannot hash with a null nonce.  Provide a non-null nonce to hash with.");
			if(nonce.Length < 8)
				throw new ArgumentOutOfRangeException("nonce", nonce, "The length of the nonce must be between 8 and 2 ** 32 - 1 bytes long.");
			// parallelism: between 1 and 2 ** 24 - 1
			if(parallelism < 1 || parallelism > Parallelism_Max)
				throw new ArgumentOutOfRangeException("parallelism", parallelism, "The degree of parallelism must be between 1 and 2 ** 24 - 1.");
			// tagLength: between 4 and 2 ** 32 - 1
			if(tagLength < 4)
				throw new ArgumentOutOfRangeException("tagLength", tagLength, "The tag (output) length must be between 4 and 2 ** 32 - 1.");
			// memorySize: between 8 * parallelsim and 2 ** 32 - 1
			if(memorySize < 8 * parallelism)
				throw new ArgumentOutOfRangeException("memorySize", memorySize, "The memory size must be between 8 * parallelism and 2 ** 32 - 1.");
			// iterations: between 1 and 2 ** 32 - 1
			if(iterations < 1)
				throw new ArgumentOutOfRangeException("iterations", iterations, "The number of iterations must be between 1 and 2 ** 32 - 1.");
			// key: non-null, between 0 and 2 ** 32 - 1 bytes long
			if(key == null)
				throw new ArgumentNullException("key", "Cannot hash with a null key.  Provide a non-null key to hash with.");
			/*if(key.Length < 0)
				throw new ArgumentOutOfRangeException("key", key, "");*/
			// associatedData: non-null, between 0 and 2 ** 32 - 1 bytes long
			if(associatedData == null)
				throw new ArgumentNullException("associatedData", "Cannot hash with null associated data.  Provide non-null associated data to hash with.");
			/*if(associatedData.Length < 0)
				throw new ArgumentOutOfRangeException("associatedData", associatedData, "");*/
			// type: between 0 and 2
			if(type < 0 || type > 2)
				throw new ArgumentOutOfRangeException("type", type, "The type must be 0 (Argon2d), 1 (Argon2i), or 2 (Argon2id).");
			// version: 0x13
			if(version != Version)
				throw new ArgumentOutOfRangeException("version", version, "The version must be 0x13.");

			// Generate H_0
			byte[] H_0 = Compute_H_0(message, nonce, parallelism, tagLength, memorySize, iterations, key, associatedData, type, version);

			// Memory size m can be any integer number of kibibytes from 8*p to 2 * *32 - 1. The actual number of blocks is m', which is m rounded down to the nearest multiple of 4 * p.
			uint parallelismX4 = 4u * parallelism;
			uint blocks = memorySize - (memorySize % parallelismX4);
			uint lanes = parallelism;
			uint columns = blocks / parallelism;
			uint segmentLength = columns / 4;

			// Allocate memory per memorySize argument.
			Block[,] memoryBlocks = new Block[lanes, columns];

			// DONE: define a "block" (1024 bytes)

			Parallel.For(0, (int) lanes, delegate (int i) {
				// Calculate B[i][0], B[i][1].
				memoryBlocks[i, 0] = new Block(HPrime(H_0, (uint) i, 0u, Block.BlockSize));
				memoryBlocks[i, 1] = new Block(HPrime(H_0, (uint) i, 1u, Block.BlockSize));
				// Complete the rest of the first slice.
				uint t = 0u;
				uint slice = 0u;
				for(uint j = 2; j < segmentLength; j++) {
					ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
					uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
					ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
					uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
					memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]);
				}
			});
			// Second slice.
			Parallel.For(0, (int) lanes, delegate (int i) {
				uint t = 0u;
				uint slice = 1u;
				for(uint j = segmentLength; j < 2 * ((int) segmentLength); j++) {
					ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
					uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
					ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
					uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
					memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]);
				}
			});
			// Third slice.
			Parallel.For(0, (int) lanes, delegate (int i) {
				uint t = 0u;
				uint slice = 2u;
				for(uint j = 2u * segmentLength; j < 3u * segmentLength; j++) {
					ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
					uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
					ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
					uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
					memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]);
				}
			});
			// Fourth slice.
			Parallel.For(0, (int) lanes, delegate (int i) {
				uint t = 0u;
				uint slice = 3u;
				for(uint j = 3u * segmentLength; j < columns; j++) {
					ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
					uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
					ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
					uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
					memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]);
				}
			});
			// Perform the rest of the iterations.
			/*for(uint t = 1; t < iterations; t++) {
				// First slice.
				Parallel.For(0, (int) lanes, delegate (int i) {
					uint j = 0u;
					uint slice = 0u;
					ulong j12 = CalculateJ1J2((uint) i, 0u, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
					uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
					ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, 0u, parallelism, columns, slice, t, segmentLength);
					uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
					memoryBlocks[i, 0] = G(memoryBlocks[i, columns - 1], memoryBlocks[iPrime, jPrime]) ^ memoryBlocks[i, 0];
					for(j = 1u; j < segmentLength; j++) {
						j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
						j1 = (uint) j12;
						j2 = (uint) (j12 >> 32);
						doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
						iPrime = (uint) (doubleIndex >> 32);
						jPrime = (uint) doubleIndex;
						memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]) ^ memoryBlocks[i, j];

					}
				});
				// Second slice.
				Parallel.For(0, (int) lanes, delegate (int i) {
					uint slice = 1u;
					for(uint j = segmentLength; j < 2u * segmentLength; j++) {
						ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
						uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
						ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
						uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
						memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]) ^ memoryBlocks[i, j];
					}
				});
				// Third slice.
				Parallel.For(0, (int) lanes, delegate (int i) {
					uint slice = 2u;
					for(uint j = 2u * segmentLength; j < 3u * segmentLength; j++) {
						ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
						uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
						ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
						uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
						memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]) ^ memoryBlocks[i, j];
					}
				});
				// Fourth slice.
				Parallel.For(0, (int) lanes, delegate (int i) {
					uint slice = 3u;
					for(uint j = 3u * segmentLength; j < columns; j++) {
						ulong j12 = CalculateJ1J2((uint) i, j, type, memoryBlocks, columns, t, slice, blocks, iterations, segmentLength);
						uint j1 = (uint) j12, j2 = (uint) (j12 >> 32);
						ulong doubleIndex = MapJ1J2(j1, j2, (uint) i, j, parallelism, columns, slice, t, segmentLength);
						uint iPrime = (uint) (doubleIndex >> 32), jPrime = (uint) doubleIndex;
						memoryBlocks[i, j] = G(memoryBlocks[i, j - 1], memoryBlocks[iPrime, jPrime]) ^ memoryBlocks[i, j];
					}
				});
			}*/

			/*Block c = memoryBlocks[0, columns - 1];
			for(int i = 1; i < lanes; i++)
				c ^= memoryBlocks[i, columns - 1];*/
			//c = c ^ memoryBlocks[i, columns - 1];

			bool tmp = true;
			if(tmp)
				throw new Exception("Lanes: " + lanes + " width: " + memoryBlocks.Length + " Columns: " + columns + " length: " + memoryBlocks.GetLength(0));
			//byte[] output = H_0;
			//byte[] output = memoryBlocks[0, 0].blockArray;
			//byte[] output = memoryBlocks[parallelism - 1, columns - 1].blockArray;
			byte[] output = memoryBlocks[3, 7].blockArray;
			//byte[] output = HPrime(c.blockArray, tagLength);

			return output;
		}

		// TODO: Make this method private after testing.
		public static byte[] Compute_H_0(byte[] message, byte[] nonce, uint parallelism, uint tagLength, uint memorySize, uint iterations, byte[] key, byte[] associatedData, uint type, uint version) {
			// Convert various arguments to byte strings, as well as creating byte strings for the length of the message, nonce, key, and associated data.
			byte[] parallelismBytes = BitConverter.GetBytes(parallelism);
			byte[] tagLengthBytes = BitConverter.GetBytes(tagLength);
			byte[] memorySizeBytes = BitConverter.GetBytes(memorySize);
			byte[] iterationsBytes = BitConverter.GetBytes(iterations);
			byte[] versionBytes = BitConverter.GetBytes(version);
			byte[] typeBytes = BitConverter.GetBytes(type);
			byte[] messageLengthBytes = BitConverter.GetBytes(message.Length);
			byte[] nonceLengthBytes = BitConverter.GetBytes(nonce.Length);
			byte[] keyLengthBytes = BitConverter.GetBytes(key.Length);
			byte[] associatedDataLengthBytes = BitConverter.GetBytes(associatedData.Length);
			// Concatenate all of the byte strings into one, and then feed it into Blake2b.
			byte[] superLongString = new byte[4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length + 4 + nonce.Length + 4 + key.Length + 4 + associatedData.Length];
			Array.Copy(parallelismBytes, 0, superLongString, 0, 4);
			Array.Copy(tagLengthBytes, 0, superLongString, 4, 4);
			Array.Copy(memorySizeBytes, 0, superLongString, 4 + 4, 4);
			Array.Copy(iterationsBytes, 0, superLongString, 4 + 4 + 4, 4);
			Array.Copy(versionBytes, 0, superLongString, 4 + 4 + 4 + 4, 4);
			Array.Copy(typeBytes, 0, superLongString, 4 + 4 + 4 + 4 + 4, 4);
			Array.Copy(messageLengthBytes, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4, 4);
			Array.Copy(message, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4, message.Length);
			Array.Copy(nonceLengthBytes, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length, 4);
			Array.Copy(nonce, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length + 4, nonce.Length);
			Array.Copy(keyLengthBytes, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length + 4 + nonce.Length, 4);
			Array.Copy(key, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length + 4 + nonce.Length + 4, key.Length);
			Array.Copy(associatedDataLengthBytes, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length + 4 + nonce.Length + 4 + key.Length, 4);
			Array.Copy(associatedData, 0, superLongString, 4 + 4 + 4 + 4 + 4 + 4 + 4 + message.Length + 4 + nonce.Length + 4 + key.Length + 4, associatedData.Length);
			//H_0 = H(p, T, m, t, v, y, length(P), P, length(S), S, length(K), K, length(X), X)
			byte[] H_0 = Blake2b.Hash(superLongString); // Blake2b outputs 64-bytes by default.

			return H_0;
		}

		// TODO: Make this method private after testing.
		// TODO: Decide on a better name.
		public static byte[] HPrime(byte[] data, uint tagLength) {
			byte[] concatenatedData = new byte[data.Length + 4];
			Array.Copy(BitConverter.GetBytes(tagLength), 0, concatenatedData, 0, 4);
			Array.Copy(data, 0, concatenatedData, 4, data.Length);
			if(tagLength <= 64) {
				byte[] hash = Blake2b.Hash(concatenatedData, (int)tagLength);
				return hash;
			}
			else {
				uint r = ((tagLength / 32u) + (System.Math.Min(tagLength % 32u, 1u))) - 2u;
				byte[] tmp = concatenatedData;
				byte[] hash = new byte[tagLength];
				for(uint x = 0u; x < r; x++) {
					tmp = Blake2b.Hash(tmp);
					Array.Copy(tmp, 0u, hash, x * 32u, 32u);
				}
				uint lastHashLength = tagLength - (32u * r);
				tmp = Blake2b.Hash(tmp, (int)lastHashLength);
				Array.Copy(tmp, 0u, hash, r * 32u, lastHashLength);
				return hash;
			}
		}

		// TODO: Make this method private after testing.
		// TODO: Decide on a better name.
		public static byte[] HPrime(byte[] data, uint i, uint j, uint tagLength) {
			byte[] concatenatedData = new byte[data.Length + 4 + 4];
			Array.Copy(data, 0, concatenatedData, 0, data.Length);
			Array.Copy(BitConverter.GetBytes(i), 0, concatenatedData, data.Length, 4);
			Array.Copy(BitConverter.GetBytes(j), 0, concatenatedData, data.Length + 4, 4);
			return HPrime(concatenatedData, tagLength);
		}

		// TODO: Make this method private after testing.
		// TODO: Decide on a better name.
		public static Block G(Block x, Block y) {
			//	G: (X, Y) -> R = X XOR Y -P-> Q -P-> Z -P-> Z XOR R
			Block r = x ^ y;
			Block q = r.PRoundRow();
			Block z = q.PRoundColumn();
			return z ^ r;
		}

		public static ulong CalculateJ1J2(uint i, uint j, uint type, Block[,] memoryBlocks, uint columns, uint t, uint slice, uint blocks, uint iterations, uint segmentLength) {
			uint j1, j2;
			ulong j12;
			byte[] leftBytes, rightBytes;
			Block leftBlock, rightBlock, jBlock;
			switch(type) {
				case Argon2D.Type:
					/*byte[] tmp = memoryBlocks[i, (j + columns - 1) % columns].blockArray;
					j1 = BitConverter.ToUInt32(tmp, 0);
					j2 = BitConverter.ToUInt32(tmp, 4);*/
					j12 = BitConverter.ToUInt64(memoryBlocks[i, (j + columns - 1) % columns].blockArray);
					break;
				case Argon2I.Type:
					leftBytes = new byte[Block.BlockSize];
					rightBytes = new byte[Block.BlockSize];
					Array.Copy(BitConverter.GetBytes((ulong) t), 0, rightBytes, 0 * 8, 8);
					Array.Copy(BitConverter.GetBytes((ulong) i), 0, rightBytes, 1 * 8, 8);
					Array.Copy(BitConverter.GetBytes((ulong) slice), 0, rightBytes, 2 * 8, 8);
					Array.Copy(BitConverter.GetBytes((ulong) blocks), 0, rightBytes, 3 * 8, 8);
					Array.Copy(BitConverter.GetBytes((ulong) iterations), 0, rightBytes, 4 * 8, 8);
					Array.Copy(BitConverter.GetBytes((ulong) type), 0, rightBytes, 5 * 8, 8);
					Array.Copy(BitConverter.GetBytes((ulong) ((j % segmentLength) + 1u)), 0, rightBytes, 6 * 8, 8);
					leftBlock = new Block(leftBytes);
					rightBlock = new Block(rightBytes);
					jBlock = G(leftBlock, rightBlock);
					//j1 = BitConverter.ToUInt32(jBlock.blockArray, 0);
					//j2 = BitConverter.ToUInt32(jBlock.blockArray, 4);
					j12 = BitConverter.ToUInt64(jBlock.blockArray);
					break;
				case Argon2ID.Type:
					//j1 = 0u;
					//j2 = 0u;
					// If it's the first pass and either the first or second slice, then use Argon2I.  Otherwise, use Argon2D.
					if(t == 0 && (slice == 0 || slice == 1)) {
						leftBytes = new byte[Block.BlockSize];
						rightBytes = new byte[Block.BlockSize];
						Array.Copy(BitConverter.GetBytes((ulong) t), 0, rightBytes, 0 * 8, 8);
						Array.Copy(BitConverter.GetBytes((ulong) i), 0, rightBytes, 1 * 8, 8);
						Array.Copy(BitConverter.GetBytes((ulong) slice), 0, rightBytes, 2 * 8, 8);
						Array.Copy(BitConverter.GetBytes((ulong) blocks), 0, rightBytes, 3 * 8, 8);
						Array.Copy(BitConverter.GetBytes((ulong) iterations), 0, rightBytes, 4 * 8, 8);
						Array.Copy(BitConverter.GetBytes((ulong) type), 0, rightBytes, 5 * 8, 8);
						Array.Copy(BitConverter.GetBytes((ulong) ((j % segmentLength) + 1u)), 0, rightBytes, 6 * 8, 8);
						leftBlock = new Block(leftBytes);
						rightBlock = new Block(rightBytes);
						jBlock = G(leftBlock, rightBlock);
						j12 = BitConverter.ToUInt64(jBlock.blockArray);
					}
					else {
						j12 = BitConverter.ToUInt64(memoryBlocks[i, (j + columns - 1) % columns].blockArray);
					}
					//j12 = 0ul;
					break;
				default:
					throw new Exception("This shouldn't be possible, assuming CalculateJ1J2 is private.");
					break;
			}
			//j12 = ((ulong) j1 << 32) | (ulong) j2;
			return j12;
		}

		public static ulong MapJ1J2(uint j1, uint j2, uint i, uint j, uint parallelism, uint columns, uint slice, uint t, uint segmentLength) {
			// parallelism == lanes
			uint l;
			uint[] r;
			if(t == 0u) {
				if(slice == 0u) {
					// If on the first pass and within the first slice, then stay within the same lane, as the only slices available for reference are being calculated concurrently, and we don't like race conditions.
					l = i;
					r = new uint[j - 1];
					for(int a = 0; a < r.Length; a++)
						r[a] = (uint) a;
				}
				else {
					l = j2 % parallelism;
					if(l == i) {
						r = new uint[j - 1];
						for(int a = 0; a < r.Length; a++)
							r[a] = (uint) a;
					}
					else {
						//r = new uint[(slice - 1u) * segmentLength];
						r = new uint[slice * segmentLength];
						for(int a = 0; a < r.Length; a++)
							r[a] = (uint) a;
					}
				}
			}
			else {
				l = j2 % parallelism;
				if(l == i) {
					r = new uint[(3 * segmentLength) + (j % segmentLength) - 1];
					for(uint a = (((slice + 1u) % 4u) * segmentLength), b = 0; b < r.Length; a = (a + 1u) % columns, b++)
						r[b] = a;
				}
				else {
					r = new uint[3 * segmentLength];
					for(uint a = (((slice + 1u) % 4u) * segmentLength), b = 0; b < r.Length; a = (a + 1u) % columns, b++)
						r[b] = a;
				}
			}
			//ulong x = (ulong) j1 * (ulong) j1 / 0x100000000ul;	// 2 ^ 32
			ulong x = ((ulong) j1 * (ulong) j1) >> 32;
			//ulong y = something * x / 0x100000000ul;	// 2 ^ 32
			ulong y = (((ulong) r.Length) * x) >> 32;
			ulong z = ((ulong) r.Length) - 1ul - y;
			// Based on z and R, determine the j' value.
			// DONE?: FIX THIS IT'S THROWING System.AggregateException
			//ulong index = (ulong) r[z];
			if(z >= 0x100000000ul)
				throw new Exception("Is z too big? l:" + l + " rLength:" + r.Length + " x:" + x + " y:" + y + " z:" + z + " j1:" + j1 + " j2:" + j2 + " i:" + i + " j:" + j + " parallelism:" + parallelism + " columns:" + columns + " slice:" + slice + " t:" + t + " segmentLength:" + segmentLength);
			uint index = r[z];
			ulong returnValue = (((ulong) l) << 32) | (ulong) index;
			return returnValue;
		}

		// TODO: Make this class private after testing.
		public class Block {
			// Should the internal array be private or public?
			public byte[] blockArray;
			public const int BlockSize = 1024;
			//public const int ULongSize = 128;
			public const int RegisterSize = 16;
			public const int RowSize = 128; // 16 * 8
			public const int RowRegisters = 8;
			public const int ColumnRegisters = 8;

			public Block() {
				blockArray = new byte[BlockSize];
			}

			public Block(byte[] bA) {
				blockArray = new byte[BlockSize];
				if(bA == null)
					return;
				if(bA.Length != BlockSize)
					throw new ArgumentOutOfRangeException("bA", bA, "Length of a Block is 1024 bytes; instantiate with a byte array that is 1024 bytes long");
				// TODO: Decide if the input array should be referenced or copied.
				blockArray = bA;
				//Array.Copy(bA, blockArray, bA.Length);
			}

			public Block PRoundRow() {
				byte[] destinationBytes = new byte[BlockSize];
				for(int x = 0; x < BlockSize / RowSize; x++) {
					byte[] tmp = new byte[RowSize];
					Array.Copy(blockArray, x * RowSize, tmp, 0, RowSize);
					byte[] tmp2 = P(tmp);
					Array.Copy(tmp2, 0, destinationBytes, x * RowSize, RowSize);
				}
				return new Block(destinationBytes);
			}

			public Block PRoundColumn() {
				byte[] destinationBytes = new byte[BlockSize];
				for(int x = 0; x < ColumnRegisters; x++) {
					byte[] tmp = new byte[RowSize];
					for(int y = 0; y < RowRegisters; y++)
						Array.Copy(blockArray, (x * RegisterSize) + (y * RowSize), tmp, y * RegisterSize, RegisterSize);
					byte[] tmp2 = P(tmp);
					for(int y = 0; y < RowRegisters; y++)
						Array.Copy(tmp2, y * RegisterSize, destinationBytes, (x * RegisterSize) + (y * RowSize), RegisterSize);
				}
				return new Block(destinationBytes);
			}

			// TODO: Make this private after testing.
			public byte[] P(byte[] input) {
				// Assume input is 128 bytes long.
				ulong[] numbers = new ulong[16];
				for(int x = 0; x < 16; x++)
					numbers[x] = BitConverter.ToUInt64(input, x * 8);
				Round(numbers, 00, 04, 08, 12);
				Round(numbers, 01, 05, 09, 13);
				Round(numbers, 02, 06, 10, 14);
				Round(numbers, 03, 07, 11, 15);
				Round(numbers, 00, 05, 10, 15);
				Round(numbers, 01, 06, 11, 12);
				Round(numbers, 02, 07, 08, 13);
				Round(numbers, 03, 04, 09, 14);
				byte[] output = new byte[RowSize];
				for(int x = 0; x < 16; x++)
					Array.Copy(BitConverter.GetBytes(numbers[x]), 0, output, x * 8, 8);
				return output;
			}

			// TODO: Make this private after testing.
			public void Round(ulong[] numbers, int a, int b, int c, int d) {
				numbers[a] = numbers[a] + numbers[b] + (2ul * (numbers[a] & 0x00000000FFFFFFFFul) * (numbers[b] & 0x00000000FFFFFFFFul));
				numbers[d] = ((numbers[d] ^ numbers[a]) >> 32) | ((numbers[d] ^ numbers[a]) << (64 - 32));
				numbers[c] = numbers[c] + numbers[d] + (2ul * (numbers[c] & 0x00000000FFFFFFFFul) * (numbers[d] & 0x00000000FFFFFFFFul));
				numbers[b] = ((numbers[b] ^ numbers[c]) >> 24) | ((numbers[b] ^ numbers[c]) << (64 - 24));
				numbers[a] = numbers[a] + numbers[b] + (2ul * (numbers[a] & 0x00000000FFFFFFFFul) * (numbers[b] & 0x00000000FFFFFFFFul));
				numbers[d] = ((numbers[d] ^ numbers[a]) >> 16) | ((numbers[d] ^ numbers[a]) << (64 - 16));
				numbers[c] = numbers[c] + numbers[d] + (2ul * (numbers[c] & 0x00000000FFFFFFFFul) * (numbers[d] & 0x00000000FFFFFFFFul));
				numbers[b] = ((numbers[b] ^ numbers[c]) >> 63) | ((numbers[b] ^ numbers[c]) << (64 - 63));
			}

			public static Block operator ^(Block a, Block b) {
				byte[] tmpArray = new byte[BlockSize];
				for(int x = 0; x < BlockSize; x++)
					tmpArray[x] = (byte)(a.blockArray[x] ^ b.blockArray[x]);
				return new Block(tmpArray);
			}
		}
	}

	public class Argon2D {
		public const uint Type = 0;

		public static byte[] Hash(byte[] message, byte[] nonce, uint parallelism, uint tagLength, uint memorySize, uint iterations, byte[] key, byte[] associatedData) {
			return Argon2.Hash(message, nonce, parallelism, tagLength, memorySize, iterations, key, associatedData, Type);
		}
	}

	public class Argon2I {
		public const uint Type = 1;

		public static byte[] Hash(byte[] message, byte[] nonce, uint parallelism, uint tagLength, uint memorySize, uint iterations, byte[] key, byte[] associatedData) {
			return Argon2.Hash(message, nonce, parallelism, tagLength, memorySize, iterations, key, associatedData, Type);
		}
	}

	public class Argon2ID {
		public const uint Type = 2;

		public static byte[] Hash(byte[] message, byte[] nonce, uint parallelism, uint tagLength, uint memorySize, uint iterations, byte[] key, byte[] associatedData) {
			return Argon2.Hash(message, nonce, parallelism, tagLength, memorySize, iterations, key, associatedData, Type);
		}
	}
}
