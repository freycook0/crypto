// https://en.wikipedia.org/wiki/Salsa20#ChaCha_variant
// https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Counter_.28CTR.29
// Testing data:
// https://tools.ietf.org/html/rfc7539#section-2.4.2

using System;

namespace Freycook0.Crypto {
	public class ChaCha20 {
		// There are 64 bytes in a 512-bit block.
		private const uint BlockSize = 64;

		// TODO: Consider this idea, change to one overload of Encrypt() which takes a ulong for the counter, and decides to utilize it as-is or cast it to a uint based on the length of the nonce.

		// NOTE: Standard mode of operation.
		public static byte[] Encrypt(byte[] data, byte[] key, byte[] nonce, uint counter = 0) {
			// Checking input paramters for issues.
			if(key == null)
				throw new ArgumentNullException("An empty key was provided; a 256-bit (32-byte) key must be provided to encrypt the message.", "key");
			if(key.Length != 32) {
				int bits = key.Length * 8;
				string errorMessage = "A key must be 256 bits (32 bytes) for the ChaCha20 encryption algorithm.  The key provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, key.Length), "key");
			}
			if(nonce == null)
				throw new ArgumentNullException("An empty nonce was provided; a 96-bit (12-byte) nonce must be provided to encrypt the message.", "nonce");
			if(nonce.Length != 12) {
				int bits = nonce.Length * 8;
				string errorMessage = "A nonce must be 96 bits (12 bytes) for the ChaCha20 encryption algorithm.  The nonce provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, nonce.Length), "nonce");
			}
			if(data == null)
				throw new ArgumentNullException("A null message was provided; please provide a non-null message.", "m");
			// Is it valid for the data to be length 0?
			if(data.Length == 0) {
				string errorMessage = "The data must not be 0 bits (0 bytes) for the ChaCha20 encryption algorithm.  The nonce provided is {0} bits {{0} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, data.Length), "data");
			}

			// Determine the # of blocks needed to encrypt our data.  The ChaCha20 block size is 512 bits (64 bytes).
			uint blocks = ((uint)data.Length) / BlockSize;
			if((data.Length % BlockSize) != 0)
				blocks++;

			// Generate the cipher block by block, then concatenate them together.
			byte[] Cipher = new byte[BlockSize * blocks];
			for(uint x = 0; x < blocks; x++) {
				byte[] blockCipher = GenerateBlockCipher(key, x + counter, nonce);
				Array.Copy(blockCipher, 0, Cipher, x * BlockSize, BlockSize);
			}

			// Take our plain text data and our cipher and XOR them to generate our cipher text.
			byte[] cipherText = new byte[data.Length];
			for(int x = 0; x < data.Length; x++)
				cipherText[x] = (byte)(data[x] ^ Cipher[x]);
			return cipherText;
		}

		// NOTE: Standard mode of operation.
		private static byte[] GenerateBlockCipher(byte[] key, uint counter, byte[] nonce) {
			// It is assumed that the input parameters are already checked by whichever public method they were originally passed to.

			// Set up the initial and working matrices before performing rounds of operations on them.
			Matrix initial = InitializeMatrix(key, counter, nonce);
			Matrix working = new Matrix(initial);

			// ChaCha20 calls for 20 rounds of ARX operations; 10 rounds of 2 different operation sets.
			for(int x = 0; x < 10; x++) {
				working.OddRound();
				working.EvenRound();
			}

			// Last step is to add each value in the initial matrix to the values in the matrix we've been working with.
			working = working + initial;

			// Get the byte array equivalent of the final matrix and return it.  That's the block cipher.
			return working.ExtractBytes();
		}

		// NOTE: This method creates a matrix where the counter is a 32-bit unsigned integer.  This is utilized by the ChaCha20 standard mode of operation which is capable of encrypting a smaller quantity of data (2 ^ 32 blocks * 512 bytes = 256 gigabytes), but utilizes larger nonces (96 bits; 12 bytes).
		private static Matrix InitializeMatrix(byte[] key, uint counter, byte[] nonce) {
			// It is assumed that the input parameters are already checked by whichever public method they were originally passed to.

			// Create initial state of our matrix:
			// -------------------------
			// |Cons |Cons |Cons |Cons |
			// -------------------------
			// |Key  |Key  |Key  |Key  |
			// -------------------------
			// |Key  |Key  |Key  |Key  |
			// -------------------------
			// |Count|Nonce|Nonce|Nonce|
			// -------------------------
			// Constant
			// Bit-string representation of ASCII string "expand 32-byte k".  This constant is determined by the ChaCha20 algorithm.
			// NOTE: The numbers below go right to left, so, for example, the first value looks like the bit string for "apxe", instead of "expa".  The bit strings appear to be "apxe", "3 dn", "yb-2", and "k et", instead of "expa", "nd 3", "2-by", and "te k".  This is because they are simply numbers in the context of the matrix, as opposed to bit strings.
			// Key
			// Convert the key bytes into integers.  There are 256 bits (32 bytes) of key material, and each integer is 32 bits (4 bytes).  This comes out to be 8 unsigned integers in the matrix.
			// Counter
			// The counter is merely a 32-bit unsigned integer.  We just put it in position 12 in the matrix.
			// Nonce
			// Convert the nonce bytes into integers.  There are 96 bits (12 bytes) of key material, and each integer is 32 bits (4 bytes).  This comes out to be 3 unsigned integers in the matrix.
			Matrix tmp = new Matrix(
				0x61707865, 0x3320646E, 0x79622D32, 0x6B206574,
				BitConverter.ToUInt32(key, 0), BitConverter.ToUInt32(key, 4), BitConverter.ToUInt32(key, 8), BitConverter.ToUInt32(key, 12),
				BitConverter.ToUInt32(key, 16), BitConverter.ToUInt32(key, 20), BitConverter.ToUInt32(key, 24), BitConverter.ToUInt32(key, 28),
				counter, BitConverter.ToUInt32(nonce, 0), BitConverter.ToUInt32(nonce, 4), BitConverter.ToUInt32(nonce, 8)
			);
			return tmp;
		}

		// NOTE: Alternate mode of operation.
		public static byte[] Encrypt(byte[] data, byte[] key, byte[] nonce, ulong counter) {
			// Checking input paramters for issues.
			if(key == null) {
				throw new ArgumentNullException("An empty key was provided; a 256-bit (32-byte) key must be provided to encrypt the message.", "key");
			}
			if(key.Length != 32) {
				int bits = key.Length * 8;
				string errorMessage = "A key must be 256 bits (32 bytes) for the ChaCha20 encryption algorithm.  The key provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, key.Length), "key");
			}
			if(nonce == null) {
				throw new ArgumentNullException("An empty nonce was provided; a 96-bit (12-byte) nonce must be provided to encrypt the message.", "nonce");
			}
			if(nonce.Length != 8) {
				int bits = nonce.Length * 8;
				string errorMessage = "A nonce must be 64 bits (8 bytes) for the ChaCha20 encryption algorithm.  The nonce provided is {0} bits {{1} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, bits, nonce.Length), "nonce");
			}
			if(data == null) {
				throw new ArgumentNullException("A null message was provided; please provide a non-null message.", "m");
			}
			// Is it valid for the data to be length 0?
			if(data.Length == 0) {
				string errorMessage = "The data must not be 0 bits (0 bytes) for the ChaCha20 encryption algorithm.  The nonce provided is {0} bits {{0} bytes) long.";
				throw new ArgumentException(String.Format(errorMessage, data.Length), "data");
			}

			// Determine the # of blocks needed to encrypt our data.  The ChaCha20 block size is 512 bits (64 bytes).
			uint blocks = ((uint)data.Length) / BlockSize;
			if((data.Length % BlockSize) != 0) {
				blocks++;
			}

			// Generate the cipher block by block, then concatenate them together.
			byte[] Cipher = new byte[BlockSize * blocks];
			for(uint x = 0; x < blocks; x++) {
				byte[] blockCipher = GenerateBlockCipher(key, x + counter, nonce);
				Array.Copy(blockCipher, 0, Cipher, x * BlockSize, BlockSize);
			}

			// Take our plain text data and our cipher and XOR them to generate our cipher text.
			byte[] cipherText = new byte[data.Length];
			for(int x = 0; x < data.Length; x++) {
				cipherText[x] = (byte)(data[x] ^ Cipher[x]);
			}
			return cipherText;
		}

		// NOTE: Alternate mode of operation.
		private static byte[] GenerateBlockCipher(byte[] key, ulong counter, byte[] nonce) {
			// It is assumed that the input parameters are already checked by whichever public method they were originally passed to.

			// Set up the initial and working matrices before performing rounds of operations on them.
			Matrix initial = InitializeMatrix(key, counter, nonce);
			Matrix working = new Matrix(initial);

			// ChaCha20 calls for 20 rounds of ARX operations; 10 rounds of 2 different operation sets.
			for(int x = 0; x < 10; x++) {
				working.OddRound();
				working.EvenRound();
			}

			// Last step is to add each value in the initial matrix to the values in the matrix we've been working with.
			working = working + initial;

			// Get the byte array equivalent of the final matrix and return it.  That's the block cipher.
			return working.ExtractBytes();
		}

		// NOTE: This method creates a matrix where the counter is a 64-bit unsigned integer.  This is utilized by the ChaCha20 mode of operation which is capable of encrypting a larger quantity of data (2 ^ 64 blocks * 512 bytes = 8 zettabytes), but utilizes smaller nonces (64 bits; 8 bytes).
		private static Matrix InitializeMatrix(byte[] key, ulong counter, byte[] nonce) {
			// It is assumed that the input parameters are already checked by whichever public method they were originally passed to.

			// Create initial state of our matrix:
			// -------------------------
			// |Cons |Cons |Cons |Cons |
			// -------------------------
			// |Key  |Key  |Key  |Key  |
			// -------------------------
			// |Key  |Key  |Key  |Key  |
			// -------------------------
			// |Count|Count|Nonce|Nonce|
			// -------------------------
			// Constant
			// Bit-string representation of ASCII string "expand 32-byte k".  This constant is determined by the ChaCha20 algorithm.
			// NOTE: The numbers below go right to left, so, for example, the first value looks like the bit string for "apxe", instead of "expa".  The bit strings appear to be "apxe", "3 dn", "yb-2", and "k et", instead of "expa", "nd 3", "2-by", and "te k".  This is because they are simply numbers in the context of the matrix, as opposed to bit strings.
			// Key
			// Convert the key bytes into integers.  There are 256 bits (32 bytes) of key material, and each integer is 32 bits (4 bytes).  This comes out to be 8 unsigned integers in the matrix.
			// Counter
			// The counter is a 64-bit unsigned integer.  We split it into 2 32-bit unsigned integers and put them in positions 12 and 13 in the matrix.
			// Nonce
			// Convert the nonce bytes into integers.  There are 64 bits (8 bytes) of key material, and each integer is 32 bits (4 bytes).  This comes out to be 2 unsigned integers in the matrix.
			Matrix tmp = new Matrix(
				0x61707865, 0x3320646E, 0x79622D32, 0x6B206574,
				BitConverter.ToUInt32(key, 00), BitConverter.ToUInt32(key, 04), BitConverter.ToUInt32(key, 08), BitConverter.ToUInt32(key, 12),
				BitConverter.ToUInt32(key, 16), BitConverter.ToUInt32(key, 20), BitConverter.ToUInt32(key, 24), BitConverter.ToUInt32(key, 28),
				// TODO: Learn which half of the counter belongs in which index.
				(uint)counter, (uint)(counter >> 32), BitConverter.ToUInt32(nonce, 0), BitConverter.ToUInt32(nonce, 4)
			);
			return tmp;
		}

		private class Matrix {
			// The matrix must never have any number of integers other than 16.  These are 4x4 matrices.
			public const uint MatrixSize = 16;
			public uint[] matrix;

			public Matrix(uint u00, uint u01, uint u02, uint u03, uint u04, uint u05, uint u06, uint u07, uint u08, uint u09, uint u10, uint u11, uint u12, uint u13, uint u14, uint u15) {
				matrix = new uint[MatrixSize];
				matrix[00] = u00;
				matrix[01] = u01;
				matrix[02] = u02;
				matrix[03] = u03;
				matrix[04] = u04;
				matrix[05] = u05;
				matrix[06] = u06;
				matrix[07] = u07;
				matrix[08] = u08;
				matrix[09] = u09;
				matrix[10] = u10;
				matrix[11] = u11;
				matrix[12] = u12;
				matrix[13] = u13;
				matrix[14] = u14;
				matrix[15] = u15;
			}

			// Don't use the assignment (=) operator to create a copy of a matrix; use this constructor instead.
			public Matrix(Matrix tmp) {
				// It is assumed that tmp has exactly 16 unsigned integers.
				matrix = new uint[MatrixSize];
				matrix[00] = tmp.matrix[00];
				matrix[01] = tmp.matrix[01];
				matrix[02] = tmp.matrix[02];
				matrix[03] = tmp.matrix[03];
				matrix[04] = tmp.matrix[04];
				matrix[05] = tmp.matrix[05];
				matrix[06] = tmp.matrix[06];
				matrix[07] = tmp.matrix[07];
				matrix[08] = tmp.matrix[08];
				matrix[09] = tmp.matrix[09];
				matrix[10] = tmp.matrix[10];
				matrix[11] = tmp.matrix[11];
				matrix[12] = tmp.matrix[12];
				matrix[13] = tmp.matrix[13];
				matrix[14] = tmp.matrix[14];
				matrix[15] = tmp.matrix[15];
			}

			public override string ToString() {
				string formatting = "{0,-10:X8}{1,-10:X8}{2,-10:X8}{3,8:X8}\r\n{4,-10:X8}{5,-10:X8}{6,-10:X8}{7,8:X8}\r\n" +
					"{8,-10:X8}{9,-10:X8}{10,-10:X8}{11,8:X8}\r\n{12,-10:X8}{13,-10:X8}{14,-10:X8}{15,8:X8}";
				return String.Format(formatting,
					matrix[00], matrix[01], matrix[02], matrix[03],
					matrix[04], matrix[05], matrix[06], matrix[07],
					matrix[08], matrix[09], matrix[10], matrix[11],
					matrix[12], matrix[13], matrix[14], matrix[15]);
			}

			public byte[] ExtractBytes() {
				byte[] bytes = new byte[MatrixSize * 4];
				for(int x = 0; x < MatrixSize; x++) {
					byte[] tmp = BitConverter.GetBytes(matrix[x]);
					Array.Copy(tmp, 0, bytes, x * 4, 4);
				}

				return bytes;
			}

			public void OddRound() {
				// Odd Round: We execute a Quarter Round against each column in the matrix.
				// Matrix:
				// -----------------
				// |00 |01 |02 |03 |
				// -----------------
				// |04 |05 |06 |07 |
				// -----------------
				// |08 |09 |10 |11 |
				// -----------------
				// |12 |13 |14 |15 |
				// -----------------

				// Column 1
				QuarterRound(00, 04, 08, 12);
				// Column 2
				QuarterRound(01, 05, 09, 13);
				// Column 3
				QuarterRound(02, 06, 10, 14);
				// Column 4
				QuarterRound(03, 07, 11, 15);
				return;
			}

			public void EvenRound() {
				// Even Round: We execute a Quarter Round against each diagonal in the matrix.
				// Matrix:
				// -----------------
				// |00 |01 |02 |03 |
				// -----------------
				// |04 |05 |06 |07 |
				// -----------------
				// |08 |09 |10 |11 |
				// -----------------
				// |12 |13 |14 |15 |
				// -----------------

				// Diagonal 1
				QuarterRound(00, 05, 10, 15);
				// Diagonal 2
				QuarterRound(01, 06, 11, 12);
				// Diagonal 3
				QuarterRound(02, 07, 08, 13);
				// Diagonal 4
				QuarterRound(03, 04, 09, 14);
				return;
			}

			private void QuarterRound(uint a, uint b, uint c, uint d) {
				// NOTE: The addition operations below utilize modular arithment around 2^32.  This is because all variables are uint.
				// NOTE: The bit shift operations below are rotational, not truncating.
				// a = a + b
				matrix[a] += matrix[b];
				// d = d XOR a
				matrix[d] ^= matrix[a];
				// d = d <<< 16
				matrix[d] = (matrix[d] << 16) | (matrix[d] >> 16);
				// c = c + d
				matrix[c] += matrix[d];
				// b = b XOR c
				matrix[b] ^= matrix[c];
				// b = b <<< 12
				matrix[b] = (matrix[b] << 12) | (matrix[b] >> 20);
				// a = a + b
				matrix[a] += matrix[b];
				// d = d XOR a
				matrix[d] ^= matrix[a];
				// d = d <<< 8
				matrix[d] = (matrix[d] << 8) | (matrix[d] >> 24);
				// c = c + d
				matrix[c] += matrix[d];
				// b = b XOR c
				matrix[b] ^= matrix[c];
				// b = b <<< 7
				matrix[b] = (matrix[b] << 7) | (matrix[b] >> 25);
			}

			public static Matrix operator +(Matrix a, Matrix b) {
				return new Matrix(
					a.matrix[00] + b.matrix[00],
					a.matrix[01] + b.matrix[01],
					a.matrix[02] + b.matrix[02],
					a.matrix[03] + b.matrix[03],
					a.matrix[04] + b.matrix[04],
					a.matrix[05] + b.matrix[05],
					a.matrix[06] + b.matrix[06],
					a.matrix[07] + b.matrix[07],
					a.matrix[08] + b.matrix[08],
					a.matrix[09] + b.matrix[09],
					a.matrix[10] + b.matrix[10],
					a.matrix[11] + b.matrix[11],
					a.matrix[12] + b.matrix[12],
					a.matrix[13] + b.matrix[13],
					a.matrix[14] + b.matrix[14],
					a.matrix[15] + b.matrix[15]
				);
			}
		}
	}
}
