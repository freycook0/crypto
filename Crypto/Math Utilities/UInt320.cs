using System;

// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/recommended-tags-for-documentation-comments

namespace Freycook0.Math {
	/**
		<summary>
		This class implements unsigned integers with 320 bits, allowing for the number range of 0 to 2.1359870359209100823950217061696...e+96 - 1.
		</summary>
		<remarks>
		The System.Numberics.BigInteger class was intended for similar use cases as this class, but it has limits as to how many bits it utilizes to represent numbers.  In testing, it was found that it did not retain the precision needed for calculations performed in the Poly1305 algorithm.
		</remarks>
*/
	public class UInt320 : IComparable {
		private const int _UIntLength = 10;
		private readonly uint _UInt00;
		private readonly uint _UInt01;
		private readonly uint _UInt02;
		private readonly uint _UInt03;
		private readonly uint _UInt04;
		private readonly uint _UInt05;
		private readonly uint _UInt06;
		private readonly uint _UInt07;
		private readonly uint _UInt08;
		private readonly uint _UInt09;

		public uint UInt00 { get { return _UInt00; } }
		public uint UInt01 { get { return _UInt01; } }
		public uint UInt02 { get { return _UInt02; } }
		public uint UInt03 { get { return _UInt03; } }
		public uint UInt04 { get { return _UInt04; } }
		public uint UInt05 { get { return _UInt05; } }
		public uint UInt06 { get { return _UInt06; } }
		public uint UInt07 { get { return _UInt07; } }
		public uint UInt08 { get { return _UInt08; } }
		public uint UInt09 { get { return _UInt09; } }

		public override bool Equals(object value) {
			UInt320 b = value as UInt320;
			bool equal = (this == b);   // == operator for UInt320 is defined further down.
			return equal;
		}

		public int CompareTo(object value) {
			UInt320 other = value as UInt320;
			if(this < other)
				return -1;
			else if(this == other)
				return 0;
			return 1;
		}

		// TODO: Actually implement a hashcode.
		public override int GetHashCode() {
			return 0;
			//return ((UInt09 >> 1) & 
		}

		public UInt320(uint i00 = 0, uint i01 = 0, uint i02 = 0, uint i03 = 0, uint i04 = 0, uint i05 = 0, uint i06 = 0, uint i07 = 0, uint i08 = 0, uint i09 = 0) {
			_UInt00 = i00;
			_UInt01 = i01;
			_UInt02 = i02;
			_UInt03 = i03;
			_UInt04 = i04;
			_UInt05 = i05;
			_UInt06 = i06;
			_UInt07 = i07;
			_UInt08 = i08;
			_UInt09 = i09;
		}

		public UInt320(UInt320 pI) {
			_UInt00 = pI.UInt00;
			_UInt01 = pI.UInt01;
			_UInt02 = pI.UInt02;
			_UInt03 = pI.UInt03;
			_UInt04 = pI.UInt04;
			_UInt05 = pI.UInt05;
			_UInt06 = pI.UInt06;
			_UInt07 = pI.UInt07;
			_UInt08 = pI.UInt08;
			_UInt09 = pI.UInt09;
		}

		public static int GetLengthBits(UInt320 lengthy) {
			int x = 0;
			UInt320 zero = new UInt320();
			UInt320 temp = lengthy;
			while(temp != zero) {
				x++;
				temp = temp >> 1;
			}
			return x;
		}

		public static int GetLengthBytes(UInt320 lengthy) {
			int x = 0;
			UInt320 zero = new UInt320();
			UInt320 temp = lengthy;
			while(temp != zero) {
				x++;
				temp = temp >> 8;
			}
			return x;
		}

		public override string ToString() {
			byte[] byteToString = new byte[_UIntLength * 4];//[40];
															// Switching from little endian to big endian.
															// 3,2,1,0 or 0,1,2,3?
															// 0,1,2,3 is the way to go...
			byteToString[39] = (byte)(_UInt00 >> (8 * 0));
			byteToString[38] = (byte)(_UInt00 >> (8 * 1));
			byteToString[37] = (byte)(_UInt00 >> (8 * 2));
			byteToString[36] = (byte)(_UInt00 >> (8 * 3));
			byteToString[35] = (byte)(_UInt01 >> (8 * 0));
			byteToString[34] = (byte)(_UInt01 >> (8 * 1));
			byteToString[33] = (byte)(_UInt01 >> (8 * 2));
			byteToString[32] = (byte)(_UInt01 >> (8 * 3));
			byteToString[31] = (byte)(_UInt02 >> (8 * 0));
			byteToString[30] = (byte)(_UInt02 >> (8 * 1));
			byteToString[29] = (byte)(_UInt02 >> (8 * 2));
			byteToString[28] = (byte)(_UInt02 >> (8 * 3));
			byteToString[27] = (byte)(_UInt03 >> (8 * 0));
			byteToString[26] = (byte)(_UInt03 >> (8 * 1));
			byteToString[25] = (byte)(_UInt03 >> (8 * 2));
			byteToString[24] = (byte)(_UInt03 >> (8 * 3));
			byteToString[23] = (byte)(_UInt04 >> (8 * 0));
			byteToString[22] = (byte)(_UInt04 >> (8 * 1));
			byteToString[21] = (byte)(_UInt04 >> (8 * 2));
			byteToString[20] = (byte)(_UInt04 >> (8 * 3));
			byteToString[19] = (byte)(_UInt05 >> (8 * 0));
			byteToString[18] = (byte)(_UInt05 >> (8 * 1));
			byteToString[17] = (byte)(_UInt05 >> (8 * 2));
			byteToString[16] = (byte)(_UInt05 >> (8 * 3));
			byteToString[15] = (byte)(_UInt06 >> (8 * 0));
			byteToString[14] = (byte)(_UInt06 >> (8 * 1));
			byteToString[13] = (byte)(_UInt06 >> (8 * 2));
			byteToString[12] = (byte)(_UInt06 >> (8 * 3));
			byteToString[11] = (byte)(_UInt07 >> (8 * 0));
			byteToString[10] = (byte)(_UInt07 >> (8 * 1));
			byteToString[09] = (byte)(_UInt07 >> (8 * 2));
			byteToString[08] = (byte)(_UInt07 >> (8 * 3));
			byteToString[07] = (byte)(_UInt08 >> (8 * 0));
			byteToString[06] = (byte)(_UInt08 >> (8 * 1));
			byteToString[05] = (byte)(_UInt08 >> (8 * 2));
			byteToString[04] = (byte)(_UInt08 >> (8 * 3));
			byteToString[03] = (byte)(_UInt09 >> (8 * 0));
			byteToString[02] = (byte)(_UInt09 >> (8 * 1));
			byteToString[01] = (byte)(_UInt09 >> (8 * 2));
			byteToString[00] = (byte)(_UInt09 >> (8 * 3));
			string bleh = BitConverter.ToString(byteToString).Replace("-", String.Empty);//' '
			bleh = bleh.TrimStart('0');
			return bleh;
		}

		public static UInt320 operator +(UInt320 a, UInt320 b) {
			uint overflow = 0;
			uint i00;
			try { i00 = checked(a.UInt00 + b.UInt00 + overflow); overflow = 0; } catch { i00 = a.UInt00 + b.UInt00 + overflow; overflow = 1; }
			uint i01;
			try { i01 = checked(a.UInt01 + b.UInt01 + overflow); overflow = 0; } catch { i01 = a.UInt01 + b.UInt01 + overflow; overflow = 1; }
			uint i02;
			try { i02 = checked(a.UInt02 + b.UInt02 + overflow); overflow = 0; } catch { i02 = a.UInt02 + b.UInt02 + overflow; overflow = 1; }
			uint i03;
			try { i03 = checked(a.UInt03 + b.UInt03 + overflow); overflow = 0; } catch { i03 = a.UInt03 + b.UInt03 + overflow; overflow = 1; }
			uint i04;
			try { i04 = checked(a.UInt04 + b.UInt04 + overflow); overflow = 0; } catch { i04 = a.UInt04 + b.UInt04 + overflow; overflow = 1; }
			uint i05;
			try { i05 = checked(a.UInt05 + b.UInt05 + overflow); overflow = 0; } catch { i05 = a.UInt05 + b.UInt05 + overflow; overflow = 1; }
			uint i06;
			try { i06 = checked(a.UInt06 + b.UInt06 + overflow); overflow = 0; } catch { i06 = a.UInt06 + b.UInt06 + overflow; overflow = 1; }
			uint i07;
			try { i07 = checked(a.UInt07 + b.UInt07 + overflow); overflow = 0; } catch { i07 = a.UInt07 + b.UInt07 + overflow; overflow = 1; }
			uint i08;
			try { i08 = checked(a.UInt08 + b.UInt08 + overflow); overflow = 0; } catch { i08 = a.UInt08 + b.UInt08 + overflow; overflow = 1; }
			uint i09;
			try { i09 = checked(a.UInt09 + b.UInt09 + overflow); overflow = 0; } catch { i09 = a.UInt09 + b.UInt09 + overflow; overflow = 1; }

			return new UInt320(i00, i01, i02, i03, i04, i05, i06, i07, i08, i09);
		}

		public static UInt320 operator +(UInt320 a, uint b) {
			UInt320 bPInt = new UInt320(b);
			return a + bPInt;
		}

		public static UInt320 operator +(uint b, UInt320 a) {
			UInt320 bPInt = new UInt320(b);
			return a + bPInt;
		}

		public static UInt320 operator -(UInt320 a, UInt320 b) {
			uint overflow = 0;
			uint i00;
			try {
				i00 = checked(a.UInt00 - b.UInt00 - overflow);
				overflow = 0;
			}
			catch {
				i00 = a.UInt00 - b.UInt00 - overflow;
				overflow = 1;
			}
			uint i01;
			try { i01 = checked(a.UInt01 - b.UInt01 - overflow); overflow = 0; } catch { i01 = a.UInt01 - b.UInt01 - overflow; overflow = 1; }
			uint i02;
			try { i02 = checked(a.UInt02 - b.UInt02 - overflow); overflow = 0; } catch { i02 = a.UInt02 - b.UInt02 - overflow; overflow = 1; }
			uint i03;
			try { i03 = checked(a.UInt03 - b.UInt03 - overflow); overflow = 0; } catch { i03 = a.UInt03 - b.UInt03 - overflow; overflow = 1; }
			uint i04;
			try { i04 = checked(a.UInt04 - b.UInt04 - overflow); overflow = 0; } catch { i04 = a.UInt04 - b.UInt04 - overflow; overflow = 1; }
			uint i05;
			try { i05 = checked(a.UInt05 - b.UInt05 - overflow); overflow = 0; } catch { i05 = a.UInt05 - b.UInt05 - overflow; overflow = 1; }
			uint i06;
			try { i06 = checked(a.UInt06 - b.UInt06 - overflow); overflow = 0; } catch { i06 = a.UInt06 - b.UInt06 - overflow; overflow = 1; }
			uint i07;
			try { i07 = checked(a.UInt07 - b.UInt07 - overflow); overflow = 0; } catch { i07 = a.UInt07 - b.UInt07 - overflow; overflow = 1; }
			uint i08;
			try { i08 = checked(a.UInt08 - b.UInt08 - overflow); overflow = 0; } catch { i08 = a.UInt08 - b.UInt08 - overflow; overflow = 1; }
			uint i09;
			try { i09 = checked(a.UInt09 - b.UInt09 - overflow); overflow = 0; } catch { i09 = a.UInt09 - b.UInt09 - overflow; overflow = 1; }

			return new UInt320(i00, i01, i02, i03, i04, i05, i06, i07, i08, i09);
		}

		public static UInt320 operator *(UInt320 a, UInt320 b) {
			UInt320 aggregate = new UInt320();
			uint[] aUInts = new uint[_UIntLength];
			uint[] bUInts = new uint[_UIntLength];
			aUInts[00] = a.UInt00;
			aUInts[01] = a.UInt01;
			aUInts[02] = a.UInt02;
			aUInts[03] = a.UInt03;
			aUInts[04] = a.UInt04;
			aUInts[05] = a.UInt05;
			aUInts[06] = a.UInt06;
			aUInts[07] = a.UInt07;
			aUInts[08] = a.UInt08;
			aUInts[09] = a.UInt09;
			bUInts[00] = b.UInt00;
			bUInts[01] = b.UInt01;
			bUInts[02] = b.UInt02;
			bUInts[03] = b.UInt03;
			bUInts[04] = b.UInt04;
			bUInts[05] = b.UInt05;
			bUInts[06] = b.UInt06;
			bUInts[07] = b.UInt07;
			bUInts[08] = b.UInt08;
			bUInts[09] = b.UInt09;
			for(int x = 0; x < _UIntLength; x++) {
				ulong holder = 0;
				uint overflow = 0;
				uint[] rUInts = new uint[_UIntLength];
				UInt320 temp = new UInt320();
				for(int y = 0; y + x < _UIntLength; y++) {
					holder = (ulong)((((ulong)aUInts[x]) * ((ulong)bUInts[y])) + ((ulong)overflow));
					rUInts[x + y] = (uint)holder;
					overflow = (uint)(holder >> 32);
				}
				temp = new UInt320(rUInts[00], rUInts[01], rUInts[02], rUInts[03], rUInts[04], rUInts[05], rUInts[06], rUInts[07], rUInts[08], rUInts[09]);
				aggregate = aggregate + temp;
			}
			return aggregate;
		}

		// Source where I found the steps to implement constant-time division: https://www.geeksforgeeks.org/divide-two-integers-without-using-multiplication-division-mod-operator/
		public static UInt320 operator /(UInt320 a, UInt320 b) {
			UInt320 result = new UInt320(0);
			if(b == result)
				throw new DivideByZeroException();
			if(b > a)
				return result;

			UInt320 temp = new UInt320(0);
			UInt320 q = new UInt320(0);
			UInt320 one = new UInt320(1);
			// TODO: Decide if hardcoding i's initial value to 159 is acceptable, or if some dynamic assignment is warranted.
			// One thought: If your division requires a number higher that 2^159, then fuck you.
			// Another thought: Why not figure this out properly?  And provide a complete solution?
			for(int i = 159; i >= 0; i--) {
				UInt320 calc = temp + (b << i);
				if(calc <= a) {
					temp = calc;
					q = q | (one << i);
				}
			}
			return q;
		}

		public static UInt320 operator %(UInt320 a, UInt320 b) {
			UInt320 result = a - ((a / b) * b);
			return result;
		}

		public static UInt320 operator &(UInt320 a, UInt320 b) {
			uint i00 = a.UInt00 & b.UInt00;
			uint i01 = a.UInt01 & b.UInt01;
			uint i02 = a.UInt02 & b.UInt02;
			uint i03 = a.UInt03 & b.UInt03;
			uint i04 = a.UInt04 & b.UInt04;
			uint i05 = a.UInt05 & b.UInt05;
			uint i06 = a.UInt06 & b.UInt06;
			uint i07 = a.UInt07 & b.UInt07;
			uint i08 = a.UInt08 & b.UInt08;
			uint i09 = a.UInt09 & b.UInt09;
			return new UInt320(i00, i01, i02, i03, i04, i05, i06, i07, i08, i09);
		}

		public static UInt320 operator |(UInt320 a, UInt320 b) {
			uint i00 = a.UInt00 | b.UInt00;
			uint i01 = a.UInt01 | b.UInt01;
			uint i02 = a.UInt02 | b.UInt02;
			uint i03 = a.UInt03 | b.UInt03;
			uint i04 = a.UInt04 | b.UInt04;
			uint i05 = a.UInt05 | b.UInt05;
			uint i06 = a.UInt06 | b.UInt06;
			uint i07 = a.UInt07 | b.UInt07;
			uint i08 = a.UInt08 | b.UInt08;
			uint i09 = a.UInt09 | b.UInt09;
			return new UInt320(i00, i01, i02, i03, i04, i05, i06, i07, i08, i09);
		}

		public static UInt320 operator ^(UInt320 a, UInt320 b) {
			uint i00 = a.UInt00 ^ b.UInt00;
			uint i01 = a.UInt01 ^ b.UInt01;
			uint i02 = a.UInt02 ^ b.UInt02;
			uint i03 = a.UInt03 ^ b.UInt03;
			uint i04 = a.UInt04 ^ b.UInt04;
			uint i05 = a.UInt05 ^ b.UInt05;
			uint i06 = a.UInt06 ^ b.UInt06;
			uint i07 = a.UInt07 ^ b.UInt07;
			uint i08 = a.UInt08 ^ b.UInt08;
			uint i09 = a.UInt09 ^ b.UInt09;
			return new UInt320(i00, i01, i02, i03, i04, i05, i06, i07, i08, i09);
		}

		public static UInt320 operator <<(UInt320 a, int b) {
			// If shifting by 320 bits or more, then go ahead and return 0.
			if(b >= 320) {
				return new UInt320();
			}
			UInt320 temp = a;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(b / 32);
			int remainder = b % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt320(0, temp.UInt00, temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06, temp.UInt07, temp.UInt08);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 << remainder);// | (temp.UInt09 >> (32 - remainder));
			uint result01 = (temp.UInt01 << remainder) | (temp.UInt00 >> (32 - remainder));
			uint result02 = (temp.UInt02 << remainder) | (temp.UInt01 >> (32 - remainder));
			uint result03 = (temp.UInt03 << remainder) | (temp.UInt02 >> (32 - remainder));
			uint result04 = (temp.UInt04 << remainder) | (temp.UInt03 >> (32 - remainder));
			uint result05 = (temp.UInt05 << remainder) | (temp.UInt04 >> (32 - remainder));
			uint result06 = (temp.UInt06 << remainder) | (temp.UInt05 >> (32 - remainder));
			uint result07 = (temp.UInt07 << remainder) | (temp.UInt06 >> (32 - remainder));
			uint result08 = (temp.UInt08 << remainder) | (temp.UInt07 >> (32 - remainder));
			uint result09 = (temp.UInt09 << remainder) | (temp.UInt08 >> (32 - remainder));
			temp = new UInt320(result00, result01, result02, result03, result04, result05, result06, result07, result08, result09);
			return temp;
		}

		// NOTE: This is a bit rotate operation, not just a bit shift operation.  The nth-most bits of the UInt320 object are brought around to the right side.
		//public static UInt320 operator<<<(UInt320 a, int b){
		public static UInt320 RotateLeft(UInt320 a, int b) {
			UInt320 temp = a;
			// There's no need to rotate by more than 320 bits, as rotating a 320 bit unsigned integer by 320 bits results in the same value.
			int bTemp = b % 320;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(bTemp / 32);
			int remainder = bTemp % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt320(temp.UInt09, temp.UInt00, temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06, temp.UInt07, temp.UInt08);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 << remainder) | (temp.UInt09 >> (32 - remainder));
			uint result01 = (temp.UInt01 << remainder) | (temp.UInt00 >> (32 - remainder));
			uint result02 = (temp.UInt02 << remainder) | (temp.UInt01 >> (32 - remainder));
			uint result03 = (temp.UInt03 << remainder) | (temp.UInt02 >> (32 - remainder));
			uint result04 = (temp.UInt04 << remainder) | (temp.UInt03 >> (32 - remainder));
			uint result05 = (temp.UInt05 << remainder) | (temp.UInt04 >> (32 - remainder));
			uint result06 = (temp.UInt06 << remainder) | (temp.UInt05 >> (32 - remainder));
			uint result07 = (temp.UInt07 << remainder) | (temp.UInt06 >> (32 - remainder));
			uint result08 = (temp.UInt08 << remainder) | (temp.UInt07 >> (32 - remainder));
			uint result09 = (temp.UInt09 << remainder) | (temp.UInt08 >> (32 - remainder));
			temp = new UInt320(result00, result01, result02, result03, result04, result05, result06, result07, result08, result09);
			return temp;
		}

		public static UInt320 operator >>(UInt320 a, int b) {
			// If shifting by 320 bits or more, then go ahead and return 0.
			if(b >= 320) {
				return new UInt320();
			}
			UInt320 temp = a;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(b / 32);
			int remainder = b % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt320(temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06, temp.UInt07, temp.UInt08, temp.UInt09, 0);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 >> remainder) | (temp.UInt01 << (32 - remainder));
			uint result01 = (temp.UInt01 >> remainder) | (temp.UInt02 << (32 - remainder));
			uint result02 = (temp.UInt02 >> remainder) | (temp.UInt03 << (32 - remainder));
			uint result03 = (temp.UInt03 >> remainder) | (temp.UInt04 << (32 - remainder));
			uint result04 = (temp.UInt04 >> remainder) | (temp.UInt05 << (32 - remainder));
			uint result05 = (temp.UInt05 >> remainder) | (temp.UInt06 << (32 - remainder));
			uint result06 = (temp.UInt06 >> remainder) | (temp.UInt07 << (32 - remainder));
			uint result07 = (temp.UInt07 >> remainder) | (temp.UInt08 << (32 - remainder));
			uint result08 = (temp.UInt08 >> remainder) | (temp.UInt09 << (32 - remainder));
			uint result09 = (temp.UInt09 >> remainder);// | (temp.UInt00 << (32 - remainder));
			temp = new UInt320(result00, result01, result02, result03, result04, result05, result06, result07, result08, result09);
			return temp;
		}

		// NOTE: This is a bit rotate operation, not just a bit shift operation.  The nth-most bits of the UInt320 object are brought around to the right side.
		//public static UInt320 operator>>>(UInt320 a, int b){
		public static UInt320 RotateRight(UInt320 a, int b) {
			UInt320 temp = a;
			// There's no need to rotate by more than 320 bits, as rotating a 320 bit unsigned integer by 320 bits results in the same value.
			int bTemp = b % 320;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(bTemp / 32);
			int remainder = bTemp % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt320(temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06, temp.UInt07, temp.UInt08, temp.UInt09, temp.UInt00);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 >> remainder) | (temp.UInt01 << (32 - remainder));
			uint result01 = (temp.UInt01 >> remainder) | (temp.UInt02 << (32 - remainder));
			uint result02 = (temp.UInt02 >> remainder) | (temp.UInt03 << (32 - remainder));
			uint result03 = (temp.UInt03 >> remainder) | (temp.UInt04 << (32 - remainder));
			uint result04 = (temp.UInt04 >> remainder) | (temp.UInt05 << (32 - remainder));
			uint result05 = (temp.UInt05 >> remainder) | (temp.UInt06 << (32 - remainder));
			uint result06 = (temp.UInt06 >> remainder) | (temp.UInt07 << (32 - remainder));
			uint result07 = (temp.UInt07 >> remainder) | (temp.UInt08 << (32 - remainder));
			uint result08 = (temp.UInt08 >> remainder) | (temp.UInt09 << (32 - remainder));
			uint result09 = (temp.UInt09 >> remainder) | (temp.UInt00 << (32 - remainder));
			temp = new UInt320(result00, result01, result02, result03, result04, result05, result06, result07, result08, result09);
			return temp;
		}

		public static bool operator ==(UInt320 a, UInt320 b) {
			bool b00 = a._UInt00 == b._UInt00;
			bool b01 = a._UInt01 == b._UInt01;
			bool b02 = a._UInt02 == b._UInt02;
			bool b03 = a._UInt03 == b._UInt03;
			bool b04 = a._UInt04 == b._UInt04;
			bool b05 = a._UInt05 == b._UInt05;
			bool b06 = a._UInt06 == b._UInt06;
			bool b07 = a._UInt07 == b._UInt07;
			bool b08 = a._UInt08 == b._UInt08;
			bool b09 = a._UInt09 == b._UInt09;
			bool result = b00 && b01 && b02 && b03 && b04 && b05 && b06 && b07 && b08 && b09;
			return result;
		}

		public static bool operator !=(UInt320 a, UInt320 b) {
			bool b00 = a._UInt00 != b._UInt00;
			bool b01 = a._UInt01 != b._UInt01;
			bool b02 = a._UInt02 != b._UInt02;
			bool b03 = a._UInt03 != b._UInt03;
			bool b04 = a._UInt04 != b._UInt04;
			bool b05 = a._UInt05 != b._UInt05;
			bool b06 = a._UInt06 != b._UInt06;
			bool b07 = a._UInt07 != b._UInt07;
			bool b08 = a._UInt08 != b._UInt08;
			bool b09 = a._UInt09 != b._UInt09;
			bool result = b00 || b01 || b02 || b03 || b04 || b05 || b06 || b07 || b08 || b09;
			return result;
		}

		public static bool operator >=(UInt320 a, UInt320 b) {
			if(a._UInt09 > b._UInt09)
				return true;
			else if(a._UInt09 == b._UInt09) {
				if(a._UInt08 > b._UInt08)
					return true;
				else if(a._UInt08 == b._UInt08) {
					if(a._UInt07 > b._UInt07)
						return true;
					else if(a._UInt07 == b._UInt07) {
						if(a._UInt06 > b._UInt06)
							return true;
						else if(a._UInt06 == b._UInt06) {
							if(a._UInt05 > b._UInt05)
								return true;
							else if(a._UInt05 == b._UInt05) {
								if(a._UInt04 > b._UInt04)
									return true;
								else if(a._UInt04 == b._UInt04) {
									if(a._UInt03 > b._UInt03)
										return true;
									else if(a._UInt03 == b._UInt03) {
										if(a._UInt02 > b._UInt02)
											return true;
										else if(a._UInt02 == b._UInt02) {
											if(a._UInt01 > b._UInt01)
												return true;
											else if(a._UInt01 == b._UInt01) {
												if(a._UInt00 > b._UInt00 || a._UInt00 == b._UInt00)
													return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		public static bool operator >(UInt320 a, UInt320 b) {
			if(a._UInt09 > b._UInt09)
				return true;
			else if(a._UInt09 == b._UInt09) {
				if(a._UInt08 > b._UInt08)
					return true;
				else if(a._UInt08 == b._UInt08) {
					if(a._UInt07 > b._UInt07)
						return true;
					else if(a._UInt07 == b._UInt07) {
						if(a._UInt06 > b._UInt06)
							return true;
						else if(a._UInt06 == b._UInt06) {
							if(a._UInt05 > b._UInt05)
								return true;
							else if(a._UInt05 == b._UInt05) {
								if(a._UInt04 > b._UInt04)
									return true;
								else if(a._UInt04 == b._UInt04) {
									if(a._UInt03 > b._UInt03)
										return true;
									else if(a._UInt03 == b._UInt03) {
										if(a._UInt02 > b._UInt02)
											return true;
										else if(a._UInt02 == b._UInt02) {
											if(a._UInt01 > b._UInt01)
												return true;
											else if(a._UInt01 == b._UInt01) {
												if(a._UInt00 > b._UInt00)
													return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		public static bool operator <=(UInt320 a, UInt320 b) {
			if(a._UInt09 < b._UInt09)
				return true;
			else if(a._UInt09 == b._UInt09) {
				if(a._UInt08 < b._UInt08)
					return true;
				else if(a._UInt08 == b._UInt08) {
					if(a._UInt07 < b._UInt07)
						return true;
					else if(a._UInt07 == b._UInt07) {
						if(a._UInt06 < b._UInt06)
							return true;
						else if(a._UInt06 == b._UInt06) {
							if(a._UInt05 < b._UInt05)
								return true;
							else if(a._UInt05 == b._UInt05) {
								if(a._UInt04 < b._UInt04)
									return true;
								else if(a._UInt04 == b._UInt04) {
									if(a._UInt03 < b._UInt03)
										return true;
									else if(a._UInt03 == b._UInt03) {
										if(a._UInt02 < b._UInt02)
											return true;
										else if(a._UInt02 == b._UInt02) {
											if(a._UInt01 < b._UInt01)
												return true;
											else if(a._UInt01 == b._UInt01) {
												if(a._UInt00 < b._UInt00 || a._UInt00 == b._UInt00)
													return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		public static bool operator <(UInt320 a, UInt320 b) {
			if(a._UInt09 < b._UInt09)
				return true;
			else if(a._UInt09 == b._UInt09) {
				if(a._UInt08 < b._UInt08)
					return true;
				else if(a._UInt08 == b._UInt08) {
					if(a._UInt07 < b._UInt07)
						return true;
					else if(a._UInt07 == b._UInt07) {
						if(a._UInt06 < b._UInt06)
							return true;
						else if(a._UInt06 == b._UInt06) {
							if(a._UInt05 < b._UInt05)
								return true;
							else if(a._UInt05 == b._UInt05) {
								if(a._UInt04 < b._UInt04)
									return true;
								else if(a._UInt04 == b._UInt04) {
									if(a._UInt03 < b._UInt03)
										return true;
									else if(a._UInt03 == b._UInt03) {
										if(a._UInt02 < b._UInt02)
											return true;
										else if(a._UInt02 == b._UInt02) {
											if(a._UInt01 < b._UInt01)
												return true;
											else if(a._UInt01 == b._UInt01) {
												if(a._UInt00 < b._UInt00)
													return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
			return false;
		}
	}
}
