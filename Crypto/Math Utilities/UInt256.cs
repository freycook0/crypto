using System;

// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/xmldoc/recommended-tags-for-documentation-comments

namespace Freycook0.Math {
	/**
		<summary>
		This class implements unsigned integers with 256 bits, allowing for the number range of 0 to 1.1579208923731619542357098500869...e+77 - 1. 
		</summary>
		<remarks>
		The System.Numberics.BigInteger class was intended for similar use cases as this class, but it has limits as to how many bits it utilizes to represent numbers.  In testing, it was found that it did not retain the precision needed for calculations performed in the Poly1305 algorithm.
		</remarks>
*/
	public class UInt256 : IComparable {
		private const int _UIntLength = 8;
		private readonly uint _UInt00;
		private readonly uint _UInt01;
		private readonly uint _UInt02;
		private readonly uint _UInt03;
		private readonly uint _UInt04;
		private readonly uint _UInt05;
		private readonly uint _UInt06;
		private readonly uint _UInt07;

		public uint UInt00 { get { return _UInt00; } }
		public uint UInt01 { get { return _UInt01; } }
		public uint UInt02 { get { return _UInt02; } }
		public uint UInt03 { get { return _UInt03; } }
		public uint UInt04 { get { return _UInt04; } }
		public uint UInt05 { get { return _UInt05; } }
		public uint UInt06 { get { return _UInt06; } }
		public uint UInt07 { get { return _UInt07; } }

		public override bool Equals(object value) {
			UInt256 b = value as UInt256;
			bool equal = (this == b);   // == operator for UInt256 is defined further down.
			return equal;
		}

		public int CompareTo(object value) {
			UInt256 other = value as UInt256;
			if(this < other)
				return -1;
			else if(this == other)
				return 0;
			return 1;
		}

		// TODO: Actually implement a hashcode.
		public override int GetHashCode() {
			return 0;
			//return ((UInt09 >> 1) & 
		}

		public UInt256(uint i00 = 0, uint i01 = 0, uint i02 = 0, uint i03 = 0, uint i04 = 0, uint i05 = 0, uint i06 = 0, uint i07 = 0) {
			_UInt00 = i00;
			_UInt01 = i01;
			_UInt02 = i02;
			_UInt03 = i03;
			_UInt04 = i04;
			_UInt05 = i05;
			_UInt06 = i06;
			_UInt07 = i07;
		}

		public UInt256(UInt256 pI) {
			_UInt00 = pI.UInt00;
			_UInt01 = pI.UInt01;
			_UInt02 = pI.UInt02;
			_UInt03 = pI.UInt03;
			_UInt04 = pI.UInt04;
			_UInt05 = pI.UInt05;
			_UInt06 = pI.UInt06;
			_UInt07 = pI.UInt07;
		}

		public static int GetLengthBits(UInt256 lengthy) {
			int x = 0;
			UInt256 zero = new UInt256();
			UInt256 temp = lengthy;
			while(temp != zero) {
				x++;
				temp = temp >> 1;
			}
			return x;
		}

		public static int GetLengthBytes(UInt256 lengthy) {
			int x = 0;
			UInt256 zero = new UInt256();
			UInt256 temp = lengthy;
			while(temp != zero) {
				x++;
				temp = temp >> 8;
			}
			return x;
		}

		public override string ToString() {
			byte[] byteToString = new byte[_UIntLength * 4];//[40];
															// Switching from little endian to big endian.
															// 3,2,1,0 or 0,1,2,3?
															// 0,1,2,3 is the way to go...
			byteToString[31] = (byte)(_UInt00 >> (8 * 0));
			byteToString[30] = (byte)(_UInt00 >> (8 * 1));
			byteToString[29] = (byte)(_UInt00 >> (8 * 2));
			byteToString[28] = (byte)(_UInt00 >> (8 * 3));
			byteToString[27] = (byte)(_UInt01 >> (8 * 0));
			byteToString[26] = (byte)(_UInt01 >> (8 * 1));
			byteToString[25] = (byte)(_UInt01 >> (8 * 2));
			byteToString[24] = (byte)(_UInt01 >> (8 * 3));
			byteToString[23] = (byte)(_UInt02 >> (8 * 0));
			byteToString[22] = (byte)(_UInt02 >> (8 * 1));
			byteToString[21] = (byte)(_UInt02 >> (8 * 2));
			byteToString[20] = (byte)(_UInt02 >> (8 * 3));
			byteToString[19] = (byte)(_UInt03 >> (8 * 0));
			byteToString[18] = (byte)(_UInt03 >> (8 * 1));
			byteToString[17] = (byte)(_UInt03 >> (8 * 2));
			byteToString[16] = (byte)(_UInt03 >> (8 * 3));
			byteToString[15] = (byte)(_UInt04 >> (8 * 0));
			byteToString[14] = (byte)(_UInt04 >> (8 * 1));
			byteToString[13] = (byte)(_UInt04 >> (8 * 2));
			byteToString[12] = (byte)(_UInt04 >> (8 * 3));
			byteToString[11] = (byte)(_UInt05 >> (8 * 0));
			byteToString[10] = (byte)(_UInt05 >> (8 * 1));
			byteToString[09] = (byte)(_UInt05 >> (8 * 2));
			byteToString[08] = (byte)(_UInt05 >> (8 * 3));
			byteToString[07] = (byte)(_UInt06 >> (8 * 0));
			byteToString[06] = (byte)(_UInt06 >> (8 * 1));
			byteToString[05] = (byte)(_UInt06 >> (8 * 2));
			byteToString[04] = (byte)(_UInt06 >> (8 * 3));
			byteToString[03] = (byte)(_UInt07 >> (8 * 0));
			byteToString[02] = (byte)(_UInt07 >> (8 * 1));
			byteToString[01] = (byte)(_UInt07 >> (8 * 2));
			byteToString[00] = (byte)(_UInt07 >> (8 * 3));
			string bleh = BitConverter.ToString(byteToString).Replace("-", String.Empty);//' '
			bleh = bleh.TrimStart('0');
			if(bleh == String.Empty)
				bleh = "0";
			return bleh;
		}

		public static UInt256 operator +(UInt256 a, UInt256 b) {
			uint overflow = 0;
			uint i00;
			try { i00 = checked(a.UInt00 + b.UInt00 + overflow); overflow = 0; } catch { i00 = a.UInt00 + b.UInt00 + overflow; overflow = 1; }
			uint i01;
			try { i01 = checked(a.UInt01 + b.UInt01 + overflow); overflow = 0; } catch { i01 = a.UInt01 + b.UInt01 + overflow; overflow = 1; }
			uint i02;
			try { i02 = checked(a.UInt02 + b.UInt02 + overflow); overflow = 0; } catch { i02 = a.UInt02 + b.UInt02 + overflow; overflow = 1; }
			uint i03;
			try { i03 = checked(a.UInt03 + b.UInt03 + overflow); overflow = 0; } catch { i03 = a.UInt03 + b.UInt03 + overflow; overflow = 1; }
			uint i04;
			try { i04 = checked(a.UInt04 + b.UInt04 + overflow); overflow = 0; } catch { i04 = a.UInt04 + b.UInt04 + overflow; overflow = 1; }
			uint i05;
			try { i05 = checked(a.UInt05 + b.UInt05 + overflow); overflow = 0; } catch { i05 = a.UInt05 + b.UInt05 + overflow; overflow = 1; }
			uint i06;
			try { i06 = checked(a.UInt06 + b.UInt06 + overflow); overflow = 0; } catch { i06 = a.UInt06 + b.UInt06 + overflow; overflow = 1; }
			uint i07;
			try { i07 = checked(a.UInt07 + b.UInt07 + overflow); overflow = 0; } catch { i07 = a.UInt07 + b.UInt07 + overflow; overflow = 1; }

			return new UInt256(i00, i01, i02, i03, i04, i05, i06, i07);
		}

		public static UInt256 operator +(UInt256 a, uint b) {
			UInt256 bPInt = new UInt256(b);
			return a + bPInt;
		}

		public static UInt256 operator +(uint b, UInt256 a) {
			UInt256 bPInt = new UInt256(b);
			return a + bPInt;
		}

		public static UInt256 operator -(UInt256 a, UInt256 b) {
			uint overflow = 0;
			uint i00;
			try { i00 = checked(a.UInt00 - b.UInt00 - overflow); overflow = 0; } catch { i00 = a.UInt00 - b.UInt00 - overflow; overflow = 1; }
			uint i01;
			try { i01 = checked(a.UInt01 - b.UInt01 - overflow); overflow = 0; } catch { i01 = a.UInt01 - b.UInt01 - overflow; overflow = 1; }
			uint i02;
			try { i02 = checked(a.UInt02 - b.UInt02 - overflow); overflow = 0; } catch { i02 = a.UInt02 - b.UInt02 - overflow; overflow = 1; }
			uint i03;
			try { i03 = checked(a.UInt03 - b.UInt03 - overflow); overflow = 0; } catch { i03 = a.UInt03 - b.UInt03 - overflow; overflow = 1; }
			uint i04;
			try { i04 = checked(a.UInt04 - b.UInt04 - overflow); overflow = 0; } catch { i04 = a.UInt04 - b.UInt04 - overflow; overflow = 1; }
			uint i05;
			try { i05 = checked(a.UInt05 - b.UInt05 - overflow); overflow = 0; } catch { i05 = a.UInt05 - b.UInt05 - overflow; overflow = 1; }
			uint i06;
			try { i06 = checked(a.UInt06 - b.UInt06 - overflow); overflow = 0; } catch { i06 = a.UInt06 - b.UInt06 - overflow; overflow = 1; }
			uint i07;
			try { i07 = checked(a.UInt07 - b.UInt07 - overflow); overflow = 0; } catch { i07 = a.UInt07 - b.UInt07 - overflow; } // overflow = 1; }

			return new UInt256(i00, i01, i02, i03, i04, i05, i06, i07);
		}

		public static UInt256 operator *(UInt256 a, UInt256 b) {
			UInt256 aggregate = new UInt256();
			uint[] aUInts = new uint[_UIntLength];
			uint[] bUInts = new uint[_UIntLength];
			aUInts[00] = a.UInt00;
			aUInts[01] = a.UInt01;
			aUInts[02] = a.UInt02;
			aUInts[03] = a.UInt03;
			aUInts[04] = a.UInt04;
			aUInts[05] = a.UInt05;
			aUInts[06] = a.UInt06;
			aUInts[07] = a.UInt07;
			bUInts[00] = b.UInt00;
			bUInts[01] = b.UInt01;
			bUInts[02] = b.UInt02;
			bUInts[03] = b.UInt03;
			bUInts[04] = b.UInt04;
			bUInts[05] = b.UInt05;
			bUInts[06] = b.UInt06;
			bUInts[07] = b.UInt07;
			for(int x = 0; x < _UIntLength; x++) {
				ulong holder = 0;
				uint overflow = 0;
				uint[] rUInts = new uint[_UIntLength];
				UInt256 temp = new UInt256();
				for(int y = 0; y + x < _UIntLength; y++) {
					holder = (ulong)((((ulong)aUInts[x]) * ((ulong)bUInts[y])) + ((ulong)overflow));
					rUInts[x + y] = (uint)holder;
					overflow = (uint)(holder >> 32);
				}
				temp = new UInt256(rUInts[00], rUInts[01], rUInts[02], rUInts[03], rUInts[04], rUInts[05], rUInts[06], rUInts[07]);
				aggregate = aggregate + temp;
			}
			return aggregate;
		}

		// DEPRECATED: A faster algorithm was identified to implement division for large numbers.
		// Source where I found the steps to implement constant-time division: https://www.geeksforgeeks.org/divide-two-integers-without-using-multiplication-division-mod-operator/
		private static UInt256 DivideWithBitShift(UInt256 a, UInt256 b) {
			UInt256 result = new UInt256(0);
			if(b == result)
				throw new DivideByZeroException();
			if(b > a)
				return result;

			UInt256 temp = new UInt256(0);
			UInt256 q = new UInt256(0);
			UInt256 one = new UInt256(1);
			// It's not quite understood what hard-coded value should be used here, but it doesn't really matter, as this method of division won't be used going forward.
			for(int i = 246; i >= 0; i--) {
				UInt256 calc = temp + (b << i);
				if(calc <= a) {
					temp = calc;
					q = q | (one << i);
				}
			}
			return q;

		}

		// Another possible division algorithm: https://stackoverflow.com/questions/5386377/division-without-using/5387432#5387432
		private static UInt256 Divide(UInt256 a, UInt256 b) {
			UInt256 denom = new UInt256(b);
			UInt256 current = new UInt256(0x1);
			UInt256 answer = new UInt256(0x0);
			UInt256 dividend = new UInt256(a);
			UInt256 zero = new UInt256(0x0);

			if(denom > dividend)
				return zero;	//return 0;

			if(denom == dividend)
				return current; //return 1;

			while(denom <= dividend) {
				denom <<= 1;
				current <<= 1;
			}

			denom >>= 1;
			current >>= 1;

			while(current != zero) {
				if(dividend >= denom) {
					dividend -= denom;
					answer |= current;
				}
				current >>= 1;
				denom >>= 1;
			}
			return answer;
		}

		public static UInt256 operator /(UInt256 a, UInt256 b) {
			return Divide(a, b);
		}

		public static UInt256 operator %(UInt256 a, UInt256 b) {
			UInt256 result = a - ((a / b) * b);
			/*UInt256 result = (a / b);
			result = result * b;
			result = a - result;*/
			return result;
		}

		public static UInt256 operator &(UInt256 a, UInt256 b) {
			uint i00 = a.UInt00 & b.UInt00;
			uint i01 = a.UInt01 & b.UInt01;
			uint i02 = a.UInt02 & b.UInt02;
			uint i03 = a.UInt03 & b.UInt03;
			uint i04 = a.UInt04 & b.UInt04;
			uint i05 = a.UInt05 & b.UInt05;
			uint i06 = a.UInt06 & b.UInt06;
			uint i07 = a.UInt07 & b.UInt07;
			return new UInt256(i00, i01, i02, i03, i04, i05, i06, i07);
		}

		public static UInt256 operator |(UInt256 a, UInt256 b) {
			uint i00 = a.UInt00 | b.UInt00;
			uint i01 = a.UInt01 | b.UInt01;
			uint i02 = a.UInt02 | b.UInt02;
			uint i03 = a.UInt03 | b.UInt03;
			uint i04 = a.UInt04 | b.UInt04;
			uint i05 = a.UInt05 | b.UInt05;
			uint i06 = a.UInt06 | b.UInt06;
			uint i07 = a.UInt07 | b.UInt07;
			return new UInt256(i00, i01, i02, i03, i04, i05, i06, i07);
		}

		public static UInt256 operator ^(UInt256 a, UInt256 b) {
			uint i00 = a.UInt00 ^ b.UInt00;
			uint i01 = a.UInt01 ^ b.UInt01;
			uint i02 = a.UInt02 ^ b.UInt02;
			uint i03 = a.UInt03 ^ b.UInt03;
			uint i04 = a.UInt04 ^ b.UInt04;
			uint i05 = a.UInt05 ^ b.UInt05;
			uint i06 = a.UInt06 ^ b.UInt06;
			uint i07 = a.UInt07 ^ b.UInt07;
			return new UInt256(i00, i01, i02, i03, i04, i05, i06, i07);
		}

		public static UInt256 operator <<(UInt256 a, int b) {
			// If shifting by 256 bits or more, then go ahead and return 0.
			if(b >= (_UIntLength * 32)) {
				return new UInt256();
			}
			UInt256 temp = a;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(b / 32);
			int remainder = b % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt256(0, temp.UInt00, temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 << remainder);// | (temp.UInt07 >> (32 - remainder));
			uint result01 = (temp.UInt01 << remainder) | (temp.UInt00 >> (32 - remainder));
			uint result02 = (temp.UInt02 << remainder) | (temp.UInt01 >> (32 - remainder));
			uint result03 = (temp.UInt03 << remainder) | (temp.UInt02 >> (32 - remainder));
			uint result04 = (temp.UInt04 << remainder) | (temp.UInt03 >> (32 - remainder));
			uint result05 = (temp.UInt05 << remainder) | (temp.UInt04 >> (32 - remainder));
			uint result06 = (temp.UInt06 << remainder) | (temp.UInt05 >> (32 - remainder));
			uint result07 = (temp.UInt07 << remainder) | (temp.UInt06 >> (32 - remainder));
			temp = new UInt256(result00, result01, result02, result03, result04, result05, result06, result07);
			return temp;
		}

		// NOTE: This is a bit rotate operation, not just a bit shift operation.  The nth-most bits of the UInt256 object are brought around to the right side.
		//public static UInt256 operator<<<(UInt256 a, int b){
		public static UInt256 RotateLeft(UInt256 a, int b) {
			UInt256 temp = a;
			// There's no need to rotate by more than 256 bits, as rotating a 256 bit unsigned integer by 256 bits results in the same value.
			int bTemp = b % (_UIntLength * 32);// 256;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(bTemp / 32);
			int remainder = bTemp % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt256(temp.UInt07, temp.UInt00, temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 << remainder) | (temp.UInt07 >> (32 - remainder));
			uint result01 = (temp.UInt01 << remainder) | (temp.UInt00 >> (32 - remainder));
			uint result02 = (temp.UInt02 << remainder) | (temp.UInt01 >> (32 - remainder));
			uint result03 = (temp.UInt03 << remainder) | (temp.UInt02 >> (32 - remainder));
			uint result04 = (temp.UInt04 << remainder) | (temp.UInt03 >> (32 - remainder));
			uint result05 = (temp.UInt05 << remainder) | (temp.UInt04 >> (32 - remainder));
			uint result06 = (temp.UInt06 << remainder) | (temp.UInt05 >> (32 - remainder));
			uint result07 = (temp.UInt07 << remainder) | (temp.UInt06 >> (32 - remainder));
			temp = new UInt256(result00, result01, result02, result03, result04, result05, result06, result07);
			return temp;
		}

		public static UInt256 operator >>(UInt256 a, int b) {
			// If shifting by 256 bits or more, then go ahead and return 0.
			if(b >= (_UIntLength * 32)) {
				return new UInt256();
			}
			UInt256 temp = a;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(b / 32);
			int remainder = b % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt256(temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06, temp.UInt07, 0);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 >> remainder) | (temp.UInt01 << (32 - remainder));
			uint result01 = (temp.UInt01 >> remainder) | (temp.UInt02 << (32 - remainder));
			uint result02 = (temp.UInt02 >> remainder) | (temp.UInt03 << (32 - remainder));
			uint result03 = (temp.UInt03 >> remainder) | (temp.UInt04 << (32 - remainder));
			uint result04 = (temp.UInt04 >> remainder) | (temp.UInt05 << (32 - remainder));
			uint result05 = (temp.UInt05 >> remainder) | (temp.UInt06 << (32 - remainder));
			uint result06 = (temp.UInt06 >> remainder) | (temp.UInt07 << (32 - remainder));
			uint result07 = (temp.UInt07 >> remainder);// | (temp.UInt00 << (32 - remainder));
			temp = new UInt256(result00, result01, result02, result03, result04, result05, result06, result07);
			return temp;
		}

		// NOTE: This is a bit rotate operation, not just a bit shift operation.  The nth-most bits of the UInt256 object are brought around to the right side.
		//public static UInt256 operator>>>(UInt256 a, int b){
		public static UInt256 RotateRight(UInt256 a, int b) {
			UInt256 temp = a;
			// There's no need to rotate by more than 256 bits, as rotating a 256 bit unsigned integer by 256 bits results in the same value.
			int bTemp = b % (_UIntLength * 32);// 256;
			// Calculate how many times we just rotate integers vs how many bits we need to shift after that.
			uint quotient = (uint)(bTemp / 32);
			int remainder = bTemp % 32;
			// Rotate integers, as there's little point in shifting more than 31 bits.
			for(uint x = 0; x < quotient; x++) {
				temp = new UInt256(temp.UInt01, temp.UInt02, temp.UInt03, temp.UInt04, temp.UInt05, temp.UInt06, temp.UInt07, temp.UInt00);
			}
			// If the number of bits to shift by is divisible by 32, then we only need to rotate integers; not bit shifting required.
			if(remainder == 0)
				return temp;
			uint result00 = (temp.UInt00 >> remainder) | (temp.UInt01 << (32 - remainder));
			uint result01 = (temp.UInt01 >> remainder) | (temp.UInt02 << (32 - remainder));
			uint result02 = (temp.UInt02 >> remainder) | (temp.UInt03 << (32 - remainder));
			uint result03 = (temp.UInt03 >> remainder) | (temp.UInt04 << (32 - remainder));
			uint result04 = (temp.UInt04 >> remainder) | (temp.UInt05 << (32 - remainder));
			uint result05 = (temp.UInt05 >> remainder) | (temp.UInt06 << (32 - remainder));
			uint result06 = (temp.UInt06 >> remainder) | (temp.UInt07 << (32 - remainder));
			uint result07 = (temp.UInt07 >> remainder) | (temp.UInt00 << (32 - remainder));
			temp = new UInt256(result00, result01, result02, result03, result04, result05, result06, result07);
			return temp;
		}

		public static bool operator ==(UInt256 a, UInt256 b) {
			bool b00 = a._UInt00 == b._UInt00;
			bool b01 = a._UInt01 == b._UInt01;
			bool b02 = a._UInt02 == b._UInt02;
			bool b03 = a._UInt03 == b._UInt03;
			bool b04 = a._UInt04 == b._UInt04;
			bool b05 = a._UInt05 == b._UInt05;
			bool b06 = a._UInt06 == b._UInt06;
			bool b07 = a._UInt07 == b._UInt07;
			bool result = b00 && b01 && b02 && b03 && b04 && b05 && b06 && b07;
			return result;
		}

		public static bool operator !=(UInt256 a, UInt256 b) {
			bool b00 = a._UInt00 != b._UInt00;
			bool b01 = a._UInt01 != b._UInt01;
			bool b02 = a._UInt02 != b._UInt02;
			bool b03 = a._UInt03 != b._UInt03;
			bool b04 = a._UInt04 != b._UInt04;
			bool b05 = a._UInt05 != b._UInt05;
			bool b06 = a._UInt06 != b._UInt06;
			bool b07 = a._UInt07 != b._UInt07;
			bool result = b00 || b01 || b02 || b03 || b04 || b05 || b06 || b07;
			return result;
		}

		public static bool operator >=(UInt256 a, UInt256 b) {
			if(a._UInt07 > b._UInt07)
				return true;
			else if(a._UInt07 == b._UInt07) {
				if(a._UInt06 > b._UInt06)
					return true;
				else if(a._UInt06 == b._UInt06) {
					if(a._UInt05 > b._UInt05)
						return true;
					else if(a._UInt05 == b._UInt05) {
						if(a._UInt04 > b._UInt04)
							return true;
						else if(a._UInt04 == b._UInt04) {
							if(a._UInt03 > b._UInt03)
								return true;
							else if(a._UInt03 == b._UInt03) {
								if(a._UInt02 > b._UInt02)
									return true;
								else if(a._UInt02 == b._UInt02) {
									if(a._UInt01 > b._UInt01)
										return true;
									else if(a._UInt01 == b._UInt01) {
										if(a._UInt00 > b._UInt00 || a._UInt00 == b._UInt00)
											return true;
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		public static bool operator >(UInt256 a, UInt256 b) {
			if(a._UInt07 > b._UInt07)
				return true;
			else if(a._UInt07 == b._UInt07) {
				if(a._UInt06 > b._UInt06)
					return true;
				else if(a._UInt06 == b._UInt06) {
					if(a._UInt05 > b._UInt05)
						return true;
					else if(a._UInt05 == b._UInt05) {
						if(a._UInt04 > b._UInt04)
							return true;
						else if(a._UInt04 == b._UInt04) {
							if(a._UInt03 > b._UInt03)
								return true;
							else if(a._UInt03 == b._UInt03) {
								if(a._UInt02 > b._UInt02)
									return true;
								else if(a._UInt02 == b._UInt02) {
									if(a._UInt01 > b._UInt01)
										return true;
									else if(a._UInt01 == b._UInt01) {
										if(a._UInt00 > b._UInt00)
											return true;
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		public static bool operator <=(UInt256 a, UInt256 b) {
			if(a._UInt07 < b._UInt07)
				return true;
			else if(a._UInt07 == b._UInt07) {
				if(a._UInt06 < b._UInt06)
					return true;
				else if(a._UInt06 == b._UInt06) {
					if(a._UInt05 < b._UInt05)
						return true;
					else if(a._UInt05 == b._UInt05) {
						if(a._UInt04 < b._UInt04)
							return true;
						else if(a._UInt04 == b._UInt04) {
							if(a._UInt03 < b._UInt03)
								return true;
							else if(a._UInt03 == b._UInt03) {
								if(a._UInt02 < b._UInt02)
									return true;
								else if(a._UInt02 == b._UInt02) {
									if(a._UInt01 < b._UInt01)
										return true;
									else if(a._UInt01 == b._UInt01) {
										if(a._UInt00 < b._UInt00 || a._UInt00 == b._UInt00)
											return true;
									}
								}
							}
						}
					}
				}
			}
			return false;
		}

		public static bool operator <(UInt256 a, UInt256 b) {
			if(a._UInt07 < b._UInt07)
				return true;
			else if(a._UInt07 == b._UInt07) {
				if(a._UInt06 < b._UInt06)
					return true;
				else if(a._UInt06 == b._UInt06) {
					if(a._UInt05 < b._UInt05)
						return true;
					else if(a._UInt05 == b._UInt05) {
						if(a._UInt04 < b._UInt04)
							return true;
						else if(a._UInt04 == b._UInt04) {
							if(a._UInt03 < b._UInt03)
								return true;
							else if(a._UInt03 == b._UInt03) {
								if(a._UInt02 < b._UInt02)
									return true;
								else if(a._UInt02 == b._UInt02) {
									if(a._UInt01 < b._UInt01)
										return true;
									else if(a._UInt01 == b._UInt01) {
										if(a._UInt00 < b._UInt00)
											return true;
									}
								}
							}
						}
					}
				}
			}
			return false;
		}
	}
}
