﻿using Freycook0.Math;
using Xunit;

public class Math_Tests {
	private UInt256 tmp00 = new UInt256(0xFFFFFFFF, 0x00FFFFFF);
	private UInt256 tmp01 = new UInt256(0x89ABCDEF, 0x01234567);
	private UInt256 tmp02 = new UInt256(0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567);
	private UInt256 tmp03 = new UInt256(0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567);
	private UInt256 tmp04 = new UInt256(0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567);
	private UInt256 tmp05 = new UInt256(0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567, 0x89ABCDEF, 0x01234567, 0xFF);
	private UInt256 tmp06 = new UInt256(0xFF);
	private UInt256 tmp07 = new UInt256(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF);
	private UInt256 tmp08 = new UInt256(0x11111111, 0x01111111);
	private UInt256 tmp09 = new UInt256(0x00004321);
	private UInt256 tmp10 = new UInt256(0x00004321);
	private UInt256 tmp11 = new UInt256(0x0000EEEE);
	private UInt256 tmp12 = new UInt256(0x00007777);
	private UInt256 tmp13 = new UInt256(0x00007776);
	private UInt256 tmp14 = new UInt256(0x00007778);
	private UInt256 tmp15 = new UInt256(0xFFFFFFFF);
	private UInt256 tmp16 = new UInt256(0x00000000, 0xFFFFFFFF);
	private UInt256 accumulator = new UInt256(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF);
	private UInt256 block = new UInt256(0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF);
	private UInt256 prefix = new UInt256(0x0, 0x0, 0x0, 0x0, 0x1);
	private UInt256 r = new UInt256(0x0FFFFFFF, 0x0FFFFFFC, 0x0FFFFFFC, 0x0FFFFFFC);
	private UInt256 p = new UInt256(0xFFFFFFFB, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0x3);//((new UInt256(0x1)) << 130) - (new UInt256(0x5));

	[Fact]
	public void Test01() {
		Assert.Equal("FFFFFFFFFFFFFF", tmp00.ToString());
		Assert.Equal("123456789ABCDEF", tmp01.ToString());
		Assert.Equal("123456789ABCDEF0123456789ABCDEF", tmp02.ToString());
		Assert.Equal("123456789ABCDEF0123456789ABCDEF0123456789ABCDEF", tmp03.ToString());
		Assert.Equal("123456789ABCDEF0123456789ABCDEF0123456789ABCDEF", tmp04.ToString());
		Assert.Equal("FF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF", tmp05.ToString());
		Assert.Equal("FF", tmp06.ToString());
		Assert.Equal("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", tmp07.ToString());
		Assert.Equal("111111111111111", tmp08.ToString());
		Assert.Equal("4321", tmp09.ToString());
		Assert.Equal("4321", tmp10.ToString());
		Assert.Equal("EEEE", tmp11.ToString());
		Assert.Equal("7777", tmp12.ToString());
		Assert.Equal("7776", tmp13.ToString());
		Assert.Equal("7778", tmp14.ToString());
		Assert.Equal("FFFFFFFF", tmp15.ToString());
		Assert.Equal("FFFFFFFF00000000", tmp16.ToString());
		Assert.Equal("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", accumulator.ToString());
		Assert.Equal("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", block.ToString());
		Assert.Equal("100000000000000000000000000000000", prefix.ToString());
		Assert.Equal("FFFFFFC0FFFFFFC0FFFFFFC0FFFFFFF", r.ToString());
		Assert.Equal("3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB", p.ToString());
	}

	[Fact]
	public void Test02() {
		Assert.Equal("23456789ABCDF0", (tmp01 - tmp00).ToString());
	}

	[Fact]
	public void Test03() {
		Assert.Equal("FF02468ACF13579BDE02468ACF13579BDE02468ACF13579BDE", (tmp04 + tmp05).ToString());
	}

	[Fact]
	public void Test04() {
		Assert.Equal("FEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF01", (tmp06 * tmp07).ToString());
	}

	[Fact]
	public void Test05() {
		Assert.Equal("479AAAAAAAAAAAA631", (tmp08 * tmp09).ToString());
	}

	[Fact]
	public void Test06() {
		Assert.False(tmp08 == tmp09);
	}

	[Fact]
	public void Test07() {
		Assert.True(tmp08 != tmp09);
	}

	[Fact]
	public void Test08() {
		Assert.True(tmp09 == tmp10);
	}

	[Fact]
	public void Test09() {
		Assert.False(tmp09 != tmp10);
	}

	[Fact]
	public void Test10() {
		Assert.False(tmp08 <= tmp09);
	}

	[Fact]
	public void Test11() {
		Assert.True(tmp08 >= tmp09);
	}

	[Fact]
	public void Test12() {
		Assert.True(tmp09 <= tmp10);
	}

	[Fact]
	public void Test13() {
		Assert.True(tmp09 >= tmp10);
	}

	[Fact]
	public void Test14() {
		Assert.False(tmp09 < tmp10);
	}

	[Fact]
	public void Test15() {
		Assert.False(tmp09 > tmp10);
	}

	[Fact]
	public void Test17() {
		Assert.Equal("1", (tmp09 / tmp10).ToString());
	}

	[Fact]
	public void Test18() {
		Assert.Equal("2", (tmp11 / tmp12).ToString());
	}

	[Fact]
	public void Test19() {
		Assert.Equal("0", (tmp11 % tmp12).ToString());
	}

	[Fact]
	public void Test20() {
		Assert.Equal("7777", (tmp12 % tmp11).ToString());
	}

	[Fact]
	public void Test21() {
		Assert.Equal("2", (tmp11 / tmp13).ToString());
	}

	[Fact]
	public void Test22() {
		Assert.Equal("2", (tmp11 % tmp13).ToString());
	}

	[Fact]
	public void Test23() {
		Assert.Equal("7776", (tmp13 % tmp11).ToString());
	}

	[Fact]
	public void Test24() {
		Assert.Equal("1", (tmp11 / tmp14).ToString());
	}

	[Fact]
	public void Test25() {
		Assert.Equal("7776", (tmp11 % tmp14).ToString());
	}

	[Fact]
	public void Test26() {
		Assert.Equal("7778", (tmp14 % tmp11).ToString());
	}

	[Fact]
	public void Test27() {
		Assert.Equal("7776", (tmp13 % tmp14).ToString());
	}

	[Fact]
	public void Test28() {
		Assert.Equal("2", (tmp14 % tmp13).ToString());
	}

	[Fact]
	public void Test29() {
		Assert.Equal("1FFFFFFFE", (tmp15 << 1).ToString());
	}

	[Fact]
	public void Test30() {
		Assert.Equal("3FFFFFFFC", (tmp15 << 2).ToString());
	}

	[Fact]
	public void Test31() {
		Assert.Equal("7FFFFFFF8", (tmp15 << 3).ToString());
	}

	[Fact]
	public void Test32() {
		Assert.Equal("FFFFFFFF0", (tmp15 << 4).ToString());
	}

	[Fact]
	public void Test33() {
		Assert.Equal("FFFFFFFF00", (tmp15 << 8).ToString());
	}

	[Fact]
	public void Test34() {
		Assert.Equal("FFFFFFFF0000", (tmp15 << 16).ToString());
	}

	[Fact]
	public void Test35() {
		Assert.Equal("FFFFFFFF000000", (tmp15 << 24).ToString());
	}

	[Fact]
	public void Test36() {
		Assert.Equal("7FFFFFFF80000000", (tmp15 << 31).ToString());
	}

	[Fact]
	public void Test37() {
		Assert.Equal("FFFFFFFF00000000", (tmp15 << 32).ToString());
	}

	[Fact]
	public void Test38() {
		Assert.Equal("1FFFFFFFE00000000", (tmp15 << 33).ToString());
	}

	[Fact]
	public void Test39() {
		Assert.Equal("FFFFFF0000000000000000000000000000000000000000000000000000000000", (tmp15 << 232).ToString());
	}

	[Fact]
	public void Test40() {
		Assert.Equal("7FFFFFFF80000000", (tmp16 >> 1).ToString());
	}

	[Fact]
	public void Test41() {
		Assert.Equal("3FFFFFFFC0000000", (tmp16 >> 2).ToString());
	}

	[Fact]
	public void Test42() {
		Assert.Equal("1FFFFFFFE0000000", (tmp16 >> 3).ToString());
	}

	[Fact]
	public void Test43() {
		Assert.Equal("FFFFFFFF0000000", (tmp16 >> 4).ToString());
	}

	[Fact]
	public void Test44() {
		Assert.Equal("FFFFFFFF000000", (tmp16 >> 8).ToString());
	}

	[Fact]
	public void Test45() {
		Assert.Equal("FFFFFFFF0000", (tmp16 >> 16).ToString());
	}

	[Fact]
	public void Test46() {
		Assert.Equal("FFFFFFFF00", (tmp16 >> 24).ToString());
	}

	[Fact]
	public void Test47() {
		Assert.Equal("1FFFFFFFE", (tmp16 >> 31).ToString());
	}

	[Fact]
	public void Test48() {
		Assert.Equal("FFFFFFFF", (tmp16 >> 32).ToString());
	}

	[Fact]
	public void Test49() {
		Assert.Equal("7FFFFFFF", (tmp16 >> 33).ToString());
	}

	[Fact]
	public void Test50() {
		Assert.Equal("0", (tmp16 >> 236).ToString());
	}

	[Fact]
	public void Test51() {
		Assert.Equal("FF", (tmp05 / tmp07).ToString());
	}

	[Fact]
	public void Test52() {
		Assert.Equal("123456789ABCDEF0123456789ABCDEF0123456789ABCEEE", (tmp05 % tmp07).ToString());
	}

	[Fact]
	public void Test_Max_Poly1305_Value() {
		Assert.Equal("2FFFFFF42FFFFFF42FFFFFF42FFFFFFCE0000007E0000007E0000007E0000002", ((accumulator + (block | prefix)) * r).ToString());
	}
}
