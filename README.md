# Crypto

A reference implementation of various cryptographic algorithms.  This code has not been peer-reviewed yet, so it should not be used in any production system or application.

The algorithms currently implemented in this library are:
*  Symmetric-Key Encryption
   *  ChaCha20
   *  AEAD_ChaCha20_Poly1305
*  Hash
   *  Blake
   *  Blake2 (both Blake2s and Blake2b)
*  Message Authentication Code
   *  Poly1305